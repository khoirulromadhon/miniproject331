function GetAllEducationLevel(){
    $("#educationLevel").html(
        `<thead>
            <tr>
                <th>Nama</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody id="educationLevelTBody"></tbody>
        `
    );

    $.ajax({
        url : "/api/educationLevel",
        type : "GET",
        contentType : "application/json",
        success: function(data){
            for(i = 0; i<data.length; i++){
                $("#educationLevelTBody").append(
                    `
                    <tr>
                        <td>${data[i].name}</td>
                        <td>
                            <button value="${data[i].id}" onClick="editEducationLevel(this.value)" class="btn btn-warning">
                                <i class="bi-pencil-square"></i>
                            </button>
                            <button value="${data[i].id}" onClick="deleteEducationLevel(this.value)" class="btn btn-danger">
                                <i class="bi-trash"></i>
                            </button>
                        </td>
                    </tr>
                    `
                )
            }
        }
    });
}

$("#addBtn").click(function(){
	$.ajax({
		url: "/educationLevel/create",
		type: "GET",
		contentType: "html",
		success: function(data){
			$(".modal-title").text("Create");
			$(".modal-body").html(data);
			$(".modal").modal("show");
		}
	});
})

function editEducationLevel(id){
	$.ajax({
		url: "/educationLevel/update/" + id,
		type: "GET",
		contentType: "html",
		success: function(data){
			$(".modal-title").text("Update");
			$(".modal-body").html(data);
			$(".modal").modal("show");
		}
	});
}

$("#delBtnCancel").click(function() {
	$(".modal").modal("hide")
})

function deleteEducationLevel(id){
	$.ajax({
		url: "/educationLevel/delete/" + id,
		type: "DELETE",
		contentType: "html",
		success: function(data){
			$(".modal-title").text("Delete");
			$(".modal-body").html(data);
			$(".modal").modal("show");
		}
	});
}

function SearchEducationLevel(request){
if (request.length > 0){
    $.ajax({
        url: '/api/searchEducationLevel=' + request,
        type: 'GET',
        contentType: 'application/json',
        success: function (result) {
            let table = "<table class='table table-stripped mt-3'>";
            table += "<tr> <th width='60%'>Nama</th> <th>Action</th>"
            if (result.length > 0){
                for (let i = 0; i < result.length; i++) {
                    table += "<tr>";
                    table += "<td>" + result[i].name + "</td>";
                    table += "<td><button class='btn btn-warning' value='" + result[i].id + "' onclick=editEducationLevel(this.value)><i class='bi-pencil-square'></i></button> <button class='btn btn-danger' value='" + result[i].id + "' onclick=deleteEducationLevel(this.value)><i class='bi-trash'></i></button></td>";
                    table += "</tr>";
                }
            } else {
                table += "<tr>";
                table += "<td colspan='2' class='text-center'>No data</td>";
                table += "</tr>";
            }
            table += "</table>";
            $('#educationLevel').html(table);
        }
    });
	} else {
		GetAllEducationLevel();
	}

}

$(document).ready(function(){
	GetAllEducationLevel();
})