$(document).ready(function() {
	GetEducationLevelById();

})

function GetEducationLevelById() {
	var id = $("#updateEducationLevelId").val();
	$.ajax({
		url: "/api/educationLevel/" + id,
		type: "GET",
		contentType: "application/json",
		success: function(data) {
			$("#nameInput").val(data.name);
		}
	})
}

$("#editBtnCancel").click(function() {
	$(".modal").modal("hide")
})

$("#editBtnCreate").click(function() {
	var id = $("#updateEducationLevelId").val();
	var name = $("#nameInput").val().trim();

	if (name == "") {
		$("#errName").text("Nama tidak boleh kosong!");
		return;
	} else {
		$("#errName").text("");
	}

	EducationLevel(function(educationLevel) {
        var isNameExist = educationLevel.some(function(item) {
            return item.name.toLowerCase() === name.toLowerCase();
        });

        if (isNameExist) {
            $("#errName").text("Nama sudah ada!");
            return;
        } else {
            $("#errName").text("");
        }

        var obj = { name };
        var myJson = JSON.stringify(obj);
        console.log(myJson);

        $.ajax({
            url: "/api/educationLevel/" + id,
            type: "PUT",
            contentType: "application/json",
            data: myJson,
            success: function(data) {
                $(".modal").modal("hide")
                GetAllEducationLevel();
            },
            error: function() {
                alert("Terjadi kesalahan")
            }
        });
    });
});

function EducationLevel(callback) {
    $.ajax({
        url: "/api/educationLevel/",
        type: "GET",
        contentType: "application/json",
        success: function(educationLevel) {
            if (callback && typeof callback === "function") {
                callback(educationLevel);
            }
        },
        error: function(error) {
            if (callback && typeof callback === "function") {
                callback(error);
            }
        }
    });
}