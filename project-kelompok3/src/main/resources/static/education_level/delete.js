$(document).ready(function() {
	GetEducationLevelById();

})

function GetEducationLevelById() {
	var id = $("#deleteEducationLevelId").val();
	$.ajax({
		url: "/api/educationLevel/" + id,
		type: "GET",
		contentType: "application/json",
		success: function(data) {
			$("#confirm").html(
			    `
			        <tr>
			            <td>Apakah anda ingin menghapus jenjang pendidikan ${data.name} ?</td>
			        </tr>
			    `
			);
		}
	})
}

$("#deleteBtnCancel").click(function() {
	$(".modal").modal("hide")
})

$("#deleteBtn").click(function() {
	var id = $("#deleteEducationLevelId").val();
	$.ajax({
		url : "/api/educationLevel/"+ id,
		type : "DELETE",
		contentType : "application/json",
		success: function(data){
            $(".modal").modal("hide")
            GetAllEducationLevel();
		},
		error: function(){
			alert("Terjadi kesalahan")
		}
	});
})