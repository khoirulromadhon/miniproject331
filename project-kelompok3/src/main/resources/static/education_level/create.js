$("#addBtnCancel").click(function() {
    $(".modal").modal("hide");
});

$("#addBtnCreate").click(function() {
    var name = $("#nameInput").val().trim();

    if (name == "") {
        $("#errName").text("Nama tidak boleh kosong!");
        return;
    } else {
        $("#errName").text("");
    }

    EducationLevel(function(educationLevel) {
        var isNameExist = educationLevel.some(function(item) {
            return item.name.toLowerCase() === name.toLowerCase();
        });

        if (isNameExist) {
            $("#errName").text("Nama sudah ada!");
            return;
        } else {
            $("#errName").text("");
        }

        var obj = { name };
        var myJson = JSON.stringify(obj);

        $.ajax({
            url: "/api/educationLevel",
            type: "POST",
            contentType: "application/json",
            data: myJson,
            success: function(data) {
                $(".modal").modal("hide");
                GetAllEducationLevel();
            },
            error: function() {
                alert("Terjadi kesalahan");
            }
        });
    });
});

function EducationLevel(callback) {
    $.ajax({
        url: "/api/educationLevel",
        type: "GET",
        contentType: "application/json",
        success: function(educationLevel) {
            if (callback && typeof callback === "function") {
                callback(educationLevel);
            }
        },
        error: function(error) {
            if (callback && typeof callback === "function") {
                callback(error);
            }
        }
    });
}