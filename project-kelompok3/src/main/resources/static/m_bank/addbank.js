$("#addBankBtnCancel").click(function(){
    $(".modal").modal("hide")
})

$("#addBankBtnCreate").click(function(){
    var name = $("#nameInput").val();
    var code = $("#codeInput").val();

    if(name == "") {
        $("#errName").text("Name is Empty!");
        return;
    } else {
        $("#errName").text("");
    }
    if(code == "") {
        $("#errCode").text("Code is Empty!");
        return;
    } else {
        $("#errCode").text("");
    }

    var obj = {};
    obj.name = name;
    obj.vaCode = code;

    var myJson = JSON.stringify(obj);

    $.ajax({
        url : "/api/addbank",
        type : "POST",
        contentType : "application/json",
        data : myJson,
        success : function(data){
            $(".modal").modal("hide")
            location.reload();
            getAllBank();
        },
        error : function(){
            alert("Have a Problem")
        }
    });
})