$(document).ready(function() {
	getBankById();

})

function getBankById() {
	var id = $("#editBankId").val();
	$.ajax({
		url: "/api/getBankById/" + id,
		type: "GET",
		contentType: "application/json",
		success: function(data) {
			$("#nameInput").val(data.name);
			$("#codeInput").val(data.vaCode);
		}
	});
}

$("#editBankBtnCancel").click(function() {
	$(".modal").modal("hide")
})

$("#editBankBtnCreate").click(function() {
    var id = $("#editBankId").val();
	var name = $("#nameInput").val();
	var code = $("#codeInput").val();

	if (name == "") {
		$("#errName").text("Nama tidak boleh kosong!");
		return;
	} else {
		$("#errName").text("");
	}
	if (code == "") {
		$("#errCode").text("Code tidak boleh kosong!");
		return;
	} else {
		$("#errCode").text("");
	}


	var obj = {};
	obj.name = name;
	obj.vaCode = code;


	var myJson = JSON.stringify(obj);

	$.ajax({
		url: "/api/editbank/" + id,
		type: "PUT",
		contentType: "application/json",
		data: myJson,
		success: function(data) {
				$(".modal").modal("hide")
				location.reload();
				getAllBank();
		},
		error: function() {
			alert("Terjadi kesalahan")
		}
	});
})
