$(document).ready(function() {
	getBankById();

})
var createdBy;
var createdOn;

function getBankById() {
var id = $("#delBankId").val();
console.log("Mengambil id :",id);
	$.ajax({
		url: "/api/getBankById/" + id,
		type: "GET",
		contentType: "application/json",
		success: function(data) {
		    //console.log(data);
		    createdBy = data.createdBy;
		    createdOn = data.createdOn;
			$("#nameDel").text(data.name);
		}
	})
}

$("#delCancelBtn").click(function() {
	$(".modal").modal("hide")
})

$("#delDeleteBtn").click(function() {
	var id = $("#delBankId").val();
	$.ajax({
		url : "/api/deletebank/"+ id,
		type : "DELETE",
		contentType : "application/json",
		success: function(data){
				$(".modal").modal("hide")
				location.reload();
				GetAllBank();

		},
		error: function(){
			alert("Terjadi kesalahan")
		}
	});
})