function getAllBank(){
	$("#mbankTable").html(
		`<thead>
			<tr>
				<th>ID</th>
				<th><button value="name" onClick="getPageableTable(this.value)" class="button container-fluid btn-transparent">Bank Name</button></th>
				<th>Code</th>
				<th>Action</th>
			</tr>
		</thead>
		<tbody id="mbankTBody"></tbody>
		`
	);


	$.ajax({
		url : "/api/getallbank",
		type : "GET",
		contentType : "application/json",
		success: function(data){
			for(i = 0; i<data.length; i++){
				$("#mbankTBody").append(
					`
					<tr>
						<td>${data[i].id}</td>
						<td>${data[i].name}</td>
						<td>${data[i].vaCode}</td>

						<td>
                            <button value="${data[i].id}" onClick="editBank(this.value)" style="background-color:#445A67; border-color:#778899; color:white; width: 120px; height: 50px;">
                                <i class=> Sunting Data </i>
                            </button>
                            <button value="${data[i].id}" onClick="deleteBank(this.value)" style="background-color:#57838D; border-color:#8B0000; color:white; width: 120px; height: 50px;">
                                <i class=> Hapus Data </i>
                            </button>
                        </td>

					</tr>

					`
				)
			}
		}
	});
}

function MBankList(currentPage, length) {
	$.ajax({
		url : '/api/bankmapped?page=' + currentPage + '&size=' + length,
		type : 'GET',
		contentType : 'application/json',
		success : function(data) {

			let table = '<select class="custom-select mt-3" id="size" onchange="MBankList(0,this.value)">'
			table += '<option value="3" selected>..</option>'
			table += '<option value="5">5</option>'
			table += '<option value="10">10</option>'
			table += '<option value="15">15</option>'
			table += '</select>'
			table += '<table class="table table-bordered mt-3">';
			table += '<tr> <th width="10%" class="text-center">ID</th> <th><button value="name" onClick="getPageableTable(' + currentPage + ',' + length + ',' + 'this.value)" class="button container-fluid btn-transparent">Bank Name</button></th> <th>Code</th> <th>Action</th> </tr>'

			table += "<tr>";
		for (let i = 0; i < data.mBank.length; i++) {
			table += "<td class='text-center'>" + ((i + 1) + (data.currentPage * length)) + "</td>";
			table += "<td>" + data.mBank[i].name + "</td>";
			table += "<td>" + data.mBank[i].vaCode + "</td>";
			table += "<td><button style='background-color:#445A67; border-color:#778899; color:white; width: 120px; height: 30px;' class='btn btn-primary btn-sm' value='" + data.mBank[i].id + "' onclick=editBank(this.value) >Sunting Data</button></td>";
			table += "<td><button style='background-color:#57838D; border-color:#57838D; color:white; width: 120px; height: 30px;' class='btn btn-primary btn-sm' value='" + data.mBank[i].id + "' onclick=deleteBank(this.value) >Hapus Data</button></td>";
			// table += "<td><input type='checkbox' onclick='SelectedItem()' class='mychecked' id='listdelete' value='"+data.category[i].id+"'></td>"
			table += "</tr>";
				}
				table += "</table>";
				table += "<br>"
				table += '<nav aria-label="Page navigation">';
				table += '<ul class="pagination">'
				table += '<li class="page-item"><a class="page-link" onclick="MBankList(' + (data.currentPage - 1) + ',' + length + ')">Previous</a></li>'
				let index = 1;
				for (let i = 0; i < data.totalPages; i++) {
					table += '<li class="page-item"><a id="pageslink" class="page-link" onclick="MBankList(' + i + ',' + length + ')">' + index + '</a></li>'
					index++;
					}
					table += '<li class="page-item"><a class="page-link" onclick="MBankList(' + (data.currentPage + 1) + ',' + length + ')">Next</a></li>'
					table += '</ul>'
					table += '</nav>';
					$('#mbankList').html(table);
		}
	});
}

$("#addBtn").click(function(){
	$.ajax({
		url: "/mbank/addbank",
		type: "GET",
		contentType: "html",
		success: function(data){
			$(".modal-title").text("Create New Bank");
			$(".modal-body").html(data);
			$(".modal").modal("show");
		}
	});
})

function editBank(id){
	$.ajax({
		url: "/mbank/editbank/" + id,
		type: "GET",
		contentType: "html",
		success: function(data){
			$(".modal-title").text("Edit Bank");
			$(".modal-body").html(data);
			$(".modal").modal("show");
		}
	});
}

function deleteBank(id){

	$.ajax({
		url: "/mbank/deletebank/" + id,
		type: "GET",
		contentType: "html",
		success: function(data){
			$(".modal-title").text("Delete Bank");
			$(".modal-body").html(data);
			$(".modal").modal("show");
		}
	});
}


//function SearchPenerbit(request) {
//
//    if(request.length > 0)
//    {
//        $.ajax({
//            url: '/api/searchpenerbit/' + request,
//            type: 'GET',
//            contentType: 'application/json',
//            success: function (result) {
//                if(result.length > 0)
//                {
//                    for(i = 0; i < result.length; i++){
//                    $("#penerbitTBody").html(
//                    `
//                    <tr>
//                        <td>${result[i].idPenerbit}</td>
//                        <td>${result[i].namaPenerbit}</td>
//                        <td>${result[i].tahunBerdiri}</td>
//                        <td>${result[i].asalPenerbit}</td>
//                        <td>${result[i].nomorTelfon}</td>
//
//                        <td>
//                            <button value="${result[i].idPenerbit}" onClick="editPenerbit(this.value)" style="background-color:#445A67; border-color:#778899; color:white; width: 120px; height: 50px;">
//                                <i class> Sunting Data </i>
//                            </button>
//                            <button value="${result[i].idPenerbit}" onClick="deletePenerbit(this.value)" style="background-color:#57838D; border-color:#8B0000; color:white; width: 120px; height: 50px;">
//                                <i class> Hapus Data </i>
//                            </button>
//                        </td>
//
//                    </tr>
//                    `)
//                    }
//                }
//                else {
//                    $("#penerbitTBody").html(
//                    `<tr>
//                    <td colspan='6' class='test-center'>No Data</td>
//                    </tr>
//                    `)
//                }
//            }
//        });
//    }
//    else {
//        GetAllPenerbit();
//    }
//}

function SearchBank(request) {
				//console.log(request)

				if (request.length > 0)
				{
					$.ajax({
						url: '/api/searchbank/' + request,
						type: 'GET',
						contentType: 'application/json',
						success: function (result) {
							//console.log(result)
							let table = "<table class='table table-bordered mt-3'>";
						    table += "<tr> <th width='10%' class='text-center'>ID</th> <th>Name</th> <th>Code</th> <th>Action</th> </tr>"
							if (result.length > 0)
							{
								for (let i = 0; i < result.length; i++) {
									table += "<tr>";
									table += "<td class='text-center'>" + (i+1) + "</td>";
									table += "<td>" + result[i].id + "</td>";
                                    table += "<td>" + result[i].name + "</td>";
                                    table += "<td>" + result[i].vaCode + "</td>";
                                    table += "<td><button style='background-color:#445A67; border-color:#778899; color:white; width: 120px; height: 30px;' class='btn btn-primary btn-sm' value='" + result[i].id + "' onclick=editBank(this.value) >Sunting Data</button></td>";
							        table += "<td><button style='background-color:#57838D; border-color:#57838D; color:white; width: 120px; height: 30px;' class='btn btn-primary btn-sm' value='" + result[i].id + "' onclick=deleteBank(this.value) >Hapus Data</button></td>";
							        table += "</tr>";
								}
							} else {
								table += "<tr>";
								table += "<td colspan='4' class='text-center'>No data</td>";
								table += "</tr>";
							}
							table += "</table>";
							$('#mbankList').html(table);
						}
					});
				} else {
					MBankList(0,5);
				}
			}


//function getPageable(page, ukuran, keterangan){
//    pagePublic = page;
//    keteranganPublic = keterangan;
//    if(keterangan === "mapped"){
//        $.ajax({
//            url : '/api/bankmapped?page=' + currentPage + '&size=' + length,
//            type : "GET",
//            contentType : "application/json",
//            success: function(data){
//               MBankList(currentPage, length);
//            }
//        });
//    }else if(keterangan === "search"){
//        var kata = kataPublic;
//        $.ajax({
//            url : "/api/baju/search?halaman="+page+"&ukuran="+ukuran+"&kata="+kata,
//            type : "GET",
//            contentType : "application/json",
//            success: function(data){
//                setPageable(data, ukuran, keterangan);
//            }
//        });
//    }else{
//        $.ajax({
//            url : "/api/baju/sort?halaman="+page+"&ukuran="+ukuran+"&kata="+keterangan+"&isAsc="+isAsc,
//            type : "GET",
//            contentType : "application/json",
//            success: function(data){
//                console.log("is asc = "+ isAsc);
//                isAsc = isAsc == true ? false : true;
//                setPageable(data, ukuran, keterangan);
//            }
//        });
//    }
//}
var isAsc = true;
function getPageableTable(currentPage, length, name) {
console.log(name);

        $.ajax({
                url : '/api/banksort?page=' + currentPage + '&size=' + length + '&name=' + name + '&isAsc=' + isAsc,
                type : "GET",
                contentType : "application/json",
                success : function(data) {
                console.log(data);
                isAsc = isAsc == true ? false : true;
                    let table = '<select class="custom-select mt-3" id="size" onchange="MBankList(' + 0 + ',' + 5 + ',' + name + ')">'
                    table += '<option value="3" selected>..</option>'
                    table += '<option value="5">5</option>'
                    table += '<option value="10">10</option>'
                    table += '<option value="15">15</option>'
                    table += '</select>'
                    table += '<table class="table table-bordered mt-3">';
                    table += '<tr> <th width="10%" class="text-center">ID</th> <th><button value="name" onClick="getPageableTable(' + currentPage + ',' + length + ',' + 'this.value)" class="button container-fluid">Bank Name</button></th> <th>Code</th> <th>Action</th> </tr>'
                    console.log(data);
                for (let i = 0; i < data.mbank.length; i++)
                {
                    table += "<tr>";
                    table += "<td class - 'text-center'>" + ((i+1) + (data.currentPage * length)) + "</td>"
                    table += "<td>" + data.mbank[i].name + "</td>";
                    table += "<td>" + data.mbank[i].vaCode + "</td>";
                    table += "<td><button style='background-color:#445A67; border-color:#778899; color:white; width: 120px; height: 30px;' class='btn btn-primary btn-sm' value='" + data.mbank[i].id + "' onclick=editBank(this.value) >Sunting Data</button></td>";
                    table += "<td><button style='background-color:#57838D; border-color:#57838D; color:white; width: 120px; height: 30px;' class='btn btn-primary btn-sm' value='" + data.mbank[i].id + "' onclick=deleteBank(this.value) >Hapus Data</button></td>";
                    table += "</tr>";
                }
                table += "</table>";
                table += "<br>"
                table += '<nav aria-label="Page navigation">';
                table += '<ul class="pagination">'
                table += '<li class="page-item"><a class="page-link" onclick="MBankList(' + (data.currentPage - 1) + ',' + length + ')">Previous</a></li>'
                let index = 1;
                for (let i = 0; i < data.totalPages; i++) {
                	table += '<li class="page-item"><a id="pageslink" class="page-link" onclick="MBankList(' + i + ',' + length + ')">' + index + '</a></li>'
                	index++;
                }
                	table += '<li class="page-item"><a class="page-link" onclick="MBankList(' + (data.currentPage + 1) + ',' + length + ')">Next</a></li>'
                	table += '</ul>'
                	table += '</nav>';
                	$('#mbankList').html(table);
                }
        })
}


$(document).ready(function(){
//	getAllBank();
	MBankList(0,5);
})