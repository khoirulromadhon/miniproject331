$("#btnKirimOtp").click(function(){
    buttonLoading(true);
    var emailInput = $("#emailDaftar").val();
    //Kombinasi dimana awal ada huruf/angka(tak hingga) + ada @ dan huruf/angka(tak hingga) + ada . dibelakangnya ada huruf/angka sebanyak 2 - 4
    var emailFormatValidation = /^[A-Za-z0-9_\-\.]+\@[A-Za-z0-9_\-\.]+\.[A-Za-z]{2,4}$/;

    if(emailInput === ""){
        $("#emailDaftarErr").text("*Masukkan email!");
        buttonLoading(false);
        return
    }else if(!emailFormatValidation.test(emailInput)){
        $("#emailDaftarErr").text("*Email tidak sesuai format!");
        buttonLoading(false);
        return
    }
    else{
        $("#emailDaftarErr").text("");
    }
    $.ajax({
        url: "api/user/email=" + emailInput,
        type: "GET",
        contentType: "html",
        success: function(data){
           if(data === true){
               document.getElementById("emailDaftarErr").className = "text-danger";
               $("#emailDaftarErr").text("*Email telah terdaftar!");
               buttonLoading(false);
               return;
           }else{
               document.getElementById("emailDaftarErr").className = "text-success";
               $("#emailDaftarErr").text("*Email tidak terdaftar, dapat digunakan");
               newOTP(emailInput);
           }
        }
    });
})

function newOTP(email){
    $.ajax({
        url: "api/token/tokenToExpired/?email="+email+"&used_for=Pendaftaran",
        type: "PUT",
        contentType: "application/json",
        success: function(data){
           sentOTP(email);
        }
    });
}

function sentOTP(emailInput){
   var token = "";
   const characters ='ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
   const charactersLength = characters.length;
   for ( let i = 0; i < 8; i++ ) {
       token += characters.charAt(Math.floor(Math.random() * characters.length));
   }

   var otpMessage = "Kode OTP kamu \n \n \n "+ token +"\n \n \n OTP ini bersifat RAHASIA harap tidak dibagikan ke siapapun. Silakan masukan kode ini untuk pendaftaran kamu.\n  Jika merasa tidak melakukan pendaftaran abaikan mail ini Terima Kasih";
   var templateSent = {};
   templateSent.subject = "OTP Register PIN";
   templateSent.message = otpMessage;
   var myJson = JSON.stringify(templateSent);
   $.ajax({
       url: "api/mail/send/?mailTo=" + emailInput,
       type: "POST",
       data: myJson,
       contentType: "application/json",
       success: function(data){
          if(data === "Berhasil"){
              postToken(emailInput, token);
          }else{
              alert("Server gagal mengirimkan OTP");
              buttonLoading(false);
              return;
          }
       }
   });
}

function postToken(email, token){
    var data = {};
    data.email = email;
    data.token = token;
    data.expired = false;
    data.used_for = "Pendaftaran";
    var myJson = JSON.stringify(data);
    $.ajax({
        url : "/api/token/save",
        type : "POST",
        contentType : "application/json",
        data : myJson,
        success : function(data){
            moveToOtp(email);
        },
        error: function(){
            alert("Terjadi kesalahan")
            buttonLoading(false);
            return;
        }
    });
}

function moveToOtp(email){
    $.ajax({
        url: "/register/otp/"+email,
        type: "GET",
        contentType: "html",
        success: function(data){
            $(".modal").modal("hide");
            $(".modal-title").text("Verifikasi E-Mail");
            $(".modal-body").html(data);
            $(".modal").modal("show");
        }
    });
}

function buttonLoading(keterangan){
    if(keterangan === true){
        document.getElementById("btnKirimOtp").disabled = true;
        document.getElementById("emailDaftar").disabled = true;
        document.getElementById("btnKirimOtp").innerHTML = "<span class='spinner-border spinner-border-sm' role='status' aria-hidden='true'></span> Loading...";
    }else{
        document.getElementById("btnKirimOtp").disabled = false;
        document.getElementById("emailDaftar").disabled = false;
        document.getElementById("btnKirimOtp").innerHTML = "Kirim OTP";
    }
}