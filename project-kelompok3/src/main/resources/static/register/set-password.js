// Mode Hidden atau Show
$("#eyeIconRegisterButton").click(function(){
    if($("#passwordInput").attr("type") == "password"){
        $("#passwordInput").attr("type", "text");
        $("#eyeIconRegister").removeClass("bi bi-eye-slash");
        $("#eyeIconRegister").addClass("bi bi-eye");
    }else if($("#passwordInput").attr("type") == "text"){
        $("#passwordInput").attr("type", "password");
        $("#eyeIconRegister").removeClass("bi bi-eye");
        $("#eyeIconRegister").addClass("bi bi-eye-slash");
    }
})
$("#eyeIconKonfirmasiRegisterButton").click(function(){
    if($("#passwordKonfirmasi").attr("type") == "password"){
        $("#passwordKonfirmasi").attr("type", "text");
        $("#eyeIconKonfirmasiRegister").removeClass("bi bi-eye-slash");
        $("#eyeIconKonfirmasiRegister").addClass("bi bi-eye");
    }else if($("#passwordKonfirmasi").attr("type") == "text"){
        $("#passwordKonfirmasi").attr("type", "password");
        $("#eyeIconKonfirmasiRegister").removeClass("bi bi-eye");
        $("#eyeIconKonfirmasiRegister").addClass("bi bi-eye-slash");
    }
})

$("#btnSetPassword").click(function(){
    var password = $("#passwordInput").val();
    var password_konfirmasi = $("#passwordKonfirmasi").val();
    var boleanPassword = false;
    var boleanKonfirmasiPassword = false;
    var simbol = "";
    var angka = /[0-9]/;
    var hurufkecil = /[a-z]/;
    var hurufbesar = /[A-Z]/;
    var special = /[~`!@#$%^&*\(\)\-_+=\{\}\[\]|\\\;:\<>,.\"/?]/;
    var whitespace = /\s/;
    var textValidasi = "";

    if(password === ""){
        textValidasi += "*Masukkan password: "
    }

    //Strong password
    if(password.length < 8){
        if(textValidasi === ""){
            textValidasi += "*"
        }
        textValidasi += "Panjang minimal 8, ";
    }
    if(!angka.test(password)){
        if(textValidasi === ""){
            textValidasi += "*Harus "
        }
        textValidasi += "terdapat angka, ";
    }
    if(!hurufkecil.test(password)){
        if(textValidasi === ""){
            textValidasi += "*Harus "
        }
        textValidasi += "terdapat huruf kecil, ";
    }
    if(!hurufbesar.test(password)){
        if(textValidasi === ""){
            textValidasi += "*Harus "
        }
        textValidasi += "terdapat huruf besar, ";
    }
    if(!special.test(password)){
        if(textValidasi === ""){
            textValidasi += "*Harus "
        }
        textValidasi += "terdapat spesial karakter, ";
    }
    if(whitespace.test(password)){
        if(textValidasi === ""){
            textValidasi += "*"
        }
        textValidasi += "tidak boleh ada whitespace";
    }
    if (password_konfirmasi === ""){
        $("#passwordKonfirmasiErr").text("*Konfimasi password tidak boleh kosong");
    }
    else if(password != password_konfirmasi){
        $("#passwordKonfirmasiErr").text("*Password tidak sama");
    }else{
        $("#passwordKonfirmasiErr").text("");
        boleanKonfirmasiPassword = true;
    }

    if(textValidasi != ""){
        $("#passwordInputErr").text(textValidasi);
    }else{
        $("#passwordInputErr").text("");
        boleanPassword = true;
    }

    if(boleanPassword == false || boleanKonfirmasiPassword == false){
        return;
    }else if(boleanPassword == true && boleanKonfirmasiPassword == true){
        moveToUserData(password);
    }
})

function moveToUserData(password){
    var email = $("#saveEmail").val();
    var encodedEmail = encodeURIComponent(email);
    var encodedPassword = encodeURIComponent(password);
    $.ajax({
        url: "/register/setUserData/" + email + "/" + password,
        type: "GET",
        contentType: "html",
        success: function(data){
            $(".modal").modal("hide");
            $(".modal-title").text("Daftar");
            $(".modal-body").html(data);
            $(".modal").modal("show");
        }
    });
}
