$("#cancelButton").click(function(){
    $(".modal").modal("hide");
});
function getCurrentPageFromURL() {
    var urlParams = new URLSearchParams(window.location.search);
    return urlParams.get('page');
}
function getAllOption() {
    $.ajax({
        url: "/api/getlocationlevel",
        type: "GET",
        contentType: "application/json",
        success: function(levelData) {
            for (let i = 0; i < levelData.length; i++) {
                $("#levelSelect").append(
                    `<option value="${levelData[i].id}">${levelData[i].name}</option>`
                );
            }
            populateAreaSelect(); // Panggil fungsi populateAreaSelect setelah selesai mengisi levelSelect
        },
        error: function(error) {
            console.error("Gagal mengambil data level lokasi:", error);
        }
    });

    // Panggil fungsi populateAreaSelect ketika terjadi perubahan pada levelSelect
    $("#levelSelect").change(populateAreaSelect);
}

function populateAreaSelect() {
    let levelselected = $("#levelSelect option:selected").text();
    $("#areaSelect").empty(); // Hapus semua opsi sebelum menambahkan yang baru

    $.ajax({
        url: "/api/getlocation",
        type: "GET",
        contentType: "application/json",
        success: function(locationData) {
            for (let i = 0; i < locationData.length; i++) {
                let locationLevelAbbreviation = locationData[i].locationLevel.abbreviation.toLowerCase();

                if (levelselected.toLowerCase() === 'provinsi' || levelselected === '--Pilih--') {
                    // Tampilkan opsi default atau "--Pilih--"
                   if ($("#areaSelect option[value='']").length === 0) {
                        $("#areaSelect").append(`<option value="">--Pilih--</option>`);
                    }
                } else if (levelselected.toLowerCase() === 'kota' || levelselected.toLowerCase() === 'kabupaten') {
                    // Tampilkan opsi dengan abbreviation "Prov."
                    if (locationLevelAbbreviation === 'prov.') {
                        $("#areaSelect").append(
                            `<option value="${locationData[i].id}">${locationData[i].locationLevel.abbreviation} ${locationData[i].name}</option>`
                        );
                    }
                } else if (levelselected.toLowerCase() === 'kecamatan') {
                    // Tampilkan opsi dengan abbreviation "Kota" atau "Kab."
                    if (locationLevelAbbreviation === 'kota' || locationLevelAbbreviation === 'kab.') {
                        $("#areaSelect").append(
                            `<option value="${locationData[i].id}">${locationData[i].locationLevel.abbreviation} ${locationData[i].name},
                            ${locationData[i].parentLocation.locationLevel.abbreviation} ${locationData[i].parentLocation.name}</option>`
                        );
                    }
                } else if (levelselected.toLowerCase() === 'desa' || levelselected.toLowerCase() === 'dusun' || levelselected.toLowerCase() === 'kelurahan') {
                    // Tampilkan opsi dengan abbreviation "Kec."
                    if (locationLevelAbbreviation === 'kec.') {
                        $("#areaSelect").append(
                            `<option value="${locationData[i].id}">${locationData[i].locationLevel.abbreviation} ${locationData[i].name},
                            ${locationData[i].parentLocation.locationLevel.abbreviation} ${locationData[i].parentLocation.name}</option>`
                        );
                    }
                }
            }
        },
        error: function(error) {
            console.error("Gagal mengambil data lokasi:", error);
        }
    });
}



function validateAreaSelection(selectedLevelName, selectArea) {
    var isProvinsi = selectedLevelName.toLowerCase() === "provinsi";

    if (isProvinsi) {
        if (selectArea !== "--Pilih--") {
            $("#errareaSelect").text("Wilayah harus kosong atau dipilih saat level lokasi adalah Provinsi.");
            return false; // Validasi tidak berhasil
        } else {
            $("#errareaSelect").text("");
            return true; // Validasi berhasil
        }
    } else {
        if (selectArea === "--Pilih--") {
            $("#errareaSelect").text("Pilih wilayah terlebih dahulu!");
            return false; // Validasi tidak berhasil
        } else {
            $("#errareaSelect").text("");
            return true; // Validasi berhasil
        }
    }
}

$("#addButton").click(function() {
    var namaLocation = $("#nameInput").val().trim();
    var selectArea = $("#areaSelect").val();
    var selectLevel = $("#levelSelect").val();

    if (namaLocation === "") {
        $("#errName").text("Nama Level Lokasi tidak boleh kosong");
        return;
    } else {
        $("#errName").text("");
    }

    var selectedLevelName = $("#levelSelect option:selected").text(); //ini ngambil data yang di select pada dropdown

    // Memanggil fungsi validasi area
    var isValidArea = validateAreaSelection(selectedLevelName, selectArea);

    if (!isValidArea) {
        return; // Berhenti jika validasi area tidak berhasil
    }

    var obj = {
        name: namaLocation,
        locationLevel: {
            id: selectLevel
        },
        parentLocation: selectArea === "--Pilih--" ? null : { id: selectArea }
    };

    var myJson = JSON.stringify(obj);
    console.log("Data yang di input : ", myJson)
    $.ajax({
        url: "/api/addlocation",
        type: "POST",
        contentType: "application/json",
        data: myJson,
        success: function(data, textStatus, xhr) {
            $(".modal").modal("hide");
            var refererURL = getCurrentPageFromURL();
            if (refererURL) {
                window.location.href = '/location?page=' + refererURL;
            } else {
                var defaultPageURL = '/location';
                window.location.href = defaultPageURL;
            }
        },
        error: function(xhr, textStatus, errorThrown) {
            if (xhr.status === 400) {
                var errorMessage = xhr.responseText;
                $("#errName").text(errorMessage);
            } else {
                alert("Terjadi kesalahan: " + errorThrown);
            }
        }
    });
});

$(document).ready(function() {
    getAllOption();
});
