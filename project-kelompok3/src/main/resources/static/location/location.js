function getlocationbyPage(pageNumber, pageSize) {
function updateURL(pageNumber) {
        const newURL = window.location.origin + window.location.pathname + '?page=' + pageNumber;
        window.history.pushState({ path: newURL }, '', newURL);
    }
    $("#location").html(
        `<thead>
            <tr>
                <th>Nama</th>
                <th>Level Lokasi</th>
                <th>Wilayah</th>
                <th>#</th>
            </tr>
        </thead>
        <tbody id="locationTBody"></tbody>`
    );

    $.ajax({
        url: "/api/locationmapped?page=" + pageNumber + "&size=" + pageSize,
        type: "GET",
        contentType: "application/json",
        success: function (data) {
            if (data && data.location && Array.isArray(data.location)) {
                $("#locationTBody").empty();
                for (let i = 0; i < data.location.length; i++) {
                //let locationLevelName = data.location[i].locationlevel ? data.location[i].locationlevel.name : 'N/A';
                //let locationName = data.location[i].location ? data.location[i].location.name : 'N/A';

                    $("#locationTBody").append(
                        `<tr>
                            <td>${data.location[i].name}</td>
                            <td>${data.location[i].locationLevel.name}</td>
                            <td>${data.location[i].parentLocation ? `${data.location[i].parentLocation.locationLevel.abbreviation} ${data.location[i].parentLocation.name}` : 'N/A'}</td>
                            <td>
                                <button value="${data.location[i].id}" onClick="editlocation(this.value)" class="btn btn-warning" style='background-color:#57838D; border-color:#57838D; color:white; width: 120px; height: 30px;'>
                                    <i class="bi-pencil-square"></i>
                                </button>
                                <button value="${data.location[i].id}" onClick="deletelocation(this.value)" class="btn btn-danger red-button" style='background-color: red; border-color:#57838D; color:white; width: 120px; height: 30px;'>
                                    <i class="bi-trash"></i>
                                </button>
                            </td>
                        </tr>`
                    );
                }
                validatePagination(data.totalPages, pageNumber);
                updateURL(pageNumber);
            } else {
                console.error('Data or data.location is undefined or not an array.');
            }
        }
    });
}
let currentPage = sessionStorage.getItem('currentPage');
function updateURL(pageNumber) {
    const newURL = window.location.origin + window.location.pathname + '?page=' + pageNumber;
    window.history.pushState({ path: newURL }, '', newURL);
}
if (!currentPage || isNaN(parseInt(currentPage))) {
    currentPage = 0;
} else {
    currentPage = parseInt(currentPage);
}
function validatePagination(totalPages, currentPage) {
    if (currentPage === 0) {
        disablePreviousButton();
    } else {
        enablePreviousButton();
    }

    if (currentPage >= totalPages - 1) {
        disableNextButton();
    } else {
        enableNextButton();
    }
}
function nextPage() {
    currentPage++;
    getlocationbyPage(currentPage, 5);
}

function previousPage() {
    if (currentPage > 0) {
        currentPage--;
        getlocationbyPage(currentPage, 5);
    }
}
function disableNextButton() {
    $('.page-link:contains("Next")').addClass('disabled');
}

function enableNextButton() {
    $('.page-link:contains("Next")').removeClass('disabled');
}
function disablePreviousButton() {
    $('.page-link:contains("Previous")').addClass('disabled');
}
function enablePreviousButton() {
    $('.page-link:contains("Previous")').removeClass('disabled');
}
$("#addBtn").click(function () {
    $.ajax({
        url: "/location/addlocation",
        type: "GET",
        contentType: "html",
        success: function (data) {
            $(".modal-title").text("Tambah Lokasi");
            $(".modal-body").html(data);
            $(".modal").modal("show");
        }
    });
});
function editlocation(id) {
    $.ajax({
        url: "/location/editlocation/" + id,
        type: "GET",
        contentType: "html",
        success: function (data) {
            $(".modal-title").text("Edit Lokasi");
            $(".modal-body").html(data);
            $(".modal").modal("show");
        }
    });
}
function deletelocation(id) {
    console.log(id);
    $.ajax({
        url: "/location/deletelocation/" + id,
        type: "GET",
        contentType: "html",
        success: function (data) {
            $(".modal-title").text("Delete Lokasi");
            $(".modal-body").html(data);
            $(".modal").modal("show");
        }
    });
}
function SearchLocation(request) {
    if (request && request.length > 0) {
        $.ajax({
            url: "/api/searchlocation/" + request,
            type: "GET",
            contentType: 'application/json',
            success: function (result) {
                if (result && Array.isArray(result) && result.length > 0) {
                    $("#locationTBody").empty();

                    for (let i = 0; i < result.length; i++) {
                        $("#locationTBody").append(
                            `<tr>
                                <td>${result[i].name}</td>
                                <td>${result[i].locationLevel.name}</td>
                                <td>${result[i].parentLocation ? `${result[i].parentLocation.locationLevel.abbreviation} ${result[i].parentLocation.name}` : 'N/A'}</td>
                                <td>
                                    <button value="${result[i].id}" onClick="editlocation(this.value)" class="btn btn-warning" style='background-color:#57838D; border-color:#57838D; color:white; width: 120px; height: 30px;'>
                                        <i class="bi-pencil-square"></i>
                                    </button>
                                    <button value="${result[i].id}" onClick="deletelocation(this.value)" class="btn btn-danger red-button" style='background-color: red; border-color:#57838D; color:white; width: 120px; height: 30px;'>
                                        <i class="bi-trash"></i>
                                    </button>
                                </td>
                            </tr>`
                        );
                    }
                } else {
                    $("#locationTBody").html(
                        `<tr>
                            <td colspan='4' class='text-center'>No Data</td>
                        </tr>`
                    );
                }
            },
            error: function (error) {
                console.error('Error occurred:', error);
            }
        });
    } else {
        getlocationbyPage(0, 5);
    }
}
function sortbylevel(){
        $.ajax({
            url: "/api/getlocationlevel",
            type: "GET",
            contentType: "application/json",
            success: function(data) {
                for (let i = 0; i < data.length; i++) {
                    $("#sortByLevel").append(
                        `<option value="${data[i].id}">${data[i].name}</option>`
                    );
                    console.log("ID dari location level:", data[i].id);
                }
            }
        });
}
function sortByLocationLevel(levelId) {
    if (levelId.toLowerCase() === "all") {
        getlocationbyPage(0, 5);
        $('#pagination').show(); // Menampilkan paginasi
        enablePagination();
        return;
    } else {
        $.ajax({
            url: "/api/location/byLevel/" + levelId,
            type: "GET",
            contentType: "application/json",
            success: function (data) {
                if (data && Array.isArray(data)) {
                    $("#locationTBody").empty();
                    for (let i = 0; i < data.length; i++) {
                        $("#locationTBody").append(
                            `<tr>
                                <td>${data[i].name}</td>
                                <td>${data[i].locationLevel.name}</td>
                                <td>${data[i].parentLocation ? `${data[i].parentLocation.locationLevel.abbreviation} ${data[i].parentLocation.name}` : 'N/A'}</td>
                                <td>
                                    <button value="${data[i].id}" onClick="editlocation(this.value)" class="btn btn-warning" style='background-color:#57838D; border-color:#57838D; color:white; width: 120px; height: 30px;'>
                                        <i class="bi-pencil-square"></i>
                                    </button>
                                    <button value="${data[i].id}" onClick="deletelocation(this.value)" class="btn btn-danger red-button" style='background-color: red; border-color:#57838D; color:white; width: 120px; height: 30px;'>
                                        <i class="bi-trash"></i>
                                    </button>
                                </td>
                            </tr>`
                        );
                    }
                    disablePagination();
                } else {
                    $("#locationTBody").html(
                        `<tr>
                            <td colspan='4' class='text-center'>No Data</td>
                        </tr>`
                    );
                }
                $('#pagination').hide(); // Menyembunyikan paginasi
            },
            error: function (error) {
                console.error('Error occurred:', error);
            }
        });
    }
}
function disablePagination() {
    $('.page-link:contains("Next")').addClass('disabled');
    $('.page-link:contains("Previous")').addClass('disabled');
    $('.page-link').addClass('disabled');
}

function enablePagination() {
    $('.page-link:contains("Next")').removeClass('disabled');
    $('.page-link:contains("Previous")').removeClass('disabled');
    $('.page-link').removeClass('disabled');
}
$(document).ready(function () {
    let pageSize = 5;
    let currentPage = sessionStorage.getItem('currentPage');
    if (!currentPage) {
            currentPage = 0;
    }
    currentPage = parseInt(currentPage);
    sortbylevel(); // Fungsi untuk mengisi dropdown dengan pilihan level

    getlocationbyPage(currentPage, pageSize); // Mendapatkan data sesuai nomor halaman dari URL

    $('body').on('click', 'a.page-link', function (event) {
            event.preventDefault();

            let text = $(this).text();
            if (text === 'Previous' && currentPage > 0) {
                currentPage--;
            } else if (text === 'Next') {
                currentPage++;
            } else {
                currentPage = parseInt(text) - 1;
            }
            getlocationbyPage(currentPage, pageSize);
    });

    function getCurrentPageFromURL() {
        const urlParams = new URLSearchParams(window.location.search);
        const page = urlParams.get('page');
        return page ? parseInt(page) : 0;
    }
});
