$(document).ready(function() {
	GetLevelLocationById();
})
function getCurrentPageFromURL() {
    var urlParams = new URLSearchParams(window.location.search);
    return urlParams.get('page');
}
function GetLevelLocationById(){
    var id = $("#editId").val();
    var currentPage = getCurrentPageFromURL();
    sessionStorage.setItem('currentPage', currentPage); // Menyimpan nilai currentPage dalam sessionStorage
    console.log(id);
    $.ajax({
        url: "/api/locationlevel/" + id,
        type: "GET",
        contentType: "application/json",
        success: function(data) {
            console.log(data);
            $("#nameEdit").val(data.name);
            $("#singkatEdit").val(data.abbreviation);
        }
    });
}

$("#cancelButton").click(function() {
	$(".modal").modal("hide")
})
$("#addButton").click(function() {
    var id = $("#editId").val();
    var namaLocation = $("#nameEdit").val().trim();
    var singkatanLocation = $("#singkatEdit").val().trim();

     if (namaLocation === "") {
            $("#errName").text("Nama Level Lokasi tidak boleh kosong");
            return;
    } else if (/^\s+$/.test(namaLocation)) {
        $("#errName").text("Nama Level Lokasi hanya berisi spasi");
        return;
    } else {
        $("#errName").text("");
    }

    var obj = {};
    obj.id = id;
    obj.name = namaLocation;
    obj.abbreviation = singkatanLocation;

    var myJson = JSON.stringify(obj);
    console.log("Inputan : ", myJson);

    var refererURL = sessionStorage.getItem('currentPage');

    $.ajax({
        url: "/api/locationlevel/" + id,
        type: "PUT",
        contentType: "application/json",
        data: myJson,
        async:true,
        success: function(data, textStatus, xhr) {
            $(".modal").modal("hide");
            if (refererURL) {
                window.location.href = '/locationlevel?page=' + refererURL;
            } else {
                var defaultPageURL = '/locationlevel';
                window.location.href = defaultPageURL;
            }
        },
        error: function(xhr, textStatus, errorThrown) {
            if (xhr.status === 400) {
                var errorMessage = xhr.responseText;
                $("#errName").text(errorMessage);
            } else {
                alert("Terjadi kesalahan: " + errorThrown);
            }
        }
    });
});

