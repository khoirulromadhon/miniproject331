function getLocationLevelbyPage(pageNumber, pageSize) {
    // Fungsi untuk memperbarui URL
    function updateURL(pageNumber) {
        const newURL = window.location.origin + window.location.pathname + '?page=' + pageNumber;
        window.history.pushState({ path: newURL }, '', newURL);
    }

    // Kode yang ada untuk mengambil data dengan AJAX dan menampilkan dalam tabel
    $("#locationLevelTable").html(
        `<thead>
            <tr>
                <th>Nama</th>
                <th>Nama Singkat</th>
                <th>#</th>
            </tr>
        </thead>
        <tbody id="locationLevelTBody"></tbody>`
    );

    $.ajax({
        url: "/api/locationlevelmapped?page=" + pageNumber + "&size=" + pageSize,
        type: "GET",
        contentType: "application/json",
        success: function (data) {
            if (data && data.locationlevel && Array.isArray(data.locationlevel)) {
                $("#locationLevelTBody").empty();
                for (let i = 0; i < data.locationlevel.length; i++) {
                    $("#locationLevelTBody").append(
                        `<tr>
                            <td>${data.locationlevel[i].name}</td>
                            <td>${data.locationlevel[i].abbreviation}</td>
                            <td>
                                <button value="${data.locationlevel[i].id}" onClick="editlocationLevel(this.value)" class="btn btn-warning" style='background-color:#57838D; border-color:#57838D; color:white; width: 120px; height: 30px;'>
                                    <i class="bi-pencil-square"></i>
                                </button>
                                <button value="${data.locationlevel[i].id}" onClick="deletelocationLevel(this.value)" class="btn btn-danger red-button" style='background-color: red; border-color:#57838D; color:white; width: 120px; height: 30px;'>
                                    <i class="bi-trash"></i>
                                </button>
                            </td>
                        </tr>`
                    );
                }
                validatePagination(data.totalPages, pageNumber);
                updateURL(pageNumber); // Memanggil fungsi untuk memperbarui URL setiap kali nomor halaman berubah
            } else {
                console.error('Data or data.locationlevel is undefined or not an array.');
            }
        }
    });
}


function updateURL(pageNumber) {
    const newURL = window.location.origin + window.location.pathname + '?page=' + pageNumber;
    window.history.pushState({ path: newURL }, '', newURL);
}

let currentPage = sessionStorage.getItem('currentPage');
if (!currentPage || isNaN(parseInt(currentPage))) {
    currentPage = 0;
} else {
    currentPage = parseInt(currentPage);
}

function validatePagination(totalPages, currentPage) {
    if (currentPage === 0) {
        disablePreviousButton();
    } else {
        enablePreviousButton();
    }

    if (currentPage >= totalPages - 1) {
        disableNextButton();
    } else {
        enableNextButton();
    }
}
function nextPage() {
    currentPage++;
    getLocationLevelbyPage(currentPage, 5);
}

function previousPage() {
    if (currentPage > 0) {
        currentPage--;
        getLocationLevelbyPage(currentPage, 5);
    }
}
function disableNextButton() {
    $('.page-link:contains("Next")').addClass('disabled');
}

function enableNextButton() {
    $('.page-link:contains("Next")').removeClass('disabled');
}
function disablePreviousButton() {
    $('.page-link:contains("Previous")').addClass('disabled');
}
function enablePreviousButton() {
    $('.page-link:contains("Previous")').removeClass('disabled');
}
$("#addBtn").click(function () {
    $.ajax({
        url: "/locationlevel/addlocationlevel",
        type: "GET",
        contentType: "html",
        success: function (data) {
            $(".modal-title").text("Tambah Level Lokasi");
            $(".modal-body").html(data);
            $(".modal").modal("show");
        }
    });
});

function editlocationLevel(id) {
    sessionStorage.setItem('currentPage', currentPage); // Simpan nilai currentPage ke sessionStorage
    $.ajax({
        url: "/locationlevel/editlocationlevel/" + id,
        type: "GET",
        contentType: "html",
        success: function (data) {
            $(".modal-title").text("Edit Level Lokasi");
            $(".modal-body").html(data);
            $(".modal").modal("show");
        }
    });
}


function deletelocationLevel(id) {
    console.log(id);
    $.ajax({
        url: "/locationlevel/deletelocationlevel/" + id,
        type: "GET",
        contentType: "html",
        success: function (data) {
            $(".modal-title").text("Delete Level Lokasi");
            $(".modal-body").html(data);
            $('.modal').modal('show');
        }
    });
}

function SearchLocationLevel(request) {
    if (request && request.length > 0) {
        $.ajax({
            url: "/api/searchlevellocation/" + request,
            type: "GET",
            contentType: 'application/json',
            success: function (result) {
                if (result && Array.isArray(result) && result.length > 0) {
                    $("#locationLevelTBody").empty();

                    for (let i = 0; i < result.length; i++) {
                        $("#locationLevelTBody").append(
                            `<tr>
                                <td>${result[i].name}</td>
                                <td>${result[i].abbreviation}</td>
                                <td>
                                    <button value="${result[i].id}" onClick="editlocationLevel(${result[i].id})" class="btn btn-warning">
                                        <i class="bi-pencil-square"></i>
                                    </button>
                                    <button value="${result[i].id}" onClick="deletelocationLevel(${result[i].id})" class="btn btn-danger red-button" style="background-color: red;">
                                        <i class="bi-trash"></i>
                                    </button>
                                </td>
                            </tr>`
                        );
                    }
                } else {
                    $("#locationLevelTBody").html(
                        `<tr>
                            <td colspan='3' class='text-center'>No Data</td>
                        </tr>`
                    );
                }
            },
            error: function (error) {
                console.error('Error occurred:', error);
            }
        });
    } else {
        getLocationLevelbyPage(0, 5);
    }
}

$(document).ready(function () {
    let pageSize = 5;
    let currentPage = sessionStorage.getItem('currentPage');
    if (!currentPage) {
        currentPage = 0;
    }

    // Ensure currentPage is a number, not a string 'null'
    currentPage = parseInt(currentPage);

    getLocationLevelbyPage(currentPage, pageSize);

    $('body').on('click', 'a.page-link', function (event) {
        event.preventDefault();

        let text = $(this).text();
        if (text === 'Previous' && currentPage > 0) {
            currentPage--;
        } else if (text === 'Next') {
            currentPage++;
        } else {
            currentPage = parseInt(text) - 1;
        }
        getLocationLevelbyPage(currentPage, pageSize);
    });
});


