$(document).ready(function() {
    $("#cancelButton").click(function() {
        $(".modal").modal("hide");
    });

function getCurrentPageFromURL() {
    var urlParams = new URLSearchParams(window.location.search);
    return urlParams.get('page');
}
    $("#addButton").click(function () {
        var namaLocation = $("#nameInput").val().trim();
        var singkatanLocation = $("#singkatInput").val().trim();
        var refererURL = getCurrentPageFromURL(); // Mendapatkan nomor halaman dari URL
        sessionStorage.setItem('currentPage', refererURL);

        if (namaLocation === "") {
            $("#errName").text("Nama Level Lokasi tidak boleh kosong");
            return;
        } else if (/^\s+$/.test(namaLocation)) {
            $("#errName").text("Nama Level Lokasi hanya berisi spasi");
            return;
        } else {
            $("#errName").text("");
        }

        var obj = {
            name: namaLocation,
            abbreviation: singkatanLocation
        };
        var myJson = JSON.stringify(obj);

        // Tambahkan logic berdasarkan lastPageCount
        var lastPageCount = ...; // Ambil jumlah data pada halaman terakhir dari sumber data (misal: dari backend)

        if (lastPageCount === 5) {
            // Halaman terakhir penuh, tambahkan data baru dan redirect ke halaman selanjutnya
            $.ajax({
                url: "/api/addlocationlevel",
                type: "POST",
                contentType: "application/json",
                data: myJson,
                async:true,
                success: function(data, textStatus, xhr) {
                    $(".modal").modal("hide");
                    // Redirect ke halaman berikutnya
                    var nextPage = parseInt(refererURL) + 1;
                    window.location.href = '/locationlevel?page=' + nextPage;
                },
                error: function(xhr, textStatus, errorThrown) {
                    if (xhr.status === 400) {
                        var errorMessage = xhr.responseText;
                        $("#errName").text(errorMessage);
                    } else {
                        alert("Terjadi kesalahan: " + errorThrown);
                    }
                }
            });
        } else {
            // Halaman terakhir masih memiliki slot kosong, tambahkan data baru dan redirect ke halaman terakhir
            $.ajax({
                url: "/api/addlocationlevel",
                type: "POST",
                contentType: "application/json",
                data: myJson,
                async:true,
                success: function(data, textStatus, xhr) {
                    $(".modal").modal("hide");
                    // Redirect ke halaman terakhir
                    window.location.href = '/locationlevel?page=' + lastPageCount;
                },
                error: function(xhr, textStatus, errorThrown) {
                    if (xhr.status === 400) {
                        var errorMessage = xhr.responseText;
                        $("#errName").text(errorMessage);
                    } else {
                        alert("Terjadi kesalahan: " + errorThrown);
                    }
                }
            });
        }
    });

