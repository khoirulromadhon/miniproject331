$(document).ready(function() {
    getLocationLevelId();
});

function getCurrentPageFromURL() {
    var urlParams = new URLSearchParams(window.location.search);
    return urlParams.get('page');
}

function getLocationLevelId() {
    var id = $("#delId").val();
    console.log("Mengambil data dengan ID:", id);

    // Debugging dengan 'debugger;'
//    debugger; // Kode akan berhenti di sini untuk memeriksa variabel dan eksekusi kode di console browser

    $.ajax({
        url: "/api/locationlevel/" + id,
        type: "GET",
        contentType: "application/json",
        success: function(data) {
            var deleteConfirmation = $("#deleteConfirmation");
            if (data && data.name) {
                deleteConfirmation.text(deleteConfirmation.text() + data.name + "?");
            }
        }
    });
}


$("#cancelBtn").click(function() {
    $(".modal").modal("hide");
});

$("#deleteBtn").click(function() {
    var id = $("#delId").val();
    console.log("Menghapus data dengan ID:", id);
    var refererURL = getCurrentPageFromURL(); // Mendapatkan nomor halaman dari URL
    sessionStorage.setItem('currentPage', refererURL);
    console.log("Nilai refererURL sebelum redirect:", refererURL);

    $.ajax({
        url: "/api/locationlevel/" + id,
        type: "DELETE",
        contentType: "application/json",
        async:true,
        success: function(data, textStatus, xhr) {
            $(".modal").modal("hide"); // Tutup modal konfirmasi
            if (refererURL) {
                window.location.href = 'http://localhost:8080/locationlevel?page=' + refererURL;
            } else {
                var defaultPageURL = 'http://localhost:8080/locationlevel?page=0';
                window.location.href = defaultPageURL;
            }
        },
        error: function(xhr, textStatus, errorThrown) {
            if (xhr.status === 400) {
                var errorMessage = xhr.responseText;
                $("#errName").text(errorMessage);
            } else {
                alert("Terjadi kesalahan: " + errorThrown);
            }
        }
    });
//    debugger;
});
