$(document).ready(function() {
	GetMCourierTypeById();
	opsiReload();
})

function opsiReload(){
        var kurirSel = document.getElementById("opsiKurir");

        $.ajax({
            url : "/api/getallmcourier",
            type : "GET",
            contentType : "application/json",
            async : false,
            success: function(data){
                for(i = 0; i<data.length; i++){
                    kurirSel.options[kurirSel.options.length] = new Option(data[i].name, data[i].id);
                }
            }
        });
}

function GetMCourierTypeById() {
	var id = $("#EditMCourierTypeId").val();
		console.log(id);
	$.ajax({
		url: "/api/getbyidmcouriertype/" + id,
		type: "GET",
		contentType: "application/json",
		success: function(data) {
			$("#opsiKurir").val(data.id_courier);
			$("#nameInput").val(data.name);
		}
	})
}

$("#editMCourierTypeBtnCancel").click(function() {
	$(".modal").modal("hide")
})

$("#editMCourierTypeBtnCreate").click(function() {
    var id = $("#EditMCourierTypeId").val();
	var id_courier = $("#opsiKurir").val();
    var name = $("#nameInput").val();

	if (id_courier == "") {
		$("#errOpsiKurir").text("Courier Id tidak boleh kosong!");
		if (name == "") {
            	$("#errName").text("Nama tidak boleh kosong!");
            	return;
            } else {
            	$("#errName").text("");
            }
		return;
	} else {
		$("#errOpsiKurir").text("");
	}

	if (name == "") {
    	$("#errName").text("Nama tidak boleh kosong!");
    	return;
    } else {
    	$("#errName").text("");
    }

MCourierType(function(mCourierType) {
        var isDataExist = mCourierType.some(function(item) {
            // Mengonversi ke string sebelum memanggil toLowerCase()
            var itemCourierId = String(item.id_courier).toLowerCase();
            var itemName = String(item.name).toLowerCase();
            return itemCourierId === id_courier.toLowerCase() && itemName === name.toLowerCase();
        });

        if (isDataExist) {
            $("#errName").text("Data sudah ada!");
            $("#errOpsiKurir").text("Courier ID sudah ada!");
            return;
        } else {
            $("#errName").text("");
            $("#errOpsiKurir").text("");
        }

	var obj = {};
	obj.id = id;
	obj.id_courier = id_courier;
    obj.name = name;

	var myJson = JSON.stringify(obj);

        $.ajax({
            url: "/api/editmcouriertype/" + id,
            type: "PUT",
            contentType: "application/json",
            data: myJson,
            success: function(data) {
                    $(".modal").modal("hide")
                    ReloadData();
                    MCourierTypeList(0,5);
            },
            error: function() {
                alert("Terjadi kesalahan")
            }
		});
	});
})

function MCourierType(callback) {
    $.ajax({
        url: "/api/getallmcouriertype",
        type: "GET",
        contentType: "application/json",
        success: function(mCourierType) {
            if (callback && typeof callback === "function") {
                callback(mCourierType);
            }
        },
        error: function(error) {
            if (callback && typeof callback === "function") {
                callback(error);
            }
        }
    });
}