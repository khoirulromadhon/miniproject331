$("#addMCourierBtnCancel").click(function(){
	$(".modal").modal("hide")
})

$(document).ready(function(){
        	opsiReload();
})

function opsiReload(){
        var kurirSel = document.getElementById("opsiKurir");

        $.ajax({
            url : "/api/getallmcourier",
            type : "GET",
            contentType : "application/json",
            success: function(data){
                for(i = 0; i<data.length; i++){
                    kurirSel.options[kurirSel.options.length] = new Option(data[i].name, data[i].id);
                }
            }
        });
}

$("#addMCourierBtnCreate").click(function(){
    var courierId = $("#opsiKurir").val();
    var name = $("#nameInput").val();

    if (courierId == "") {
        $("#errOpsiKurir").text("Opsi tidak boleh kosong!");
        if (name == "") {
            $("#errName").text("Nama Kurir tidak boleh kosong!");
        } else {
            $("#errName").text("");
        }
        return;
    } else {
        $("#errOpsiKurir").text("");
    }

    if (name == "") {
        $("#errName").text("Nama Kurir tidak boleh kosong!");
        return;
    } else {
        $("#errName").text("");
    }

    MCourierType(function(mCourierType) {
        var isDataExist = mCourierType.some(function(item) {
            // Mengonversi ke string sebelum memanggil toLowerCase()
            var itemCourierId = String(item.id_courier).toLowerCase();
            var itemName = String(item.name).toLowerCase();
            return itemCourierId === courierId.toLowerCase() && itemName === name.toLowerCase();
        });

        if (isDataExist) {
            $("#errName").text("Data sudah ada!");
            $("#errOpsiKurir").text("Courier ID sudah ada!");
            return;
        } else {
            $("#errName").text("");
            $("#errOpsiKurir").text("");
        }

        var obj = {};
        obj.id_courier = courierId;
        obj.name = name;

        var myJson = JSON.stringify(obj);

        $.ajax({
            url: "/api/addmcouriertype",
            type: "POST",
            contentType: "application/json",
            data: myJson,
            success: function(data) {
                $(".modal").modal("hide");
                location.reload();
                GetAllMCourierType();
            },
            error: function() {
                alert("Terjadi kesalahan");
            }
        });
    });
});


function MCourierType(callback) {
    $.ajax({
        url: "/api/getallmcouriertype",
        type: "GET",
        contentType: "application/json",
        success: function(mCourierType) {
            if (callback && typeof callback === "function") {
                callback(mCourierType);
            }
        },
        error: function(error) {
            if (callback && typeof callback === "function") {
                callback(error);
            }
        }
    });
}