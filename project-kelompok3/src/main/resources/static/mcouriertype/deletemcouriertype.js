$(document).ready(function() {
	GetMCourierTypeById();

})

var createdOn;
var createdBy;

function GetMCourierTypeById() {
	var id = $("#deleteMCourierTypeId").val();
	console.log(id);
	$.ajax({
		url: "/api/getbyidmcouriertype/" + id,
		type: "GET",
		contentType: "application/json",
		success: function(data) {
		    //console.log(data.name);
			$("#nameInput").text(data.name);
		    createdBy = data.createdBy;
            createdOn = data.createdOn;
		}
	})
}

$("#deleteMCourierTypeBtnCancel").click(function() {
	$(".modal").modal("hide")
})

$("#deleteMCourierTypeBtnDelete").click(function() {
	var id = $("#deleteMCourierTypeId").val();
	$.ajax({
		url : "/api/deletemcouriertype/" + id,
		type : "DELETE",
		contentType : "application/json",
		success: function(data){
            $(".modal").modal("hide")
            ReloadData();
		},
		error: function(){
			alert("Terjadi kesalahan")
		}
	});
})
