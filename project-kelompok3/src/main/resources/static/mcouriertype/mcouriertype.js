//function GetAllMCourierType(){
//	$("#mCourierTypeList").html(
//		`<thead>
//			<tr>
//				<th>Nama Kurir</th>
//				<th><button value="name" onClick="getPageableTable(this.value)" class="button container-fluid">Name Jenis Kurir</button></th>
//				<th>Action</th>
//			</tr>
//		</thead>
//		<tbody id="mcourierTypeTBody"></tbody>
//		`
//	);
//
//	$.ajax({
//		url : "/api/getallmcouriertype",
//		type : "GET",
//		contentType : "application/json",
//		success: function(data){
//			for(i = 0; i<data.length; i++){
//				$("#MCourierTypeTBody").append(
//					`
//					<tr>
//						<td>${data[i].courierId}</td>
//						<td>${data[i].name}</td>
//
//						<td>
//                            <button value="${data[i].id}" onClick="editMCourierType(this.value)" style="background-color:#445A67; border-color:#778899; color:white; width: 120px; height: 50px;">
//                                <i class=> Sunting Data </i>
//                            </button>
//                            <button value="${data[i].id}" onClick="deleteMCourierType(this.value)" style="background-color:#57838D; border-color:#8B0000; color:white; width: 120px; height: 50px;">
//                                <i class=> Hapus Data </i>
//                            </button>
//                        </td>
//
//					</tr>
//
//					`
//				)
//			}
//		}
//	});
//}
//

var currentPagePublic;
var lengthPublic;

function MCourierTypeList(currentPage, length) {
    currentPagePublic = currentPage;
    lengthPublic = length;
	$.ajax({
		url : '/api/mcouriertypemapped?page=' + currentPage + '&size=' + length,
		type : 'GET',
		contentType : 'application/json',
		success : function(data) {

			let table = '<select style="margin-left:4mm;" class="custom-select mt-3" id="size" onchange="MCourierTypeList(0,this.value)">'
			table += '<option value="3" selected>..</option>'
			table += '<option value="5">5</option>'
			table += '<option value="10">10</option>'
			table += '<option value="15">15</option>'
			table += '</select>'
			table += '<table class="table table-bordered mt-3">';
			table += '<th><button value="m_courier" onClick="getPageableTable(' + currentPage + ',' + length + ',' + 'this.value)" class="btn container-fluid btn-transparent fw-bold text-start text-center">Courier    ↨</button></th>'
			table += '<th><button value="name" onClick="getPageableTable(' + currentPage + ',' + length + ',' + 'this.value)" class="btn container-fluid btn-transparent fw-bold text-start text-center">Name Jenis Courier    ↨</button></th>'
			table += "<th class='text-center'>Action</th>"
			table += "</tr>"
		for (let i = 0; i < data.mCourierType.length; i++) {
		    console.log(data)
			table += "<tr>";
//			table += "<td class='text-center'>" + ((i + 1) + (data.currentPage * length)) + "</td>";
			table += "<td class='text-center'>" + data.mCourierType[i].mCourier.name + "</td>";
			table += "<td class='text-center'>" + data.mCourierType[i].name + "</td>";
			table += "<td class='text-center'><button style='background-color:#445A67; border-color:#778899; color:white; width: 120px; height: 30px;' class='btn btn-primary btn-sm' value='" + data.mCourierType[i].id + "' onclick=editMCourierType(this.value) >Sunting Data</button> <button style='background-color:#DC3545; border-color:#DC3545; color:white; width: 120px; height: 30px;' class='btn btn-primary btn-sm' value='" + data.mCourierType[i].id + "' onclick=deleteMCourierType(this.value) >Hapus Data</button></td>";
//			table += "<td><button style='background-color:#DC3545; border-color:#DC3545; color:white; width: 120px; height: 30px;' class='btn btn-primary btn-sm' value='" + data.mCourierType[i].id + "' onclick=deleteMCourierType(this.value) >Hapus Data</button></td>";
			// table += "<td><input type='checkbox' onclick='SelectedItem()' class='mychecked' id='listdelete' value='"+data.category[i].id+"'></td>"
			table += "</tr>";
				}
				table += "</table>";
				table += "<br>"
				table += '<nav aria-label="Page navigation" style="margin-left:4mm;">';
				table += '<ul class="pagination">'
				table += '<li class="page-item"><a class="page-link" onclick="MCourierTypeList(' + (data.currentPage - 1) + ',' + length + ')">Previous</a></li>'
				let index = 1;
//				for (let i = 0; i < data.totalPages; i++) {
//					table += '<li class="page-item"><a id="pageslink" class="page-link" onclick="MCourierTypeList(' + i + ',' + length + ')">' + index + '</a></li>'
//					index++;
//					}
				table += '<li class="page-item"><a class="page-link" onclick="MCourierTypeList(' + (data.currentPage + 1) + ',' + length + ')">Next</a></li>'
				table += '</ul>'
				table += '</nav>';
				$('#MCourierTypeList').html(table);
		}
	});
}

$("#addBtn").click(function(){
	$.ajax({
		url: "/mcouriertype/addmcouriertype",
		type: "GET",
		contentType: "html",
		success: function(data){
			$(".modal-title").text("Create New McourierType");
			$(".modal-body").html(data);
			$(".modal").modal("show");
		}
	});
})

function editMCourierType(id){
	$.ajax({
		url: "/mcouriertype/editmcouriertype/" + id,
		type: "GET",
		contentType: "html",
		success: function(data){
			$(".modal-title").text("Edit Courier Type");
			$(".modal-body").html(data);
			$(".modal").modal("show");
		}
	});
}

function deleteMCourierType(id){
	$.ajax({
		url: "/mcouriertype/deletemcouriertype/" + id,
		type: "GET",
		contentType: "html",
		success: function(data){
			$(".modal-title").text("Delete Courier Type");
			$(".modal-body").html(data);
			$(".modal").modal("show");
		}
	});
}

function SearchMCourierType(request) {
				//console.log(request)

				if (request.length > 0)
				{
					$.ajax({
						url: '/api/searchmcouriertype/' + request,
						type: 'GET',
						contentType: 'application/json',
						success: function (result) {
							//console.log(result)
							let table = "<table class='table table-bordered mt-3'>";
						    table += "<th class='text-center'>Courier</th>"
						    table += "<th class='text-center'>Name Courier</th>"
						    table += "<th class='text-center'>Action</th>"
						    table += "</tr>"
							if (result.length > 0)
							{
								for (let i = 0; i < result.length; i++) {
									table += "<tr>";
//									table += "<td class='text-center'>" + (i+1) + "</td>";
									table += "<td class='text-center'>" + result[i].mCourier.name + "</td>";
                                    table += "<td class='text-center'>" + result[i].name + "</td>";
                                    table += "<td class='text-center'><button style='background-color:#445A67; border-color:#778899; color:white; width: 120px; height: 30px;' class='btn btn-primary btn-sm' value='" + result[i].id + "' onclick=editMCourierType(this.value) >Sunting Data</button>  <button style='background-color:#DC3545; border-color:#DC3545; color:white; width: 120px; height: 30px;' class='btn btn-primary btn-sm' value='" + result[i].id + "' onclick=deleteMCourierType(this.value) >Hapus Data</button></td>";
//							        table += "<td><button style='background-color:#DC3545; border-color:#DC3545; color:white; width: 120px; height: 30px;' class='btn btn-primary btn-sm' value='" + result[i].id + "' onclick=deleteMCourierType(this.value) >Hapus Data</button></td>";
							        table += "</tr>";
								}
							} else {
								table += "<tr>";
								table += "<td colspan='4' class='text-center'>No data</td>";
								table += "</tr>";
							}
							table += "</table>";
							$('#MCourierTypeList').html(table);
						}
					});
				} else {
					MCourierTypeList(0,5);
				}
			}

var isAsc = true;
function getPageableTable(currentPage, length, name) {
//console.log(name);

        $.ajax({
                url : '/api/mcouriertypesort?page=' + currentPage + '&size=' + length + '&name=' + name + '&isAsc=' + isAsc,
                type : "GET",
                contentType : "application/json",
                success : function(data) {
                //console.log(data);
                isAsc = isAsc == true ? false : true;
                    let table = '<select style="margin-left:4mm;" class="custom-select mt-3" id="size" onchange="MCourierTypeList(' + 0 + ',' + 5 + ',' + name + ')">'
                    table += '<option value="3" selected>..</option>'
                    table += '<option value="5">5</option>'
                    table += '<option value="10">10</option>'
                    table += '<option value="15">15</option>'
                    table += '</select>'
                    table += '<table class="table table-bordered mt-3">';
                    table += '<th><button value="m_courier" onClick="getPageableTable(' + currentPage + ',' + length + ',' + 'this.value)" class="btn container-fluid btn-transparent fw-bold text-start text-center">Courier    ↨</button></th>'
                    table += '<th><button value="name" onClick="getPageableTable(' + currentPage + ',' + length + ',' + 'this.value)" class="btn container-fluid btn-transparent fw-bold text-start text-center">Name Jenis Courier    ↨</button></th>'
                    table += "<th class='text-center'>Action</th>"
                    table += "</tr>"
                    console.log(data);
                for (let i = 0; i < data.mCourierType.length; i++)
                {
                    table += "<tr>";
//                    table += "<td class - 'text-center'>" + ((i+1) + (data.currentPage * length)) + "</td>"
                    table += "<td class='text-center'>" + data.mCourierType[i].mCourier.name + "</td>";
                    table += "<td class='text-center'>" + data.mCourierType[i].name + "</td>";
                    table += "<td class='text-center'><button style='background-color:#445A67; border-color:#778899; color:white; width: 120px; height: 30px;' class='btn btn-primary btn-sm' value='" + data.mCourierType[i].id + "' onclick=editMCourierType(this.value) >Sunting Data</button>  <button style='background-color:#DC3545; border-color:#57838D; color:white; width: 120px; height: 30px;' class='btn btn-primary btn-sm' value='" + data.mCourierType[i].id + "' onclick=deleteMCourierType(this.value) >Hapus Data</button></td>";
//                    table += "<td><button style='background-color:#DC3545; border-color:#57838D; color:white; width: 120px; height: 30px;' class='btn btn-primary btn-sm' value='" + data.mCourierType[i].id + "' onclick=deleteMCourierType(this.value) >Hapus Data</button></td>";
                    table += "</tr>";
                }
                table += "</table>";
                table += "<br>"
                table += '<nav aria-label="Page navigation" style="margin-left:4mm;">';
                table += '<ul class="pagination">'
                table += '<li class="page-item"><a class="page-link" onclick="MCourierTypeList(' + (data.currentPage - 1) + ',' + length + ')">Previous</a></li>'
                let index = 1;
//                for (let i = 0; i < data.totalPages; i++) {
//                	table += '<li class="page-item"><a id="pageslink" class="page-link" onclick="MCourierTypeList(' + i + ',' + length + ')">' + index + '</a></li>'
//                	index++;
//                }
                table += '<li class="page-item"><a class="page-link" onclick="MCourierTypeList(' + (data.currentPage + 1) + ',' + length + ')">Next</a></li>'
                table += '</ul>'
                table += '</nav>';
                $('#MCourierTypeList').html(table);
                }
        })
}

function ReloadData(){
MCourierTypeList(currentPagePublic, lengthPublic);
}

$(document).ready(function(){
//	GetAllPenerbit();
MCourierTypeList(0,5);
})