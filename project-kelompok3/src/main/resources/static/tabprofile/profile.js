const imagePath = $('#imagePath').val();
function fetchProfileImage(imagePath) {
    $.ajax({
        url: 'api/getImage/' + imagePath,
        method: 'GET',
        responseType: 'arraybuffer',
        success: function(response) {
            const base64String = btoa(
                new Uint8Array(response).reduce(function(data, byte) {
                    return data + String.fromCharCode(byte);
                }, '')
            );
            $('.profile-photo').attr('src', 'data:image/jpeg;base64,' + base64String);
        },
        error: function(xhr, status, error) {
            console.error(error);
        }
    });
}

// Panggil fetchProfileImage() ketika dokumen siap
$(document).ready(function() {
    console.log("path gambar = ", imagePath);
    fetchProfileImage();
});
