$(document).ready(function(){
    getSession();
})

function getSession(){
    $.ajax({
        url: "/api/user/session",
        type: "GET",
        contentType: "application/json",
        success: function(data){
            if(data.role_id === 0){
                $("#containerUser").html(
                    `
                    <div class="col-auto">
                        <button id="daftarButton" class="btn btn-light content-end" type="button">
                                Daftar
                        </button>
                    </div>
                    <div class="col-auto">
                        <button id="loginButton" class="btn btn-info content-end" type="button">
                                Login
                        </button>
                    </div>
                    `
                );
                $("#daftarButton").click(function(){
                    $.ajax({
                        url: "/register",
                        type: "GET",
                        contentType: "html",
                        success: function(data){
                            $(".modal-title").text("Daftar");
                            $(".modal-body").html(data);
                            $(".modal").modal("show");
                        }
                    });
                })

                $("#loginButton").click(function(){
                    $.ajax({
                        url: "/login",
                        type: "GET",
                        contentType: "html",
                        success: function(data){
                            $(".modal-title").text("Masuk");
                            $(".modal-body").html(data);
                            $(".modal").modal("show");
                        }
                    });
                })
            }else{
                getUserName(data.user_id);
            }
        }
    });
}
