function getAllPayment(){
	$("#paymentMethodTable").html(
		`<thead>
			<tr>
				<th>ID</th>
				<th><button value = "name" onClick = "getPageableTable(this.value)" class = "button container-fluid btn-transparent border-none">Payment Method</button></th>
				<th>Code</th>
				<th>Action</th>
			</tr>
		</thead>
		<tbody id="paymentTBody"></tbody>
		`
	);


	$.ajax({
		url : "/api/getallpayment",
		type : "GET",
		contentType : "application/json",
		success: function(data){
			for(i = 0; i<data.length; i++){
				$("#paymentTBody").append(
					`
					<tr>
						<td>${data[i].id}</td>
						<td>${data[i].name}</td>
						<td>
                            <button value="${data[i].id}" onClick="editPayment(this.value)" style="background-color:#445A67; border-color:#778899; color:white; width: 120px; height: 50px;">
                                <i class=> Sunting Data </i>
                            </button>
                            <button value="${data[i].id}" onClick="deletePayment(this.value)" style="background-color:#57838D; border-color:#8B0000; color:white; width: 120px; height: 50px;">
                                <i class=> Hapus Data </i>
                            </button>
                        </td>

					</tr>

					`
				)
			}
		}
	});
}

function PaymentMethodList(currentPage, length) {
	$.ajax({
		url : '/api/paymentmapped?page=' + currentPage + '&size=' + length,
		type : 'GET',
		contentType : 'application/json',
		success : function(data) {

			let table = '<select class="custom-select mt-3" id="size" onchange="PaymentMethodList(0,this.value)">'
			table += '<option value="3" selected>..</option>'
			table += '<option value="5">5</option>'
			table += '<option value="10">10</option>'
			table += '<option value="15">15</option>'
			table += '</select>'
			table += '<table class="table table-bordered mt-3">';
			table += '<tr> <th width="10%" class="text-center">ID</th> <th><button value = "name" onClick = "getPageableTable(' + currentPage + ',' + length + ',' + 'this.value)" class = "button container-fluid bg-transparent border-none">Payment Method</button></th> <th>Action</th> </tr>'
//			console.log(data);
		for (let i = 0; i < data.paymentMethod.length; i++) {
			table += "<tr>";
			table += "<td class='text-center'>" + ((i + 1) + (data.currentPage * length)) + "</td>";
//			table += "<td>" + data.paymentMethod[i].id + "</td>";
			table += "<td>" + data.paymentMethod[i].name + "</td>";
			table += "<td><button style='background-color:#445A67; border-color:#778899; color:white; width: 120px; height: 30px;' class='btn btn-primary btn-sm' value='" + data.paymentMethod[i].id + "' onclick=editPayment(this.value) >Sunting Data</button></td>";
			table += "<td><button style='background-color:#57838D; border-color:#57838D; color:white; width: 120px; height: 30px;' class='btn btn-primary btn-sm' value='" + data.paymentMethod[i].id + "' onclick=deletePayment(this.value) >Hapus Data</button></td>";
			// table += "<td><input type='checkbox' onclick='SelectedItem()' class='mychecked' id='listdelete' value='"+data.category[i].id+"'></td>"
			table += "</tr>";
				}
				table += "</table>";
				table += "<br>"
				table += '<nav aria-label="Page navigation">';
				table += '<ul class="pagination">'
				table += '<li class="page-item"><a class="page-link" onclick="PaymentMethodList(' + (data.currentPage - 1) + ',' + length + ')">Previous</a></li>'
				let index = 1;
				for (let i = 0; i < data.totalPages; i++) {
					table += '<li class="page-item"><a id="pageslink" class="page-link" onclick="PaymentMethodList(' + i + ',' + length + ')">' + index + '</a></li>'
					index++;
					}
					table += '<li class="page-item"><a class="page-link" onclick="PaymentMethodList(' + (data.currentPage + 1) + ',' + length + ')">Next</a></li>'
					table += '</ul>'
					table += '</nav>';
					$('#paymentMethodList').html(table);
		}
	});
}

$("#addBtn").click(function(){
	$.ajax({
		url: "/payment_method/addpayment",
		type: "GET",
		contentType: "html",
		success: function(data){
			$(".modal-title").text("Create New Payment Method");
			$(".modal-body").html(data);
			$(".modal").modal("show");
		}
	});
})

function editPayment(id){
	$.ajax({
		url: "/payment_method/editpayment/" + id,
		type: "GET",
		contentType: "html",
		success: function(data){
			$(".modal-title").text("Edit Payment Method");
			$(".modal-body").html(data);
			$(".modal").modal("show");
		}
	});
}

function deletePayment(id){

	$.ajax({
		url: "/payment_method/deletepayment/" + id,
		type: "GET",
		contentType: "html",
		success: function(data){
			$(".modal-title").text("Delete Payment Method");
			$(".modal-body").html(data);
			$(".modal").modal("show");
		}
	});
}

//function SearchPenerbit(request) {
//
//    if(request.length > 0)
//    {
//        $.ajax({
//            url: '/api/searchpenerbit/' + request,
//            type: 'GET',
//            contentType: 'application/json',
//            success: function (result) {
//                if(result.length > 0)
//                {
//                    for(i = 0; i < result.length; i++){
//                    $("#penerbitTBody").html(
//                    `
//                    <tr>
//                        <td>${result[i].idPenerbit}</td>
//                        <td>${result[i].namaPenerbit}</td>
//                        <td>${result[i].tahunBerdiri}</td>
//                        <td>${result[i].asalPenerbit}</td>
//                        <td>${result[i].nomorTelfon}</td>
//
//                        <td>
//                            <button value="${result[i].idPenerbit}" onClick="editPenerbit(this.value)" style="background-color:#445A67; border-color:#778899; color:white; width: 120px; height: 50px;">
//                                <i class> Sunting Data </i>
//                            </button>
//                            <button value="${result[i].idPenerbit}" onClick="deletePenerbit(this.value)" style="background-color:#57838D; border-color:#8B0000; color:white; width: 120px; height: 50px;">
//                                <i class> Hapus Data </i>
//                            </button>
//                        </td>
//
//                    </tr>
//                    `)
//                    }
//                }
//                else {
//                    $("#penerbitTBody").html(
//                    `<tr>
//                    <td colspan='6' class='test-center'>No Data</td>
//                    </tr>
//                    `)
//                }
//            }
//        });
//    }
//    else {
//        GetAllPenerbit();
//    }
//}

function SearchPayment(request) {
				//console.log(request)

				if (request.length > 0)
				{
					$.ajax({
						url: '/api/searchpayment/' + request,
						type: 'GET',
						contentType: 'application/json',
						success: function (result) {
							//console.log(result)
							let table = "<table class='table table-bordered mt-3'>";
						    table += "<tr> <th width='10%' class='text-center'>ID</th> <th>Name</th> <th>Action</th> </tr>"
							if (result.length > 0)
							{
								for (let i = 0; i < result.length; i++) {
									table += "<tr>";
									table += "<td class='text-center'>" + (i+1) + "</td>";
									table += "<td>" + result[i].id + "</td>";
                                    table += "<td>" + result[i].name + "</td>";
                                    table += "<td><button style='background-color:#445A67; border-color:#778899; color:white; width: 120px; height: 30px;' class='btn btn-primary btn-sm' value='" + result[i].id + "' onclick=editPayment(this.value) >Sunting Data</button></td>";
							        table += "<td><button style='background-color:#57838D; border-color:#57838D; color:white; width: 120px; height: 30px;' class='btn btn-primary btn-sm' value='" + result[i].id + "' onclick=deletePayment(this.value) >Hapus Data</button></td>";
							        table += "</tr>";
								}
							} else {
								table += "<tr>";
								table += "<td colspan='4' class='text-center'>No data</td>";
								table += "</tr>";
							}
							table += "</table>";
							$('#paymentMethodList').html(table);
						}
					});
				} else {
					PaymentMethodList(0,5);
				}
			}


var isAsc = true;
function getPageableTable(currentPage, length, name) {
console.log(name);

        $.ajax({
                url : '/api/paymentsort?page=' + currentPage + '&size=' + length + '&name=' + name + '&isAsc=' + isAsc,
                type : "GET",
                contentType : "application/json",
                success : function(data) {
                console.log(data);
                isAsc = isAsc == true ? false : true;
                    let table = '<select class="custom-select mt-3" id="size" onchange="PaymentMethodList(' + 0 + ',' + 5 + ',' + name + ')">'
                    table += '<option value="3" selected>..</option>'
                    table += '<option value="5">5</option>'
                    table += '<option value="10">10</option>'
                    table += '<option value="15">15</option>'
                    table += '</select>'
                    table += '<table class="table table-bordered mt-3">';
                    table += '<tr> <th width="10%" class="text-center">Nomor</th> <th><button value="name" onClick="getPageableTable(' + currentPage + ',' + length + ',' + 'this.value)" class="button container-fluid bg-transparent border-none">Payment Method</button></th> <th>Action</th> </tr>'
                    console.log(data);
                for (let i = 0; i < data.paymentMethod.length; i++)
                {
                    table += "<tr>";
                    table += "<td class - 'text-center'>" + ((i+1) + (data.currentPage * length)) + "</td>"
//                    table += "<td>" + data.paymentMethod[i].id + "</td>";
                    table += "<td>" + data.paymentMethod[i].name + "</td>";
                    table += "<td><button style='background-color:#445A67; border-color:#778899; color:white; width: 120px; height: 30px;' class='btn btn-primary btn-sm' value='" + data.paymentMethod[i].id + "' onclick=editPayment(this.value) >Sunting Data</button></td>";
                    table += "<td><button style='background-color:#57838D; border-color:#57838D; color:white; width: 120px; height: 30px;' class='btn btn-primary btn-sm' value='" + data.paymentMethod[i].id + "' onclick=deletePayment(this.value) >Hapus Data</button></td>";
                    table += "</tr>";
                }
                table += "</table>";
                table += "<br>"
                table += '<nav aria-label="Page navigation">';
                table += '<ul class="pagination">'
                table += '<li class="page-item"><a class="page-link" onclick="MBankList(' + (data.currentPage - 1) + ',' + length + ')">Previous</a></li>'
                let index = 1;
                for (let i = 0; i < data.totalPages; i++) {
                	table += '<li class="page-item"><a id="pageslink" class="page-link" onclick="PaymentMethodList(' + i + ',' + length + ')">' + index + '</a></li>'
                	index++;
                }
                	table += '<li class="page-item"><a class="page-link" onclick="PaymentMethodList(' + (data.currentPage + 1) + ',' + length + ')">Next</a></li>'
                	table += '</ul>'
                	table += '</nav>';
                	$('#paymentMethodList').html(table);
                }
        })
}

$(document).ready(function(){
//	getAllBank();
	PaymentMethodList(0,5);
})