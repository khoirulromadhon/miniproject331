$("#addPaymentBtnCancel").click(function(){
    $(".modal").modal("hide")
})

$("#addPaymentBtnCreate").click(function(){
    var name = $("#nameInput").val();

    if(name == "") {
        $("#errName").text("Name is Empty!");
        return;
    } else {
        $("#errName").text("");
    }

    var obj = {};
    obj.name = name;

    var myJson = JSON.stringify(obj);

    $.ajax({
        url : "/api/addpayment",
        type : "POST",
        contentType : "application/json",
        data : myJson,
        success : function(data){
            $(".modal").modal("hide")
            location.reload();
            getAllPayment();
        },
        error : function(){
            alert("Have a Problem")
        }
    });
})