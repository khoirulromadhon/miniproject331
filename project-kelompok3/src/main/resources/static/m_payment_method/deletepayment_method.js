$(document).ready(function() {
	getPaymentById();

})
var createdBy;
var createdOn;

function getPaymentById() {
var id = $("#delPaymentId").val();
console.log("Mengambil id :"+id);
	$.ajax({
		url: "/api/getPaymentById/" + id,
		type: "GET",
		contentType: "application/json",
		success: function(data) {
		    //console.log(data);
		    createdBy = data.createdBy;
		    createdOn = data.createdOn;
			$("#nameDel").text(data.name);
		}
	})
}

$("#delCancelBtn").click(function() {
	$(".modal").modal("hide")
})

$("#delDeleteBtn").click(function() {
	var id = $("#delPaymentId").val();
	$.ajax({
		url : "/api/deletepayment/"+ id,
		type : "DELETE",
		contentType : "application/json",
		success: function(data){
				$(".modal").modal("hide")
				location.reload();
				GetAllPayment();

		},
		error: function(){
			alert("Terjadi kesalahan")
		}
	});
})