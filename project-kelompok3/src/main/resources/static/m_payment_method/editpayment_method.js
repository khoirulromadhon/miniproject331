$(document).ready(function() {
	getPaymentById();

})

function getPaymentById() {
	var id = $("#editPaymentId").val();
	$.ajax({
		url: "/api/getPaymentById/" + id,
		type: "GET",
		contentType: "application/json",
		success: function(data) {
			$("#nameInput").val(data.name);
		}
	});
}

$("#editPaymentBtnCancel").click(function() {
	$(".modal").modal("hide")
})

$("#editPaymentBtnCreate").click(function() {
    var id = $("#editPaymentId").val();
	var name = $("#nameInput").val();

	if (name == "") {
		$("#errName").text("Nama tidak boleh kosong!");
		return;
	} else {
		$("#errName").text("");
	}


	var obj = {};
	obj.name = name;


	var myJson = JSON.stringify(obj);

	$.ajax({
		url: "/api/editpayment/" + id,
		type: "PUT",
		contentType: "application/json",
		data: myJson,
		success: function(data) {
				$(".modal").modal("hide")
				location.reload();
				getAllPayment();
		},
		error: function() {
			alert("Terjadi kesalahan")
		}
	});
})
