$(document).ready(function() {
	GetCustomerRelationlById();

})

function GetCustomerRelationlById() {
	var id = $("#updateCustomerRelationId").val();
	$.ajax({
		url: "/api/customerRelation/" + id,
		type: "GET",
		contentType: "application/json",
		success: function(data) {
			$("#nameInput").val(data.name);
		}
	})
}

$("#editBtnCancel").click(function() {
	$(".modal").modal("hide")
})

$("#editBtnCreate").click(function() {
	var id = $("#updateCustomerRelationId").val();
	var name = $("#nameInput").val().trim();

	if (name == "") {
		$("#errName").text("Nama tidak boleh kosong!");
		return;
	} else {
		$("#errName").text("");
	}

	CustomerRelation(function(customerRelation) {
        var isNameExist = customerRelation.some(function(item) {
            return item.name.toLowerCase() === name.toLowerCase();
        });

        if (isNameExist) {
            $("#errName").text("Nama sudah ada!");
            return;
        } else {
            $("#errName").text("");
        }

        var obj = { name };
        var myJson = JSON.stringify(obj);

        $.ajax({
            url: "/api/customerRelation/" + id,
            type: "PUT",
            contentType: "application/json",
            data: myJson,
            success: function(data) {
                    $(".modal").modal("hide")
                    GetAllCustomerRelation();
            },
            error: function() {
                alert("Terjadi kesalahan")
            }
        });
    });
});

function CustomerRelation(callback) {
    $.ajax({
        url: "/api/customerRelation",
        type: "GET",
        contentType: "application/json",
        success: function(customerRelation) {
            if (callback && typeof callback === "function") {
                callback(customerRelation);
            }
        },
        error: function(error) {
            if (callback && typeof callback === "function") {
                callback(error);
            }
        }
    });
}