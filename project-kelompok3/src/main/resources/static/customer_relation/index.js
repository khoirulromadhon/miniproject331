function GetAllCustomerRelation(){
    $("#customerRelation").html(
        `<thead>
            <tr>
                <th>Nama</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody id="customerRelationTBody"></tbody>
        `
    );

    $.ajax({
        url : "/api/customerRelation",
        type : "GET",
        contentType : "application/json",
        success: function(data){
            for(i = 0; i<data.length; i++){
                $("#customerRelationTBody").append(
                    `
                    <tr>
                        <td>${data[i].name}</td>
                        <td>
                            <button value="${data[i].id}" onClick="editCustomerRelation(this.value)" class="btn btn-warning">
                                <i class="bi-pencil-square"></i>
                            </button>
                            <button value="${data[i].id}" onClick="deleteCustomerRelation(this.value)" class="btn btn-danger">
                                <i class="bi-trash"></i>
                            </button>
                        </td>
                    </tr>
                    `
                )
            }
        }
    });
}

$("#addBtn").click(function(){
	$.ajax({
		url: "/customerRelation/create",
		type: "GET",
		contentType: "html",
		success: function(data){
			$(".modal-title").text("Create");
			$(".modal-body").html(data);
			$(".modal").modal("show");
		}
	});
})

function editCustomerRelation(id){
	$.ajax({
		url: "/customerRelation/update/" + id,
		type: "GET",
		contentType: "html",
		success: function(data){
			$(".modal-title").text("Update");
			$(".modal-body").html(data);
			$(".modal").modal("show");
		}
	});
}

$("#delBtnCancel").click(function() {
	$(".modal").modal("hide")
})

function deleteCustomerRelation(id){
	$.ajax({
		url: "/customerRelation/delete/" + id,
		type: "DELETE",
		contentType: "html",
		success: function(data){
			$(".modal-title").text("Delete");
			$(".modal-body").html(data);
			$(".modal").modal("show");
		}
	});
}

function SearchCustomerRelation(request){
if (request.length > 0){
    $.ajax({
        url: '/api/searchCustomerRelation=' + request,
        type: 'GET',
        contentType: 'application/json',
        success: function (result) {
            let table = "<table class='table table-stripped mt-3'>";
            table += "<tr> <th width='60%'>Nama</th> <th>Action</th>"
            if (result.length > 0){
                for (let i = 0; i < result.length; i++) {
                    table += "<tr>";
                    table += "<td>" + result[i].name + "</td>";
                    table += "<td><button class='btn btn-warning' value='" + result[i].id + "' onclick=editCustomerRelation(this.value)><i class='bi-pencil-square'></i></button> <button class='btn btn-danger' value='" + result[i].id + "' onclick=deleteCustomerRelation(this.value)><i class='bi-trash'></i></button></td>";
                    table += "</tr>";
                }
            } else {
                table += "<tr>";
                table += "<td colspan='2' class='text-center'>No data</td>";
                table += "</tr>";
            }
            table += "</table>";
            $('#customerRelation').html(table);
        }
    });
	} else {
		GetAllCustomerRelation();
	}

}

$(document).ready(function(){
	GetAllCustomerRelation();
})