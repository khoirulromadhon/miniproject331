$(document).ready(function() {
	GetCustomerRelationlById();

})

function GetCustomerRelationlById() {
	var id = $("#deleteCustomerRelationlId").val();
	$.ajax({
		url: "/api/customerRelation/" + id,
		type: "GET",
		contentType: "application/json",
		success: function(data) {
			$("#confirm").html(
                `
                    <tr>
                        <td>Anda akan menghapus ${data.name} ?</td>
                    </tr>
                `
            );
		}
	})
}

$("#deleteBtnCancel").click(function() {
	$(".modal").modal("hide")
})

$("#deleteBtn").click(function() {
	var id = $("#deleteCustomerRelationlId").val();
	$.ajax({
		url : "/api/customerRelation/"+ id,
		type : "DELETE",
		contentType : "application/json",
		success: function(data){
            $(".modal").modal("hide")
            GetAllCustomerRelation();
		},
		error: function(){
			alert("Terjadi kesalahan")
		}
	});
})