function registerOnLogin(){
    $.ajax({
        url: "/register",
        type: "GET",
        contentType: "html",
        success: function(data){
            $(".modal-title").text("Daftar");
            $(".modal-body").html(data);
            $(".modal").modal("show");
        }
    });
}

function lupapassword(){
    $.ajax({
        url: "/lupapassword",
        type: "GET",
        contentType: "html",
        success: function(data){
            $(".modal-title").text("Lupa Password");
            $(".modal-body").html(data);
            $(".modal").modal("show");
        }
    });
}

