$(document).ready(function() {
	GetMCourierById();

})

var createdOn;
var createdBy;

function GetMCourierById() {
	var id = $("#deleteMCourierId").val();
	console.log(id);
	$.ajax({
		url: "/api/getbyidmcourier/" + id,
		type: "GET",
		contentType: "application/json",
		success: function(data) {
		    console.log(data.name);
			$("#nameDel").text(data.name);
		    createdBy = data.createdBy;
            createdOn = data.createdOn;
		}
	})
}

$("#deleteMCourierBtnCancel").click(function() {
	$(".modal").modal("hide")
})

$("#deleteMCourierBtnDelete").click(function() {
	var id = $("#deleteMCourierId").val();
	$.ajax({
		url : "/api/deletemcourier/" + id,
		type : "DELETE",
		contentType : "application/json",
		success: function(data){
            $(".modal").modal("hide")
            ReloadData();
		},
		error: function(){
			alert("Terjadi kesalahan")
		}
	});
})
