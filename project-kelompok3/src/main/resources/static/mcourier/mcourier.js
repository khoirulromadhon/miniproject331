//function GetAllMCourier(){
//	$("#mCourierList").html(
//		`<thead>
//			<tr>
//				<th>Id Courier</th>
//				<th>Name Courier</th>
//				<th>Action</th>
//			</tr>
//		</thead>
//		<tbody id="mcourierTBody"></tbody>
//		`
//	);
//
//	$.ajax({
//		url : "/api/getallmcourier",
//		type : "GET",
//		contentType : "application/json",
//		success: function(data){
//			for(i = 0; i<data.length; i++){
//				$("#MCourierTBody").append(
//					`
//					<tr>
//						<td>${data[i].id}</td>
//						<td>${data[i].name}</td>
//
//						<td>
//                            <button value="${data[i].id}" onClick="editMCourier(this.value)" style="background-color:#445A67; border-color:#778899; color:white; width: 120px; height: 50px;">
//                                <i class=> Sunting Data </i>
//                            </button>
//                            <button value="${data[i].id}" onClick="deleteMCourier(this.value)" style="background-color:#57838D; border-color:#8B0000; color:white; width: 120px; height: 50px;">
//                                <i class=> Hapus Data </i>
//                            </button>
//                        </td>
//
//					</tr>
//
//					`
//				)
//			}
//		}
//	});
//}
var currentPagePublic;
var lengthPublic;

function MCourierList(currentPage, length) {
    currentPagePublic = currentPage;
    lengthPublic = length;
	$.ajax({
		url : '/api/mcouriermapped?page=' + currentPage + '&size=' + length,
		type : 'GET',
		contentType : 'application/json',
		success : function(data) {

//			let table = '<select class="custom-select mt-3" id="size" onchange="MCourierList(0,this.value)">'
//			table += '<option value="3" selected>..</option>'
//			table += '<option value="5">5</option>'
//			table += '<option value="10">10</option>'
//			table += '<option value="15">15</option>'
//			table += '</select>'
			let table = "<table class='table table-bordered mt-3' id='size' onchange='MCourierList(0,this.value)'>";
			table += "<tr>"
			table += "<th class='text-center'>Di Ubah Oleh</th>"
			table += "<th class='text-center'>Name Courier</th>"
			table += "<th class='text-center'>Action</th>"
			table += "</tr>"
		for (let i = 0; i < data.mCourier.length; i++) {
			table += "<tr>";
//			table += "<td class='text-center'>" + ((i + 1) + (data.currentPage * length)) + "</td>";
//			table += "<td>" + data.mCourier[i].id + "</td>";
            console.log(data.mCourier[i]);
            table += "<td class='text-center'>" + data.mCourier[i].modified_by + "</td>";
			table += "<td class='text-center'>" + data.mCourier[i].name + "</td>";
			table += "<td class='text-center'><button style='background-color:#445A67; border-color:#778899; color:white; width: 120px; height: 30px;' class='btn btn-primary btn-sm' value='" + data.mCourier[i].id + "' onclick=editMCourier(this.value) >Sunting Data</button> <button style='background-color:#DC3545; border-color:#DC3545; color:white; width: 120px; height: 30px;' class='btn btn-primary btn-sm' value='" + data.mCourier[i].id + "' onclick=deleteMCourier(this.value) >Hapus Data</button></td>";
//			table += "<td><button style='background-color:#DC3545; border-color:#DC3545; color:white; width: 120px; height: 30px;' class='btn btn-primary btn-sm' value='" + data.mCourier[i].id + "' onclick=deleteMCourier(this.value) >Hapus Data</button></td>";
			// table += "<td><input type='checkbox' onclick='SelectedItem()' class='mychecked' id='listdelete' value='"+data.category[i].id+"'></td>"
			table += "</tr>";
				}
				table += "</table>";
				table += "<br>"
				table += '<nav aria-label="Page navigation">';
				table += '<ul class="pagination">'
				table += '<li class="page-item"><a class="page-link" onclick="MCourierList(' + (data.currentPage - 1) + ',' + length + ')">Previous</a></li>'
				let index = 1;
//				for (let i = 0; i < data.totalPages; i++) {
//					table += '<li class="page-item"><a id="pageslink" class="page-link" onclick="MCourierList(' + i + ',' + length + ')">' + index + '</a></li>'
//					index++;
//					}
				table += '<li class="page-item"><a class="page-link" onclick="MCourierList(' + (data.currentPage + 1) + ',' + length + ')">Next</a></li>'
				table += '</ul>'
				table += '</nav>';
				$('#mCourierList').html(table);
		}
	});
}

$("#addBtn").click(function(){
	$.ajax({
		url: "/mcourier/addmcourier",
		type: "GET",
		contentType: "html",
		success: function(data){
			$(".modal-title").text("Create New Mcourier");
			$(".modal-body").html(data);
			$(".modal").modal("show");
		}
	});
})

function editMCourier(id){
	$.ajax({
		url: "/mcourier/editmcourier/" + id,
		type: "GET",
		contentType: "html",
		success: function(data){
			$(".modal-title").text("Edit Courier");
			$(".modal-body").html(data);
			$(".modal").modal("show");
		}
	});
}

function deleteMCourier(id){
	$.ajax({
		url: "/mcourier/deletemcourier/" + id,
		type: "GET",
		contentType: "html",
		success: function(data){
			$(".modal-title").text("Delete Courier");
			$(".modal-body").html(data);
			$(".modal").modal("show");
		}
	});
}

function SearchMCourier(request) {
				//console.log(request)

				if (request.length > 0)
				{
					$.ajax({
						url: '/api/searchmcourier/' + request,
						type: 'GET',
						contentType: 'application/json',
						success: function (result) {
							//console.log(result)
							let table = "<table class='table table-bordered mt-3'>";
						    table += "<tr>"
						    table += "<th class='text-center'>Di Ubah Oleh</th>"
                            table += "<th class='text-center'>Name Courier</th>"
                            table += "<th class='text-center'>Action</th>"
                            table += "</tr>"
							if (result.length > 0)
							{
								for (let i = 0; i < result.length; i++) {
									table += "<tr>";
//									table += "<td class='text-center'>" + (i+1) + "</td>";
//									table += "<td>" + result[i].id + "</td>";
                                    table += "<td class='text-center'>" + result[i].modified_by + "</td>";
                                    table += "<td class='text-center'>" + result[i].name + "</td>";
                                    table += "<td><button style='background-color:#445A67; border-color:#778899; color:white; width: 120px; height: 30px;' class='btn btn-primary btn-sm' value='" + result[i].id + "' onclick=editMCourier(this.value) >Sunting Data</button> <button style='background-color:#DC3545; border-color:#DC3545; color:white; width: 120px; height: 30px;' class='btn btn-primary btn-sm' value='" + result[i].id + "' onclick=deleteMCourier(this.value) >Hapus Data</button></td>";
//							        table += "<td><button style='background-color:#DC3545; border-color:#DC3545; color:white; width: 120px; height: 30px;' class='btn btn-primary btn-sm' value='" + result[i].id + "' onclick=deleteMCourier(this.value) >Hapus Data</button></td>";
							        table += "</tr>";
								}
							} else {
								table += "<tr>";
								table += "<td colspan='4' class='text-center'>No data</td>";
								table += "</tr>";
							}
							table += "</table>";
							$('#mCourierList').html(table);
						}
					});
				} else {
					MCourierList(0,5);
				}
			}

function ReloadData(){
MCourierList(currentPagePublic, lengthPublic);
}

function ModifedBy(){
}

$(document).ready(function(){
//	GetAllPenerbit();
MCourierList(0,5);
})