$(document).ready(function() {
	GetMCourierById();

})

function GetMCourierById() {
	var id = $("#EditMCourierId").val();
	$.ajax({
		url: "/api/getbyidmcourier/" + id,
		type: "GET",
		contentType: "application/json",
		success: function(data) {
			$("#nameInput").val(data.name);
		}
	})
}

$("#editMCourierBtnCancel").click(function() {
	$(".modal").modal("hide")
})

$("#editMCourierBtnCreate").click(function() {
    var id = $("#EditMCourierId").val();
	var name = $("#nameInput").val();

	if (name == "") {
		$("#errName").text("Curier tidak boleh kosong!");
		return;
	} else {
		$("#errName").text("");
	}

	MCourier(function(mCourier) {
        var isNameExist = mCourier.some(function(item) {
            return item.name.toLowerCase() === name.toLowerCase();
        });

        if (isNameExist) {
            $("#errName").text("Nama sudah ada!");
            return;
        } else {
            $("#errName").text("");
        }

	var obj = {};
	obj.id = id;
	obj.name = name;

	var myJson = JSON.stringify(obj);

        $.ajax({
            url: "/api/editmcourier/" + id,
            type: "PUT",
            contentType: "application/json",
            data: myJson,
            success: function(data) {
                    $(".modal").modal("hide")
                    ReloadData();
                    GetAllMCourier();
            },
            error: function() {
                alert("Terjadi kesalahan")
            }
		});
	});
});

function MCourier(callback) {
    $.ajax({
        url: "/api/getallmcourier",
        type: "GET",
        contentType: "application/json",
        success: function(mCourier) {
            if (callback && typeof callback === "function") {
                callback(mCourier);
            }
        },
        error: function(error) {
            if (callback && typeof callback === "function") {
                callback(error);
            }
        }
    });
}