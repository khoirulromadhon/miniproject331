package projectkelompok3.projectkelompok3.Models;

import javax.persistence.*;

@Entity
@Table(name = "t_reset_password")
public class T_Reset_Password extends BaseProperties{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;

    @Column(name = "old_password", nullable = true, length = 255)
    private String old_password;

    @Column(name = "new_password", nullable = true, length = 255)
    private String new_password;

    @Column(name = "reset_for", nullable = true, length = 20)
    private String reset_for;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getOld_password() {
        return old_password;
    }

    public void setOld_password(String old_password) {
        this.old_password = old_password;
    }

    public String getNew_password() {
        return new_password;
    }

    public void setNew_password(String new_password) {
        this.new_password = new_password;
    }

    public String getReset_for() {
        return reset_for;
    }

    public void setReset_for(String reset_for) {
        this.reset_for = reset_for;
    }
}
