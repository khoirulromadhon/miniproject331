package projectkelompok3.projectkelompok3.Models;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

@Entity
@Table(name = "m_customer")
public class MCustomer extends BaseProperties {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Biodata getBiodata() {
        return biodata;
    }

    public void setBiodata(Biodata biodata) {
        this.biodata = biodata;
    }

    public Date getDob() {
        return dob;
    }

    public void setDob(Date dob) {
        this.dob = dob;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public MBloodGroup getBloodGroup() {
        return bloodGroup;
    }

    public void setBloodGroup(MBloodGroup bloodGroup) {
        this.bloodGroup = bloodGroup;
    }

    public String getRhesusType() {
        return rhesusType;
    }

    public void setRhesusType(String rhesusType) {
        this.rhesusType = rhesusType;
    }

    public BigDecimal getHeight() {
        return height;
    }

    public void setHeight(BigDecimal height) {
        this.height = height;
    }

    public BigDecimal getWeight() {
        return weight;
    }

    public void setWeight(BigDecimal weight) {
        this.weight = weight;
    }

    @ManyToOne
    @JoinColumn(name = "biodata_id", referencedColumnName = "id")
    private Biodata biodata;

    private Date dob;

    @Column(length = 1)
    private String gender;

    @ManyToOne
    @JoinColumn(name = "blood_group_id", referencedColumnName = "id")
    private MBloodGroup bloodGroup;

    @Column(length = 5)
    private String rhesusType;

    private BigDecimal height;

    private BigDecimal weight;

}
