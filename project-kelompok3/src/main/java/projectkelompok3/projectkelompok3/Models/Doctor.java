package projectkelompok3.projectkelompok3.Models;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "m_doctor")
public class Doctor extends BaseProperties {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "biodata_id", insertable = false, updatable = false)
    private Biodata biodata;

    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "doctor_id")
    private List<CurrentDoctorSpecialization> currentSpecialization;
    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "doctor_id")
    private List<DoctorOffice> doctorOffice;

    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "doctor_id")
    private List<DoctorTreatment> doctorTreatment;
    @Column(name = "biodata_id", nullable = true)
    private Long biodataId;

    @Column(name = "str", length = 50)
    private String str;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Biodata getBiodata() {
        return biodata;
    }

    public void setBiodata(Biodata biodata) {
        this.biodata = biodata;
    }

    public Long getBiodataId() {
        return biodataId;
    }

    public void setBiodataId(Long biodataId) {
        this.biodataId = biodataId;
    }

    public String getStr() {
        return str;
    }

    public void setStr(String str) {
        this.str = str;
    }

    public List<CurrentDoctorSpecialization> getCurrentSpecialization() {
        return currentSpecialization;
    }

    public void setCurrentSpecialization(List<CurrentDoctorSpecialization> currentSpecialization) {
        this.currentSpecialization = currentSpecialization;
    }

    public List<DoctorOffice> getDoctorOffice() {
        return doctorOffice;
    }

    public void setDoctorOffice(List<DoctorOffice> doctorOffice) {
        this.doctorOffice = doctorOffice;
    }

    public List<DoctorTreatment> getDoctorTreatment() {
        return doctorTreatment;
    }

    public void setDoctorTreatment(List<DoctorTreatment> doctorTreatment) {
        this.doctorTreatment = doctorTreatment;
    }
}
