package projectkelompok3.projectkelompok3.Models;


import javax.persistence.*;

@Entity
@Table(name = "m_courier_type")
public class MCourierType extends BaseProperties{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private long Id;

    @ManyToOne
    @JoinColumn(name = "m_courier",insertable = false, updatable = false)
    public MCourier mCourier;
    @Column(name = "m_courier", nullable = true)
    private Long id_courier;

    @Column(name = "name", nullable = true, length = 20)
    private String Name;


    public long getId() {
        return Id;
    }

    public void setId(long id) {
        Id = id;
    }

    public MCourier getmCourier() {
        return mCourier;
    }

    public void setmCourier(MCourier mCourier) {
        this.mCourier = mCourier;
    }

    public Long getId_courier() {
        return id_courier;
    }

    public void setId_courier(Long id_courier) {
        this.id_courier = id_courier;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }
}
