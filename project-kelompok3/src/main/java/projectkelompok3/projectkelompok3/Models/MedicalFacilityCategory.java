package projectkelompok3.projectkelompok3.Models;

import javax.persistence.*;

@Entity
@Table(name = "m_medical_facility_category")
public class MedicalFacilityCategory extends BaseProperties{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;

    @Column(name = "medical_facility_name", nullable = true, length = 50)
    private String medicalFacilityName;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMedicalFacilityName() {
        return medicalFacilityName;
    }

    public void setMedicalFacilityName(String medicalFacilityName) {
        this.medicalFacilityName = medicalFacilityName;
    }
}
