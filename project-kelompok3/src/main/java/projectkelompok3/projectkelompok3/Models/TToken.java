package projectkelompok3.projectkelompok3.Models;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "t_token")
public class TToken extends BaseProperties{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "email", length = 100)
    private String email;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private MUser mUser;

    @Column(name = "token", length = 50)
    private String token;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "expired_on")
    private Date expiredOn;

    @Column(name = "is_expired")
    private Boolean isExpired;

    @Column(name = "used_for", nullable = true, length = 20)
    private String used_for;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public MUser getmUser() {
        return mUser;
    }

    public void setmUser(MUser mUser) {
        this.mUser = mUser;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public Date getExpiredOn() {
        return expiredOn;
    }

    public void setExpiredOn(Date expiredOn) {
        this.expiredOn = expiredOn;
    }

    public Boolean getExpired() {
        return isExpired;
    }

    public void setExpired(Boolean expired) {
        isExpired = expired;
    }

    public String getUsed_for() {
        return used_for;
    }

    public void setUsed_for(String used_for) {
        this.used_for = used_for;
    }
}
//t_token,