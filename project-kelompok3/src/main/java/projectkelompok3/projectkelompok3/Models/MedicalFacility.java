package projectkelompok3.projectkelompok3.Models;

import javax.persistence.*;

@Entity
@Table(name = "m_medical_facility")
public class MedicalFacility extends BaseProperties{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;

    @Column(name = "medical_facility_name", nullable = true, length = 50)
    private String medicalFacilityName;

    @ManyToOne
    @JoinColumn(name = "medical_facility_category_id", insertable = false, updatable = false)
    public MedicalFacilityCategory medicalFacilityCategory;

    @Column(name = "medical_facility_category_id", nullable = true)
    private long medicalFacilityCategoryId;

    @ManyToOne
    @JoinColumn(name = "location_id", insertable = false, updatable = false)
    public MLocation mLocation;

    @Column(name = "location_id", nullable = true)
    private long locationId;

    @Column(name = "full_address", nullable = true, columnDefinition = "TEXT")
    private String fullAddress;

    @Column(name = "email", nullable = true, length = 100)
    private String email;

    @Column(name = "phone_code", nullable = true, length = 10)
    private String phoneCode;

    @Column(name = "phone", nullable = true, length = 15)
    private String phone;

    @Column(name = "fax", nullable = true, length = 15)
    private String fax;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMedicalFacilityName() {
        return medicalFacilityName;
    }

    public void setMedicalFacilityName(String medicalFacilityName) {
        this.medicalFacilityName = medicalFacilityName;
    }

    public MedicalFacilityCategory getMedicalFacilityCategory() {
        return medicalFacilityCategory;
    }

    public void setMedicalFacilityCategory(MedicalFacilityCategory medicalFacilityCategory) {
        this.medicalFacilityCategory = medicalFacilityCategory;
    }

    public long getMedicalFacilityCategoryId() {
        return medicalFacilityCategoryId;
    }

    public void setMedicalFacilityCategoryId(long medicalFacilityCategoryId) {
        this.medicalFacilityCategoryId = medicalFacilityCategoryId;
    }

    public MLocation getmLocation() {
        return mLocation;
    }

    public void setmLocation(MLocation mLocation) {
        this.mLocation = mLocation;
    }

    public long getLocationId() {
        return locationId;
    }

    public void setLocationId(long locationId) {
        this.locationId = locationId;
    }

    public String getFullAddress() {
        return fullAddress;
    }

    public void setFullAddress(String fullAddress) {
        this.fullAddress = fullAddress;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhoneCode() {
        return phoneCode;
    }

    public void setPhoneCode(String phoneCode) {
        this.phoneCode = phoneCode;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }
}
