package projectkelompok3.projectkelompok3.Models;
import javax.persistence.*;

@Entity
@Table(name = "m_blood_group")
public class MBloodGroup extends BaseProperties{
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "code", length = 5)
    private String code;

    @Column(name = "description", length = 255)
    private String description;
}
