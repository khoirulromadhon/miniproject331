package projectkelompok3.projectkelompok3.Models;

import javax.persistence.*;

@Entity
@Table(name = "m_location")
public class MLocation extends BaseProperties{
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public MLocation getParentLocation() {
        return parentLocation;
    }

    public void setParentLocation(MLocation parentLocation) {
        this.parentLocation = parentLocation;
    }

    public MLocationLevel getLocationLevel() {
        return locationLevel;
    }

    public void setLocationLevel(MLocationLevel locationLevel) {
        this.locationLevel = locationLevel;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name", length = 100)
    private String name;

    @ManyToOne
    @JoinColumn(name = "parent_id")
    private MLocation parentLocation;

    @ManyToOne
    @JoinColumn(name = "location_level_id")
    private MLocationLevel locationLevel;
}
