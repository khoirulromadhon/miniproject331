package projectkelompok3.projectkelompok3.Models;

import org.hibernate.annotations.Type;

import javax.persistence.*;

@Entity
@Table(name = "m_biodata")
public class Biodata extends BaseProperties{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;

    @Column(name = "fullname", length = 255, nullable = true)
    private String fullname;

    @Column(name = "mobile_phone", length = 15, nullable = true)
    private String mobilePhone;

    @Column(name = "image", nullable = true)
    private byte[] image;

    @Column(name = "image_path", nullable = true)
    private String imagePath;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getMobilePhone() {
        return mobilePhone;
    }

    public void setMobilePhone(String mobilePhone) {
        this.mobilePhone = mobilePhone;
    }

    public byte[] getImage() {
        return image;
    }

    public void setImage(byte[] image) {
        this.image = image;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }
}
