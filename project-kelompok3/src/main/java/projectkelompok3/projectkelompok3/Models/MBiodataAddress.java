package projectkelompok3.projectkelompok3.Models;

import javax.persistence.*;

@Entity
@Table(name = "m_biodata_address")
public class MBiodataAddress extends BaseProperties {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Biodata getBiodata() {
        return biodata;
    }

    public void setBiodata(Biodata biodata) {
        this.biodata = biodata;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getRecipient() {
        return recipient;
    }

    public void setRecipient(String recipient) {
        this.recipient = recipient;
    }

    public String getRecipientPhoneNumber() {
        return recipientPhoneNumber;
    }

    public void setRecipientPhoneNumber(String recipientPhoneNumber) {
        this.recipientPhoneNumber = recipientPhoneNumber;
    }

    public MLocation getLocation() {
        return location;
    }

    public void setLocation(MLocation location) {
        this.location = location;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @ManyToOne
    @JoinColumn(name = "biodata_id", referencedColumnName = "id")
    private Biodata biodata;

    @Column(name = "label", length = 100)
    private String label;

    @Column(name = "recipient", length = 100)
    private String recipient;

    @Column(name = "recipient_phone_number", length = 15)
    private String recipientPhoneNumber;

    @ManyToOne
    @JoinColumn(name = "location_id", referencedColumnName = "id")
    private MLocation location;

    @Column(name = "postal_code", length = 10)
    private String postalCode;

    @Column(name = "address", columnDefinition = "TEXT")
    private String address;
}
