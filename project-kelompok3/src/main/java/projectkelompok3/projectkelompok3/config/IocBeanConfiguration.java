package projectkelompok3.projectkelompok3.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import projectkelompok3.projectkelompok3.Repositories.DoctorTreatmentRepository;
import projectkelompok3.projectkelompok3.Repositories.SearchDoctorRepositories;
import projectkelompok3.projectkelompok3.Repositories.SpecializationRepositories;
import projectkelompok3.projectkelompok3.Service.*;

@Configuration
public class IocBeanConfiguration {
    @Bean
    SearchDoctorService searchDoctorService(SearchDoctorRepositories searchDoctorRepositories){
        return new SearchDoctorImpl(searchDoctorRepositories);
    }
    @Bean
    SpecializatonService specializatonService(SpecializationRepositories specializationRepositories){
        return new SpecializationImpl(specializationRepositories);
    }
    @Bean
    DoctorTreatmentService doctorTreatmentService(DoctorTreatmentRepository doctorTreatmentRepositories){
        return new DoctorTreatmentImpl(doctorTreatmentRepositories);
    }
}
