package projectkelompok3.projectkelompok3.Controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import projectkelompok3.projectkelompok3.Models.Specialization;
import projectkelompok3.projectkelompok3.Service.SpecializatonService;

import java.util.List;

@RestController
@RequestMapping("/api")
public class APISpecialization {
    @Autowired
    private SpecializatonService specializatonService;

    @GetMapping("/specialization")
    public List<Specialization> GetAllSpecialization(){
        return specializatonService.getAllSpecializations();
    }
}
