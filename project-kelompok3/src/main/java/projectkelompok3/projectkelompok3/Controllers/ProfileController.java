package projectkelompok3.projectkelompok3.Controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import projectkelompok3.projectkelompok3.Models.Biodata;
import projectkelompok3.projectkelompok3.Models.MCustomer;
import projectkelompok3.projectkelompok3.Models.MUser;
import projectkelompok3.projectkelompok3.Service.BiodataService;
import projectkelompok3.projectkelompok3.Service.CustomerService;
import projectkelompok3.projectkelompok3.Service.UserService;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping("/profile")
public class ProfileController {

    private final BiodataService biodataService;
    private final UserService userService;
    private final CustomerService customerService;

    @Autowired
    public ProfileController(BiodataService biodataService, UserService userService, CustomerService customerService) {
        this.biodataService = biodataService;
        this.userService = userService;
        this.customerService = customerService;
    }

    @GetMapping("/{id}")
    public String profile(@PathVariable Long id, Model model) {
        MCustomer customer = customerService.getCustomerById(id);

        if (customer != null) {
            Long biodataId = customer.getBiodata().getId();
            Biodata biodata = biodataService.getBiodataById(biodataId);
            Date dob = customer.getDob();

            if (biodata != null) {
                String customerFullName = biodata.getFullname();
                String numberPhone = biodata.getMobilePhone();
                String imagePath = biodata.getImagePath();
                // Mendapatkan informasi user terkait dari biodata
                List<MUser> users = userService.getUserByBiodataId(biodataId);
                MUser user = null;

                if (!users.isEmpty()) {
                    user = users.get(0); // Mengambil MUser pertama dari daftar user
                    String email = user.getEmail();
                    String password = user.getPassword();

                    // Menambahkan informasi user ke model
                    model.addAttribute("email", email);
                    model.addAttribute("password", password);
                }

                model.addAttribute("imagePath", imagePath);
                model.addAttribute("biodata", biodata);
                model.addAttribute("dob", dob);
                model.addAttribute("customerFullName", customerFullName);
                model.addAttribute("numberPhone", numberPhone);
            }
        }
        return "tabprofile/profile";
    }


}
