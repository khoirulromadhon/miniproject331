package projectkelompok3.projectkelompok3.Controllers;


import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class DefaultController {
    @RequestMapping("")
    public String dashboard(){return "dashboard"; }

    @RequestMapping("/login")
    public String login(){
        return "login/login";
    }

    @RequestMapping("/lupapassword")
    public String lupapassword(){
        return "lupapassword/lupapassword";
    }

    @RequestMapping("/register")
    public String register(){
        return "register/register";
    }

    @RequestMapping("/register/otp/{email}")
    public String registerOtp(@PathVariable("email")String email, Model model){
        model.addAttribute(email);
        return "register/otp-register";
    }
    @RequestMapping("/register/setPassword/{email}")
    public String registerPassword(@PathVariable("email") String email, Model model){
        model.addAttribute(email);
        return "register/set-password";
    }

    @RequestMapping("/register/setUserData/{email}/{password}")
    public String registerUserData(@PathVariable("email") String email, @PathVariable("password") String password, Model model) {
        try {
            // ... (kode sebenarnya)

            System.out.println("Email: " + email);
            System.out.println("Password: " + password);

            // Menambahkan atribut ke model jika diperlukan
            model.addAttribute("email", email);
            model.addAttribute("password", password);

            // Mengembalikan nama view yang ingin ditampilkan
            return "register/set-userdata";
        } catch (Exception e) {
            e.printStackTrace();
            // Tambahkan penanganan kesalahan sesuai kebutuhan
            return "error";
        }
    }

    @RequestMapping("/register/info-register")
    public String registerInfo(){
        return "register/info-register";
    }

    @RequestMapping("/lupapassword/otp/{email}")
    public String lupapasswordOtp(@PathVariable("email")String email, Model model){
        model.addAttribute(email);
        return "lupapassword/otp-lupapassword";
    }
    @RequestMapping("/lupapassword/setPassword/{email}")
    public String lupapasswordSet(@PathVariable("email")String email, Model model){
        model.addAttribute(email);
        return "lupapassword/set-lupapassword";
    }
}