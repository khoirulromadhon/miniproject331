package projectkelompok3.projectkelompok3.Controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import projectkelompok3.projectkelompok3.Models.Biodata;
import projectkelompok3.projectkelompok3.Models.MCustomer;
import projectkelompok3.projectkelompok3.Repositories.BiodataRepositories;
import projectkelompok3.projectkelompok3.Repositories.CustomerRepo;

import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@RestController
@CrossOrigin("*")
@RequestMapping("/api")
public class APIBiodataController {
    @Autowired
    private BiodataRepositories biodataRepositories;
    @Autowired
    private CustomerRepo customerRepo;

    @PostMapping("/uploadImage")
    public ResponseEntity<?>AddImage(@RequestParam("uploadImages")MultipartFile[]uploadImages) throws IOException{
        for (MultipartFile multipartFile: uploadImages){
            Biodata biodata = new Biodata();
            biodata.setCreatedBy(Long.valueOf(1));
            biodata.setCreatedOn(new Date());
            biodata.setImagePath(multipartFile.getOriginalFilename());
            biodata.setImage(multipartFile.getBytes());
            biodataRepositories.save(biodata);
        }
        return new ResponseEntity<>("Image Saved", HttpStatus.OK);
    }

    @GetMapping("/getImage/{pathImage}")
    public ResponseEntity<?>GetImage(@PathVariable("pathImage")String pathImage){
        byte[] image = new byte[0];
        image = biodataRepositories.getImage(pathImage).getImage();
        return ResponseEntity.ok().contentType(MediaType.IMAGE_JPEG).body(image);
    }

    @PutMapping("/biodata/{id}")
    public ResponseEntity<Object> SaveBiodata(@RequestBody Biodata biodata, @PathVariable("id") Long id){
        Optional<Biodata> biodataData = this.biodataRepositories.findById(id);
        String image = biodata.getImagePath();
        biodata.setImagePath(image);
        biodata.setCreatedBy(Long.valueOf(1));
        biodata.setCreatedOn(new Date());
        biodata.setModifiedBy(Long.valueOf(1));
        biodata.setModifiedOn(new Date());

        if (biodataData.isPresent()){
            biodata.setId(id);
            this.biodataRepositories.save(biodata);
            ResponseEntity response = new ResponseEntity<>(biodata, HttpStatus.OK);
            return response;
        }
        else {
            return ResponseEntity.noContent().build();
        }
    }

    @PostMapping("/biodata")
    public ResponseEntity<Object> Biodata(@RequestParam("uploadImages")MultipartFile[]uploadImages, @RequestParam("fullname") String fullname, @RequestParam("mobilePhone") String mobilePhone) {
        Biodata biodata = new Biodata();
        try {
            biodata.setCreatedBy(Long.valueOf(1));
            biodata.setCreatedOn(new Date());
            biodata.setFullname(fullname);
            biodata.setMobilePhone(mobilePhone);
            for (MultipartFile multipartFile: uploadImages){
                biodata.setImagePath(multipartFile.getOriginalFilename());
                this.biodataRepositories.save(biodata);
            }
            return new ResponseEntity<>(biodata, HttpStatus.OK);
        }
        catch (Exception exception){
            return new ResponseEntity<>("Fail To Save Data !!!!!", HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/biodata")
    public ResponseEntity<List<Biodata>> GetAllBiodata(){
        try {
            List<Biodata> biodataList = this.biodataRepositories.findAll();
            return new ResponseEntity<>(biodataList, HttpStatus.OK);
        }
        catch (Exception exception){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

//
    //untuk save register
    @PostMapping("/biodata/callId")
    public ResponseEntity<Long> saveBiodataCallId (@RequestBody Biodata m_biodata){
        try {
            m_biodata.setCreatedBy(1L);
            m_biodata.setCreatedOn(new Date());
            this.biodataRepositories.save(m_biodata);
            return new ResponseEntity<>(m_biodata.getId(), HttpStatus.OK);
        }
        catch (Exception exception){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }
//
//
//    @GetMapping("/biodata/callId")
//    public ResponseEntity<Biodata> getBiodataWithDOBByID (@PathVariable Long id) {
//        Optional<Biodata> biodataOptional = biodataRepositories.findById(id);
//        if (biodataOptional.isPresent()) {
//            Biodata biodata = biodataOptional.get();
//            Optional<MCustomer> customerOptional = customerRepo.findByBiodataId(biodata.getId());
//            if (customerOptional.isPresent()) {
//                MCustomer customer = customerOptional.get();
//                customer.setDob(customer.getDob());
//            }
//            return ResponseEntity.ok().body(biodata);
//        } else {
//            return ResponseEntity.notFound().build();
//        }
//    }
}
