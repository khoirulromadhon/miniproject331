package projectkelompok3.projectkelompok3.Controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import projectkelompok3.projectkelompok3.Models.MailStructure;
import projectkelompok3.projectkelompok3.Service.EmailService;

@RestController
@RequestMapping("/api")
public class ApiEmailController {

    @Autowired
    private EmailService emailService;

    @PostMapping("/mail/send/")
    public String sendMailOTP(@RequestParam String mailTo, @RequestBody MailStructure mailStructure) {
        try {
            emailService.sendOTP(mailTo, mailStructure);
            return "Berhasil";
        } catch (Exception exception) {
            return "Otp Gagal";
        }
    }
}
