package projectkelompok3.projectkelompok3.Controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import projectkelompok3.projectkelompok3.Models.T_Reset_Password;
import projectkelompok3.projectkelompok3.Repositories.ResetPasswordRepo;

import java.util.Date;

@RestController
@CrossOrigin("*")
@RequestMapping("/api")
public class ApiResetPasswordController {

    @Autowired
    private ResetPasswordRepo resetPasswordRepo;

    @PostMapping("/resetpassword/saveResetPassword")
    public ResponseEntity<Object> saveResetPassword(@RequestBody T_Reset_Password t_reset_password){
        try {
            t_reset_password.setCreatedBy(1L);
            t_reset_password.setCreatedOn(new Date());
            this.resetPasswordRepo.save(t_reset_password);
            return new ResponseEntity<>("Success", HttpStatus.OK);
        }catch (Exception exception){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }
}
