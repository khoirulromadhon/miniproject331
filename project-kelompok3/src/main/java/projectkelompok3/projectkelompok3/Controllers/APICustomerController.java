package projectkelompok3.projectkelompok3.Controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import projectkelompok3.projectkelompok3.Models.MCustomer;
import projectkelompok3.projectkelompok3.Repositories.CustomerRepo;

import java.util.Date;

@RestController
@RequestMapping("/api")
public class APICustomerController {

    @Autowired
    private CustomerRepo customerRepo;

    @PostMapping("/register/as/code=3")
    public ResponseEntity<Object> saveCustomerByCode(@RequestBody MCustomer m_customer){
        try {
            m_customer.setCreatedBy(1L);
            m_customer.setCreatedOn(new Date());
            this.customerRepo.save(m_customer);
            return new ResponseEntity<>("Success", HttpStatus.OK);
        }
        catch (Exception exception){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }
}
