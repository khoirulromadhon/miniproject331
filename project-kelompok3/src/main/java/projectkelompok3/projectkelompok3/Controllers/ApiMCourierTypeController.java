package projectkelompok3.projectkelompok3.Controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import projectkelompok3.projectkelompok3.Models.Bank;
import projectkelompok3.projectkelompok3.Models.MCourier;
import projectkelompok3.projectkelompok3.Models.MCourierType;
import projectkelompok3.projectkelompok3.Repositories.MCourierTypeRepo;

import java.util.*;

@RestController
@CrossOrigin("*")
@RequestMapping("/api")
public class ApiMCourierTypeController {

    @Autowired
    private MCourierTypeRepo mCourierTypeRepo;

    //mengambil semua data
    @GetMapping("getallmcouriertype")
    public ResponseEntity<List<MCourierType>> GetAllMCourierType(){
        try {
            List<MCourierType> mCourierType = this.mCourierTypeRepo.findAllNotDeleted();
            return new ResponseEntity<>(mCourierType, HttpStatus.OK);
        } catch (Exception exception){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    //menambah data
    @PostMapping("addmcouriertype")
    public ResponseEntity<Object> SaveMCourierType (@RequestBody MCourierType mCourierType){
        try{
            mCourierType.setCreatedBy(Long.valueOf(1));
            mCourierType.setCreatedOn(new Date());
            this.mCourierTypeRepo.save(mCourierType);
            return new ResponseEntity<>(mCourierType, HttpStatus.OK);
        }
        catch (Exception exception){
            return new ResponseEntity<>("failed", HttpStatus.BAD_REQUEST);
        }
    }

    //mencari data by id
    @GetMapping("getbyidmcouriertype/{id}")
    public ResponseEntity<List<MCourierType>> GetMCourierTypeById(@PathVariable("id") Long id){
        try {
            Optional<MCourierType> mCourierType = this.mCourierTypeRepo.findById(id);
            if (mCourierType.isPresent()){
                ResponseEntity rest = new ResponseEntity<>(mCourierType, HttpStatus.OK);
                return rest;
            } else {
                return ResponseEntity.notFound().build();
            }
        }
        catch (Exception exception){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    //edit data
    @PutMapping("editmcouriertype/{id}")
    public ResponseEntity<Object> UpdateMCourierType (@RequestBody MCourierType mCourierType, @PathVariable("id") Long id){
        Optional<MCourierType> MCourierTypeData = this.mCourierTypeRepo.findById(id);
        if (MCourierTypeData.isPresent()){
            mCourierType.setCreatedBy(Long.valueOf(1));
            mCourierType.setCreatedOn(new Date());
            mCourierType.setModifiedBy(Long.valueOf(1));
            mCourierType.setModifiedOn(new Date());
            this.mCourierTypeRepo.save(mCourierType);
            ResponseEntity rest = new ResponseEntity<>("Success", HttpStatus.OK);
            return rest;
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    //delete data
    @DeleteMapping("deletemcouriertype/{id}")
    public String DeleteMcourierType (@PathVariable("id") Long id){
        try {
            MCourierType mCourierType = this.mCourierTypeRepo.findByIdData(id);
            mCourierType.setDeletedBy(Long.valueOf(1));
            mCourierType.setDeleteOn(new Date());
            mCourierType.setDelete(true);
            this.mCourierTypeRepo.save(mCourierType);
            return "ok";
        }
        catch (Exception exception)
        {
            return "Data Not Found";
        }
    }

    //searching
    @GetMapping("/searchmcouriertype/{keyword}")
    public ResponseEntity<List<MCourierType>> SearchMCourierTypeName(@PathVariable("keyword") String keyword)
    {
        if (keyword != null)
        {
            List<MCourierType> mCourierType = this.mCourierTypeRepo.SearchMCourierType(keyword);
            return new ResponseEntity<>(mCourierType, HttpStatus.OK);
        } else {
            List<MCourierType> mCourierType = this.mCourierTypeRepo.findAll();
            return new ResponseEntity<>(mCourierType, HttpStatus.OK);
        }
    }

    //paging
    @GetMapping("/mcouriertypemapped")
    public ResponseEntity<Map<String, Object>> GetAllPage(
            @RequestParam(defaultValue = "0") int page,
            @RequestParam(defaultValue = "5") int size)
    {
        try {
            List<MCourierType> mCourierType = this.mCourierTypeRepo.findAllNotDeleted();

            int start = page * size;
            int end = Math.min((start + size), mCourierType.size());
            List<MCourierType> paginatedMcourierType = mCourierType.subList(start,end);

            Map<String, Object> response = new HashMap<>();
            response.put("mCourierType", paginatedMcourierType);
            response.put("currentPage", page);
            response.put("totalItems", mCourierType.size());
            response.put("totalPages", (int) Math.ceil((double) mCourierType.size() / size));

            return new ResponseEntity<>(response, HttpStatus.OK);
        }

        catch (Exception e)
        {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/mcouriertypesort")
    public ResponseEntity<Map<String, Object>> getSortMapped(
            @RequestParam(defaultValue = "0") int page,
            @RequestParam(defaultValue = "5") int size,
            @RequestParam String name,
            @RequestParam(defaultValue = "true") Boolean isAsc)
    {
        try {
            List<MCourierType> mCourierType = new ArrayList<>();
            Pageable pagingSort;
            if (isAsc){
                pagingSort = PageRequest.of(page, size, Sort.by(name).ascending());
            }else{
                pagingSort = PageRequest.of(page, size, Sort.by(name).descending());
            }
            Page<MCourierType> pageTuts;
            pageTuts = this.mCourierTypeRepo.findAllOrder(pagingSort);
            mCourierType = pageTuts.getContent();

            Map<String, Object> respon = new HashMap<>();
            respon.put("mCourierType", mCourierType);
            respon.put("currentPage", pageTuts.getNumber());
            respon.put("totalItems", pageTuts.getTotalElements());
            respon.put("totalPages", pageTuts.getTotalPages());
            return new ResponseEntity<>(respon, HttpStatus.OK);
        }catch (Exception exception){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

}
