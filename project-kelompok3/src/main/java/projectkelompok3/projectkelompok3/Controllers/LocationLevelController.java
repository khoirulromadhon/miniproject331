package projectkelompok3.projectkelompok3.Controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping(value = "locationlevel")
public class LocationLevelController {
    @RequestMapping("")
    public String locationlevel (){
        return "locationlevel/locationlevel";
    }
    @RequestMapping("addlocationlevel")
    public String addlocationlevel(){
        return "locationlevel/addlocationlevel";
    }
    @RequestMapping("editlocationlevel/{id}")
    public String editlocationlevel(@PathVariable("id") Long id, Model model) {
        model.addAttribute("id", id);
        return "locationlevel/editlocationlevel";
    }

    @RequestMapping("deletelocationlevel/{id}")
    public String deletelocationlevel(@PathVariable("id") Long id, Model model) {
        model.addAttribute("id", id);
        return "locationlevel/deletelocationlevel";
    }
}
