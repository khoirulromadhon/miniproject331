package projectkelompok3.projectkelompok3.Controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping(value = "customerRelation")
public class CustomerRelationController {
    @RequestMapping("")
    public String index(){
        return "customer_relation/index";
    }

    @RequestMapping("/create")
    public String create(){
        return "customer_relation/create";
    }

    @RequestMapping("/update/{id}")
    public String update(@PathVariable("id") Long id, Model model){
        model.addAttribute("id", id);
        return "customer_relation/update";
    }

    @RequestMapping("/delete/{id}")
    public String delete(@PathVariable("id") Long id, Model model){
        model.addAttribute("id", id);
        return "customer_relation/delete";
    }
}
