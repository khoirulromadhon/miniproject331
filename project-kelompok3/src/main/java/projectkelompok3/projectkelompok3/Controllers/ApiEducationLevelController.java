package projectkelompok3.projectkelompok3.Controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import projectkelompok3.projectkelompok3.Models.EducationLevel;
import projectkelompok3.projectkelompok3.Repositories.EducationLevelRepositories;

import java.util.*;

@RestController
@CrossOrigin("*")
@RequestMapping("/api")
public class ApiEducationLevelController {
    @Autowired
    private EducationLevelRepositories educationLevelRepositories;

    @GetMapping("/educationLevelMapped")
    public ResponseEntity<Map<String, Object>> GetAllEducationLevelMapped(@RequestParam(defaultValue = "0") int page, @RequestParam(defaultValue = "5") int size){
        try{
            List<EducationLevel> educationLevelList = new ArrayList<>();
            Pageable paging = PageRequest.of(page, size);

            Page<EducationLevel> pageTuts;

            pageTuts = educationLevelRepositories.findAll(paging);

            educationLevelList = pageTuts.getContent();

            Map<String, Object> response = new HashMap<>();
            response.put("educationLevelList", educationLevelList);
            response.put("currentPage", pageTuts.getNumber());
            response.put("totalItems", pageTuts.getTotalElements());
            response.put("totalPages", pageTuts.getTotalPages());

            return new ResponseEntity<>(response, HttpStatus.OK);
        }
        catch (Exception exception){
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/educationLevel")
    public ResponseEntity<List<EducationLevel>> GetAllEducationalLevel(){
        try {
            List<EducationLevel> educationLevels = this.educationLevelRepositories.findAllNotDeleted();
            return new ResponseEntity<>(educationLevels, HttpStatus.OK);
        }
        catch (Exception exception){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }


    @GetMapping("/educationLevel/{id}")
    public ResponseEntity<List<EducationLevel>> GetEducationLevelById(@PathVariable("id") Long id){
        try{
            Optional<EducationLevel> educationLevel = this.educationLevelRepositories.findById(id);

            if (educationLevel.isPresent()){
                ResponseEntity response = new ResponseEntity<>(educationLevel, HttpStatus.OK);
                return response;
            }
            else {
                return  ResponseEntity.notFound().build();
            }
        }
        catch (Exception exception){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @GetMapping("/searchEducationLevel={keyword}")
    public ResponseEntity<List<EducationLevel>> SearchEducationLevel(@PathVariable("keyword") String keyword){
        try{
            List<EducationLevel> educationLevelList = this.educationLevelRepositories.searchEducationLevel(keyword);
            return new ResponseEntity<>(educationLevelList, HttpStatus.OK);
        }
        catch (Exception exception){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @PostMapping("/educationLevel")
    public ResponseEntity<Object> SaveEducationLevel(@RequestBody EducationLevel educationLevel){
        try {
            educationLevel.setCreatedBy(Long.valueOf(1));
            educationLevel.setCreatedOn(new Date());
            this.educationLevelRepositories.save(educationLevel);
            return new ResponseEntity<>(educationLevel, HttpStatus.OK);
        }
        catch (Exception exception) {
            return new ResponseEntity<>("Fail to save data !!!!!", HttpStatus.BAD_REQUEST);
        }
    }

    @PutMapping("/educationLevel/{id}")
    public ResponseEntity<Object> UpdateEducationLevel(@RequestBody EducationLevel educationLevel, @PathVariable("id") Long id){
        Optional<EducationLevel> educationLevelData = this.educationLevelRepositories.findById(id);
        educationLevel.setCreatedBy(Long.valueOf(1));
        educationLevel.setCreatedOn(new Date());
        educationLevel.setModifiedBy(Long.valueOf(1));
        educationLevel.setModifiedOn(new Date());

        if (educationLevelData.isPresent()){
            educationLevel.setId(id);
            this.educationLevelRepositories.save(educationLevel);
            ResponseEntity response = new ResponseEntity<>("Success Update Data !!!!!", HttpStatus.OK);
            return response;
        }
        else {
            return ResponseEntity.notFound().build();
        }
    }

    @DeleteMapping("/educationLevel/{id}")
    public String DeleteEducationLevel(@PathVariable("id") Long id){
        try {
            EducationLevel educationLevel = this.educationLevelRepositories.findByIdData(id);
            educationLevel.setDeletedBy(Long.valueOf(1));
            educationLevel.setDeleteOn(new Date());
            educationLevel.setDelete(true);
            this.educationLevelRepositories.save(educationLevel);
            return "Success Delete Data !!!!!";
        }
        catch (Exception exception){
            return "Data Not Found !!!!!";
        }
    }
}
