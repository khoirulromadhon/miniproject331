package projectkelompok3.projectkelompok3.Controllers;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import projectkelompok3.projectkelompok3.Models.MCourier;
import projectkelompok3.projectkelompok3.Repositories.MCourierRepo;

import java.util.*;

@RestController
@CrossOrigin("*")
@RequestMapping("/api")
public class ApiMCourierController {

    @Autowired
    private MCourierRepo mCourierRepo;


    //mengambil semua data
    @GetMapping("getallmcourier")
    public ResponseEntity<List<MCourier>> GetAllMCourier(){
        try {
            List<MCourier> mCourier = this.mCourierRepo.findAllNotDeleted();
            return new ResponseEntity<>(mCourier, HttpStatus.OK);
        } catch (Exception exception){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    //menambah data
    @PostMapping("addmcourier")
    public ResponseEntity<Object> SaveMCourier (@RequestBody MCourier mCourier){
        try{
            mCourier.setCreatedBy(Long.valueOf(1));
            mCourier.setCreatedOn(new Date());
            this.mCourierRepo.save(mCourier);
            return new ResponseEntity<>(mCourier, HttpStatus.OK);
        }
        catch (Exception exception){
            return new ResponseEntity<>("failed", HttpStatus.BAD_REQUEST);
        }
    }

    //mencari data by id
    @GetMapping("getbyidmcourier/{id}")
    public ResponseEntity<List<MCourier>> GetMCourierById(@PathVariable("id") Long id){
        try {
            Optional<MCourier> mCourier = this.mCourierRepo.findById(id);
            if (mCourier.isPresent()){
                ResponseEntity rest = new ResponseEntity<>(mCourier, HttpStatus.OK);
                return rest;
            } else {
                return ResponseEntity.notFound().build();
            }
        }
        catch (Exception exception){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    //update data
    @PutMapping("editmcourier/{id}")
    public ResponseEntity<Object> UpdateMCourier (@RequestBody MCourier mCourier, @PathVariable("id") Long id){
        Optional<MCourier> MCourierData = this.mCourierRepo.findById(id);
        if (MCourierData.isPresent()){
            mCourier.setCreatedBy(Long.valueOf(1));
            mCourier.setCreatedOn(new Date());
            mCourier.setModifiedBy(Long.valueOf(1));
            mCourier.setModifiedOn(new Date());
            this.mCourierRepo.save(mCourier);
            ResponseEntity rest = new ResponseEntity<>("Success", HttpStatus.OK);
            return rest;
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    //delete data
    @DeleteMapping("deletemcourier/{id}")
    public String DeleteMcourier (@PathVariable("id") Long id){
        try {
            MCourier mCourier = this.mCourierRepo.findByIdData(id);
            mCourier.setDeletedBy(Long.valueOf(1));
            mCourier.setDeleteOn(new Date());
            mCourier.setDelete(true);
            this.mCourierRepo.save(mCourier);
            return "ok";
        }
        catch (Exception exception)
        {
            return "Data Not Found";
        }
    }

    //searching
    @GetMapping("/searchmcourier/{keyword}")
    public ResponseEntity<List<MCourier>> SearchMCourierName(@PathVariable("keyword") String keyword)
    {
        if (keyword != null)
        {
            List<MCourier> mCourier = this.mCourierRepo.SearchMCourier(keyword);
            return new ResponseEntity<>(mCourier, HttpStatus.OK);
        } else {
            List<MCourier> mCourier = this.mCourierRepo.findAll();
            return new ResponseEntity<>(mCourier, HttpStatus.OK);
        }
    }

    //paging
    @GetMapping("/mcouriermapped")
    public ResponseEntity<Map<String, Object>> GetAllPage(@RequestParam(defaultValue = "0") int page, @RequestParam(defaultValue = "5") int size)
    {
        try {
            List<MCourier> mCourier = new ArrayList<>();
            Pageable pagingSort = PageRequest.of(page, size);

            Page<MCourier> pageTuts;

            pageTuts = mCourierRepo.findAllOrderByAsc(pagingSort);

            mCourier = pageTuts.getContent();

            Map<String, Object> response = new HashMap<>();
            response.put("mCourier", mCourier);
            response.put("currentPage", pageTuts.getNumber());
            response.put("totalItems", pageTuts.getTotalElements());
            response.put("totalPages", pageTuts.getTotalPages());

            return new ResponseEntity<>(response, HttpStatus.OK);
        }

        catch (Exception e)
        {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
