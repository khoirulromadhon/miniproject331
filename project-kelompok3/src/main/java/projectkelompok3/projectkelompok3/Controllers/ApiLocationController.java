package projectkelompok3.projectkelompok3.Controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import projectkelompok3.projectkelompok3.Models.MLocation;
import projectkelompok3.projectkelompok3.Repositories.LocationRepo;

import javax.persistence.NonUniqueResultException;
import java.util.*;

@RestController
@CrossOrigin("*")
@RequestMapping("/api")
public class ApiLocationController {
    @Autowired
    private LocationRepo locationRepo;

    @GetMapping("/getlocation")
    public ResponseEntity<List<MLocation>> GetAlllocation() {
        try {
            List<MLocation> location = this.locationRepo.findAllNotDeleted();
            return new ResponseEntity<>(location, HttpStatus.OK);
        } catch (Exception exception) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @PostMapping("/addlocation")
    public ResponseEntity<Object> Savelocation(@RequestBody MLocation location) {
        try {
            MLocation existingLocation = null;

            if (location.getParentLocation() != null) {
                existingLocation = locationRepo.findByNameAndParentIdAndLocationLevelId(
                        location.getName(),
                        location.getParentLocation().getId(),
                        location.getLocationLevel().getId()
                );
            } else {
                existingLocation = locationRepo.findByNameAndLocationLevelId(
                        location.getName(),
                        location.getLocationLevel().getId()
                );
            }

            if (existingLocation != null) {
                return new ResponseEntity<>("Data lokasi sudah ada.", HttpStatus.BAD_REQUEST);
            }
            location.setCreatedBy(1L);
            location.setCreatedOn(new Date());
            this.locationRepo.save(location);
            return new ResponseEntity<>(location, HttpStatus.OK);
        } catch (NonUniqueResultException e) {
            String errorMessage = "Terjadi kesalahan: Nama lokasi atau level lokasi sudah di gunakan.";
            return new ResponseEntity<>(errorMessage, HttpStatus.BAD_REQUEST);
        } catch (Exception exception) {
            String errorMessage = "Terjadi kesalahan: " + exception.getMessage();
            return new ResponseEntity<>(errorMessage, HttpStatus.BAD_REQUEST);
        }
    }


    @GetMapping("/location/{id}")
    public ResponseEntity<List<MLocation>> GetlocationById(@PathVariable("id") Long id) {
        try {
            Optional<MLocation> location = this.locationRepo.findById(id);

            if (location.isPresent()) {
                ResponseEntity rest = new ResponseEntity<>(location, HttpStatus.OK);
                return rest;
            } else {
                return ResponseEntity.notFound().build();
            }
        } catch (Exception exception) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @PutMapping("/location/{id}")
    public ResponseEntity<Object> Updatelocation(@RequestBody MLocation location, @PathVariable("id") Long id) {
        Optional<MLocation> locationOptional = this.locationRepo.findById(id);

        if (locationOptional.isPresent()) {
            MLocation existinglocation = locationOptional.get();

            Date createOn = existinglocation.getCreatedOn();
            Long createdBy = existinglocation.getCreatedBy();

            if (createOn != null && createdBy != null) {
                existinglocation.setName(location.getName());
                existinglocation.setParentLocation(location.getParentLocation());
                existinglocation.setLocationLevel(location.getLocationLevel());

                existinglocation.setModifiedBy(1L);
                existinglocation.setModifiedOn(new Date());

                try {
                    MLocation existingDuplicateLocation = null;

                    if (location.getParentLocation() != null) {
                        existingDuplicateLocation = locationRepo.findByNameAndParentIdAndLocationLevelIdAndIdNot(
                                location.getName(),
                                location.getParentLocation().getId(),
                                location.getLocationLevel().getId(),
                                id
                        );
                    } else {
                        existingDuplicateLocation = locationRepo.findByNameAndLocationLevelIdAndIdNot(
                                location.getName(),
                                location.getLocationLevel().getId(),
                                id
                        );
                    }

                    if (existingDuplicateLocation != null) {
                        return new ResponseEntity<>("Data lokasi sudah ada.", HttpStatus.BAD_REQUEST);
                    }

                    this.locationRepo.save(existinglocation);
                    ResponseEntity<Object> response = new ResponseEntity<>("Success", HttpStatus.OK);
                    return response;
                } catch (Exception e) {
                    return ResponseEntity.badRequest().body("Terjadi kesalahan: " + e.getMessage());
                }
            } else {
                return ResponseEntity.badRequest().body("Nilai createOn atau createdBy kosong");
            }
        } else {
            return ResponseEntity.notFound().build();
        }
    }


    @GetMapping("/searchlocation/{keyword}")
    public ResponseEntity<List<MLocation>> Searchlocation(@PathVariable("keyword") String keyword) {
        if (keyword != null) {
            List<MLocation> locations = this.locationRepo.searchLocation(keyword);
            return new ResponseEntity<>(locations, HttpStatus.OK);
        } else {
            List<MLocation> locations = this.locationRepo.findAll();
            return new ResponseEntity<>(locations, HttpStatus.OK);
        }
    }

    @GetMapping("/locationmapped")
    public ResponseEntity<Map<String, Object>> GetPage(@RequestParam(defaultValue = "0") int page, @RequestParam(defaultValue = "5") int size) {
        try {
            List<MLocation> locations = this.locationRepo.findAllNotDeleted();

            int start = page * size;
            int end = Math.min((start + size), locations.size());
            List<MLocation> paginatedlocation = locations.subList(start, end);

            Map<String, Object> response = new HashMap<>();
            response.put("location", paginatedlocation);
            response.put("currentPage", page);
            response.put("totalItems", locations.size());
            response.put("totalPages", (int) Math.ceil((double) locations.size() / size));

            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/location/{id}")
    public ResponseEntity<String> deleteLocation(@PathVariable("id") Long id) {
        try {
            MLocation location = this.locationRepo.findByIdData(id);

            // Periksa apakah lokasi ini masih digunakan sebagai parentLocation pada lokasi lain
            List<MLocation> childLocations = locationRepo.findByParentLocation(location);

            if (!childLocations.isEmpty()) {
                return ResponseEntity.badRequest().body("Tidak dapat menghapus lokasi ini karena masih digunakan");
            }

            // Lakukan penghapusan jika tidak ada lokasi lain yang menggunakan lokasi ini sebagai parentLocation
            location.setDeletedBy(1L);
            location.setDelete(true);
            location.setDeleteOn(new Date());
            this.locationRepo.save(location);

            return ResponseEntity.ok("Data lokasi berhasil dihapus");
        } catch (Exception exception) {
            return ResponseEntity.notFound().build();
        }
    }

    @GetMapping("/location/byLevel/{levelId}")
    public ResponseEntity<List<MLocation>> GetLocationsByLevelId(@PathVariable("levelId") Long levelId) {
        try {
            List<MLocation> locations = this.locationRepo.findByLocationLevelId(levelId);
            return new ResponseEntity<>(locations, HttpStatus.OK);
        } catch (Exception exception) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }
}
