package projectkelompok3.projectkelompok3.Controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping(value = "educationLevel")
public class EducationLevelController {
    @RequestMapping("")
    public String index(){
        return "education_level/index";
    }

    @RequestMapping("/create")
    public String create(){
        return "education_level/create";
    }

    @RequestMapping("/update/{id}")
    public String update(@PathVariable("id") Long id, Model model){
        model.addAttribute("id", id);
        return "education_level/update";
    }

    @RequestMapping("delete/{id}")
    public String delete(@PathVariable("id") Long id, Model model){
        model.addAttribute("id", id);
        return "education_level/delete";
    }
}
