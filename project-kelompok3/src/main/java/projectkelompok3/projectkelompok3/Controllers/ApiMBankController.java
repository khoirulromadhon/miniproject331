package projectkelompok3.projectkelompok3.Controllers;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import projectkelompok3.projectkelompok3.Models.Bank;
import projectkelompok3.projectkelompok3.Repositories.BankRepo;


import java.util.*;

@RestController
@CrossOrigin("*")
@RequestMapping("/api")
public class ApiMBankController {

    @Autowired
    private BankRepo bankRepo;

    @GetMapping("/getallbank")
    public ResponseEntity<List<Bank>> GetAllBank()
    {
        try
        {
            List<Bank> bank = this.bankRepo.findAllNotDelete();
            return new ResponseEntity<>(bank, HttpStatus.OK);
        }
        catch (Exception exception)
        {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @GetMapping("/getBankById/{id}")
    public ResponseEntity<List<Bank>> GetBankById(@PathVariable("id") Long id)
    {
        try
        {
            Optional<Bank> bank = this.bankRepo.findById(id);
            if (bank.isPresent())
            {
                ResponseEntity rest = new ResponseEntity<>(bank, HttpStatus.OK);
                return rest;
            } else
            {
                return ResponseEntity.notFound().build();
            }
        }
        catch (Exception exception)
        {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }

    }

    @PostMapping("/addbank")
    public ResponseEntity<Object> SaveBank(@RequestBody Bank bank)
    {
        try {
            bank.setCreatedBy(Long.valueOf(1));
            bank.setCreatedOn(new Date());
            this.bankRepo.save(bank);
            return new ResponseEntity<>(bank, HttpStatus.OK);
        }
        catch (Exception exception)
        {
            return new ResponseEntity<>("failed", HttpStatus.BAD_REQUEST);
        }
    }

//    @PutMapping("/editbank/{id}")
//    public ResponseEntity<Object> UpdateBank(@RequestBody MBank mBank, @PathVariable("id") Long id)
//    {
//        Optional<MBank> mBankData = this.mBankRepo.findById(id);
//        if (mBankData.isPresent())
//        {
//            mBank.setModifiedBy(Long.valueOf(1));
//            mBank.setModifiedOn(new Date());
//            mBank.setId(id);
//            this.mBankRepo.save(mBank);
//            ResponseEntity rest = new ResponseEntity<>("Success", HttpStatus.OK);
//            return rest;
//        } else
//        {
//            return ResponseEntity.notFound().build();
//        }
//    }

    @PutMapping("/editbank/{id}")
    public ResponseEntity<Object> UpdateBank(@RequestBody Bank bank, @PathVariable("id") Long id) {
        Optional<Bank> BankOptional = this.bankRepo.findById(id);

        if (BankOptional.isPresent()) {
            Bank existingBank = BankOptional.get();

            Date createOn = existingBank.getCreatedOn();
            Long createdBy = existingBank.getCreatedBy();

            if (createOn != null && createdBy != null) {
                // Set only fields that are meant to be updated
                existingBank.setName(bank.getName());
                existingBank.setVaCode(bank.getVaCode());

                existingBank.setModifiedBy(1L);
                existingBank.setModifiedOn(new Date());

                this.bankRepo.save(existingBank);
                ResponseEntity<Object> response = new ResponseEntity<>("Success", HttpStatus.OK);
                return response;
            } else {
                return ResponseEntity.badRequest().body("Nilai createOn atau createdBy kosong");
            }
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @GetMapping("/searchbank/{keyword}")
    public ResponseEntity<List<Bank>> SearchBankName(@PathVariable("keyword") String keyword)
    {
        if (keyword != null)
        {
            List<Bank> bank = this.bankRepo.SearchBank(keyword);
            return new ResponseEntity<>(bank, HttpStatus.OK);
        } else {
            List<Bank> bank = this.bankRepo.findAll();
            return new ResponseEntity<>(bank, HttpStatus.OK);
        }
    }

    //    @GetMapping("/bankmapped")
//    public ResponseEntity<Map<String, Object>> GetAllPage(@RequestParam(defaultValue = "0") int page, @RequestParam(defaultValue = "5") int size)
//    {
//        try {
//            List<MBank> mBanks = this.mBankRepo.findAllNotDelete();
//
//            List<MBank> mBank = new ArrayList<>();
//            Pageable pagingSort = PageRequest.of(page, size);
//
//            Page<MBank> pageTuts;
//
//            pageTuts = mBankRepo.findAll(pagingSort);
//            mBank = pageTuts.getContent();
//
//            Map<String, Object> response = new HashMap<>();
//            response.put("mBank", mBank);
//            response.put("currentPage", pageTuts.getNumber());
//            response.put("totalItems", pageTuts.getTotalElements());
//            response.put("totalPages", pageTuts.getTotalPages());
//
//            return new ResponseEntity<>(response, HttpStatus.OK);
//        }
//        catch (Exception e)
//        {
//            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
//        }
//    }
    @GetMapping("/bankmapped")
    public ResponseEntity<Map<String, Object>> GetPage(@RequestParam(defaultValue = "0") int page, @RequestParam(defaultValue = "5") int size) {
        try {
            List<Bank> banks = this.bankRepo.findAllNotDelete();

            int start = page * size;
            int end = Math.min((start + size), banks.size());
            List<Bank> paginatedmbank = banks.subList(start, end);

            Map<String, Object> response = new HashMap<>();
            response.put("mBank", paginatedmbank);
            response.put("currentPage", page);
            response.put("totalItems", banks.size());
            response.put("totalPages", (int) Math.ceil((double) banks.size() / size));

            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/banksort")
    public ResponseEntity<Map<String, Object>> getSortMapped(@RequestParam(defaultValue = "0") int page, @RequestParam(defaultValue = "5") int size,
                                                             @RequestParam String name,@RequestParam(defaultValue = "true") Boolean isAsc){
        try {
            List<Bank> bank = new ArrayList<>();
            Pageable pagingSort;
            if (isAsc){
                pagingSort = PageRequest.of(page, size, Sort.by(name).ascending());
            }else{
                pagingSort = PageRequest.of(page, size, Sort.by(name).descending());
            }
            Page<Bank> pageTuts;
            pageTuts = this.bankRepo.findAllOrder(pagingSort);
            bank = pageTuts.getContent();

            Map<String, Object> respon = new HashMap<>();
            respon.put("mbank", bank);
            respon.put("currentPage", pageTuts.getNumber());
            respon.put("totalItems", pageTuts.getTotalElements());
            respon.put("totalPages", pageTuts.getTotalPages());
            return new ResponseEntity<>(respon, HttpStatus.OK);
        }catch (Exception exception){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }




    //
    @DeleteMapping("/deletebank/{id}")
    public String DeleteBank(@PathVariable("id")Long id)
    {
        try {
            Bank bank = this.bankRepo.findByIdData(id);
            bank.setDeletedBy(Long.valueOf(1));
            bank.setDeleteOn(new Date());
            bank.setDelete(true);
            this.bankRepo.save(bank);
            return "ok";
        }
        catch (Exception exception)
        {
            return "Data Not Found";
        }
    }



}
