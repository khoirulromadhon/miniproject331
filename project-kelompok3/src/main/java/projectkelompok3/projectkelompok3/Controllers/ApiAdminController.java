package projectkelompok3.projectkelompok3.Controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import projectkelompok3.projectkelompok3.Models.MAdmin;
import projectkelompok3.projectkelompok3.Repositories.AdminRepo;

import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/api")
public class ApiAdminController {

    @Autowired
    private AdminRepo adminRepo;

    @GetMapping("/admin/getAllAdmin")
    public ResponseEntity<List<MAdmin>> getAllAdmin(){
        try {
            List<MAdmin> m_admin = this.adminRepo.findAll();
            return new ResponseEntity<>(m_admin, HttpStatus.OK);
        }
        catch (Exception exception){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }


    @PostMapping ("/admin")
    public ResponseEntity<Object> saveAdmin(@RequestBody MAdmin m_admin){
        try {
            m_admin.setCreatedBy(Long.valueOf(1));
            m_admin.setCreatedOn(new Date());
            this.adminRepo.save(m_admin);
            return new ResponseEntity<>("Success", HttpStatus.OK);
        }
        catch (Exception exception){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping("/register/as/code=1")
    public ResponseEntity<Object> saveAdminByCode(@RequestBody MAdmin m_admin){
        try {
            m_admin.setCreatedBy(1L);
            m_admin.setCreatedOn(new Date());
            this.adminRepo.save(m_admin);
            return new ResponseEntity<>("Success", HttpStatus.OK);
        }
        catch (Exception exception){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }
}