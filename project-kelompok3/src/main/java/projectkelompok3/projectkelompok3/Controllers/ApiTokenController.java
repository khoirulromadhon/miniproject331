package projectkelompok3.projectkelompok3.Controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import projectkelompok3.projectkelompok3.Models.TToken;
import projectkelompok3.projectkelompok3.Repositories.TokenRepo;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@RestController
@CrossOrigin("*")
@RequestMapping("/api")
public class ApiTokenController {

    @Autowired
    private TokenRepo tokenRepo;

    @GetMapping("/token/getAllToken")
    public ResponseEntity<List<TToken>> getAllUser(){
        try {
            List<TToken> t_token = this.tokenRepo.findAll();
            return new ResponseEntity<>(t_token, HttpStatus.OK);
        }
        catch (Exception exception){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @GetMapping("/token/lastToken/")
    public String getLastToken(@RequestParam String email, @RequestParam String used_for, @RequestParam String input){
        try {
            Optional<TToken> t_token = this.tokenRepo.getLastToken(email, used_for, input);
            if (t_token.isPresent()){
                Long expired = t_token.get().getExpiredOn().getTime() - new Date().getTime();
                Boolean is_expired = t_token.get().getExpired();
                if (expired > 0 && !is_expired){
                    return "aman";
                }else {
                    return "kadaluarsa";
                }
            }else {
                return "salah";
            }
        }catch (Exception exception){
            return exception.getMessage();
        }
    }

    @PostMapping("/token/save")
    public ResponseEntity<Object> saveToken(@RequestBody TToken t_token){
        try {
            Date expiredDate = new Date();
            Integer minutes = expiredDate.getMinutes();
            expiredDate.setMinutes(minutes + 3);

            t_token.setCreatedBy(1L);
            t_token.setCreatedOn(new Date());
            t_token.setExpiredOn(expiredDate);
            this.tokenRepo.save(t_token);
            return new ResponseEntity<>("Success", HttpStatus.OK);
        }catch (Exception exception){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @PutMapping("/token/tokenToExpired/")
    public ResponseEntity<Object> tokenToExpired(@RequestParam String email, @RequestParam String used_for){
        try {
            TToken t_token = this.tokenRepo.getLastToken(email, used_for);
            t_token.setExpired(true);
            this.tokenRepo.save(t_token);
            return new ResponseEntity<>("Success", HttpStatus.OK);
        }catch (Exception exception){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }


}
