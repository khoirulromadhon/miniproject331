package projectkelompok3.projectkelompok3.Controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/alamat")
public class AlamatController {
    @RequestMapping("")
    public String alamat (){
        return "tabprofile/alamat";
    }
}
