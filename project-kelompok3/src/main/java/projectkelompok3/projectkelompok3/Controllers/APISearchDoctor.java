package projectkelompok3.projectkelompok3.Controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import projectkelompok3.projectkelompok3.Models.Doctor;
import projectkelompok3.projectkelompok3.Service.SearchDoctorService;

import java.util.List;

@RestController
@RequestMapping("/api")
public class APISearchDoctor {
    @Autowired
    SearchDoctorService searchDoctorService;

    @GetMapping("/doctor")
    public List<Doctor> GetAllDoctors(){
        return searchDoctorService.getAllDoctors();
    }

    @GetMapping("doctor/{id}")
    public Doctor GetDoctorById(@PathVariable("id") Long id){
        return searchDoctorService.getDoctorById(id);
    }

    @PostMapping("/filter")
    public List<Doctor> FilterDoctors(@RequestParam Doctor doctorFilter){
        return searchDoctorService.filterDoctor(doctorFilter);
    }
}
