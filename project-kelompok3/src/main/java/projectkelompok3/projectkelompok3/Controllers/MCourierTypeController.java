package projectkelompok3.projectkelompok3.Controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("mcouriertype")
public class MCourierTypeController {

    @RequestMapping("")
    public String mCourierType() {
        return "mcouriertype/mcouriertype";
    }

    @RequestMapping("/addmcouriertype")
    public String addMCourierType() {
        return "mcouriertype/addmcouriertype";
    }

    @RequestMapping("editmcouriertype/{id}")
    public String editMCourierType(@PathVariable("id") Long id, Model model)
    {
        model.addAttribute("id", id);
        return "mcouriertype/editmcouriertype";
    }

    @RequestMapping("deletemcouriertype/{id}")
    public String deleteMCourierType(@PathVariable("id") Long id, Model model)
    {
        model.addAttribute("id", id);
        return "mcouriertype/deletemcouriertype";
    }

}
