package projectkelompok3.projectkelompok3.Controllers;


import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/payment_method")
public class PaymentMethodController {

//    @GetMapping(value = "index")
//    public ModelAndView index()
//    {
//        ModelAndView view = new ModelAndView("paymentMethod/index");
//        return view;
//    }

    @RequestMapping("")
    public String getAllPayment()
    {
        return "m_payment_method/payment_method";
    }

    @RequestMapping("addpayment")
    public String addPayment()
    {
        return "m_payment_method/addpayment_method";
    }

    @RequestMapping("editpayment/{id}")
    public String editPayment(@PathVariable("id") Long id, Model model)
    {
        return "m_payment_method/editpayment_method";
    }

    @RequestMapping("deletepayment/{id}")
    public String deletePayment(@PathVariable("id") Long id, Model model)
    {
        return "m_payment_method/deletepayment_method";
    }
}
