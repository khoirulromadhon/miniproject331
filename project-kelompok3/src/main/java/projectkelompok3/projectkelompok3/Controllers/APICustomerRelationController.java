package projectkelompok3.projectkelompok3.Controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import projectkelompok3.projectkelompok3.Models.CustomerRelation;
import projectkelompok3.projectkelompok3.Repositories.CustomerRelationRepositories;

import java.util.*;

@RestController
@CrossOrigin("*")
@RequestMapping("/api")
public class APICustomerRelationController {
    @Autowired CustomerRelationRepositories customerRelationRepositories;

    @GetMapping("/customerRelationMapped")
    public ResponseEntity<Map<String, Object>> GetAllCustomerRelationMapped(@RequestParam(defaultValue = "0") int page, @RequestParam(defaultValue = "5") int size){
        try{
            List<CustomerRelation> customerRelationList = new ArrayList<>();
            Pageable paging = PageRequest.of(page, size);

            Page<CustomerRelation> pageTuts;

            pageTuts = customerRelationRepositories.findAll(paging);

            customerRelationList = pageTuts.getContent();

            Map<String, Object> response = new HashMap<>();
            response.put("customerRelationList", customerRelationList);
            response.put("currentPage", pageTuts.getNumber());
            response.put("totalItems", pageTuts.getTotalElements());
            response.put("totalPages", pageTuts.getTotalPages());

            return new ResponseEntity<>(response, HttpStatus.OK);
        }
        catch (Exception exception){
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/customerRelation")
    public ResponseEntity<List<CustomerRelation>> GetAllCustomerRelation(){
        try {
            List<CustomerRelation> customerRelations = this.customerRelationRepositories.findAllNotDeleted();
            return new ResponseEntity<>(customerRelations, HttpStatus.OK);
        }
        catch (Exception exception){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @GetMapping("/customerRelation/{id}")
    public ResponseEntity<List<CustomerRelation>> GetCustomerRelationById(@PathVariable("id") Long id){
        try {
            Optional<CustomerRelation> customerRelation = this.customerRelationRepositories.findById(id);

            if (customerRelation.isPresent()){
                ResponseEntity response = new ResponseEntity<>(customerRelation, HttpStatus.OK);
                return response;
            }
            else {
                return ResponseEntity.notFound().build();
            }
        }
        catch (Exception exception){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @GetMapping("/searchCustomerRelation={keyword}")
    public ResponseEntity<List<CustomerRelation>> SearchCustomerRelation(@PathVariable("keyword") String keyword){
        try{
            List<CustomerRelation> customerRelationList = this.customerRelationRepositories.searchCustomerRelation(keyword);
            return new ResponseEntity<>(customerRelationList, HttpStatus.OK);
        }
        catch (Exception exception){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @PostMapping("/customerRelation")
    public ResponseEntity<Object> SaveCustomerRelation(@RequestBody CustomerRelation customerRelation){
        try{
            customerRelation.setCreatedBy(Long.valueOf(1));
            customerRelation.setCreatedOn(new Date());
            this.customerRelationRepositories.save(customerRelation);
            return new ResponseEntity<>(customerRelation, HttpStatus.OK);
        }
        catch (Exception exception){
            return new ResponseEntity<>("Fail To Save Data !!!!!", HttpStatus.BAD_REQUEST);
        }
    }

    @PutMapping("/customerRelation/{id}")
    public ResponseEntity<Object> UpdateCustomerRelation(@RequestBody CustomerRelation customerRelation, @PathVariable("id") Long id){
        Optional<CustomerRelation> customerRelationData = this.customerRelationRepositories.findById(id);
        customerRelation.setCreatedBy(Long.valueOf(1));
        customerRelation.setCreatedOn(new Date());
        customerRelation.setModifiedBy(Long.valueOf(1));
        customerRelation.setModifiedOn(new Date());

        if (customerRelationData.isPresent()){
            customerRelation.setId(id);
            this.customerRelationRepositories.save(customerRelation);
            ResponseEntity response = new ResponseEntity<>("Success Update Data !!!!!", HttpStatus.OK);
            return response;
        }
        else {
            return ResponseEntity.noContent().build();
        }
    }

    @DeleteMapping("/customerRelation/{id}")
    public String DeleteCustomerRelation(@PathVariable("id") Long id){
        try {
            CustomerRelation customerRelation = this.customerRelationRepositories.findByIdData(id);
            customerRelation.setDeletedBy(Long.valueOf(1));
            customerRelation.setDeleteOn(new Date());
            customerRelation.setDelete(true);
            this.customerRelationRepositories.save(customerRelation);
            return "Success Delete Data !!!!!";
        }
        catch (Exception exception){
            return "Data Not Found !!!!!";
        }
    }
}
