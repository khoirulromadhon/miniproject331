package projectkelompok3.projectkelompok3.Controllers;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import projectkelompok3.projectkelompok3.Models.MUser;
import projectkelompok3.projectkelompok3.Repositories.UserRepo;

import javax.servlet.http.HttpSession;
import java.util.*;

@RestController
@CrossOrigin("*")
@RequestMapping("/api")
public class ApiUserController {

    @Autowired
    private UserRepo userRepo;

    @GetMapping("/user/session")
    public Map isSessionLogin(HttpSession httpSession){
        Long user_id = (Long) httpSession.getAttribute("userID");
        Long role_id = (Long) httpSession.getAttribute("roleID");

        if (user_id != null){
            Map obj = new HashMap();
            obj.put("user_id", user_id);
            obj.put("role_id", role_id);
            List data =  new ArrayList();
            data.add(obj);
            return obj;
        }else {
            Map obj = new HashMap();
            obj.put("user_id", 0);
            obj.put("role_id", 0);
            List data =  new ArrayList<>();
            data.add(obj);
            return obj;
        }
    }


    @GetMapping("/user/getAllUser")
    public ResponseEntity<List<MUser>> getAllUser(){
        try {
            List<MUser> mUser = this.userRepo.findAll();
            return new ResponseEntity<>(mUser, HttpStatus.OK);
        }
        catch (Exception exception){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @GetMapping("/user/userID={user_id}")
    public ResponseEntity<Object> getUserById(@PathVariable("user_id") Long id){
        try {
            Optional<MUser> mUser = this.userRepo.findById(id);
            if (mUser.isPresent()) {
                return new ResponseEntity<>(mUser, HttpStatus.OK);
            } else {
                return ResponseEntity.notFound().build();
            }
        }catch (Exception exception){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @GetMapping("user/email={email}")
    public boolean isEmailTerdaftar(@PathVariable("email") String email){
        Optional<MUser> mUser = this.userRepo.isEmailTerdaftar(email);
        if (mUser.isPresent()){
            return true;
        }else {
            return false;
        }
    }

    @PostMapping("/user/callResponse")
    public ResponseEntity<Object> saveUser(@RequestBody MUser mUser){
        try {
            mUser.setCreatedBy(1L);
            mUser.setCreatedOn(new Date());
            this.userRepo.save(mUser);
            return new ResponseEntity<>("Success", HttpStatus.OK);
        }
        catch (Exception exception){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping("/user/lupa-password")
    public ResponseEntity<Object> savePasswordBaru(@RequestParam String email,@RequestBody MUser mUser){
        try {
            MUser userLama = this.userRepo.lupapassword(email);
            String PasswordLama = userLama.getPassword();
            userLama.setModifiedBy(1L);
            userLama.setModifiedOn(new Date());
            userLama.setPassword(mUser.getPassword());
            this.userRepo.save(userLama);
            return new ResponseEntity<>(PasswordLama, HttpStatus.OK);
        }
        catch (Exception exception){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }
}