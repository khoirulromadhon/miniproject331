package projectkelompok3.projectkelompok3.Controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/doctor")
public class SearchDoctorController {
    @RequestMapping("/searchdoctor")
    public String searchDokter() {
        return "search_doctor/search_doctor";
    }
}
