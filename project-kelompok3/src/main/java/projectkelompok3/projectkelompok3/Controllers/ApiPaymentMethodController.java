package projectkelompok3.projectkelompok3.Controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import projectkelompok3.projectkelompok3.Models.Bank;
import projectkelompok3.projectkelompok3.Models.PaymentMethod;
import projectkelompok3.projectkelompok3.Repositories.PaymentMethodRepo;

import java.util.*;

@RestController
@CrossOrigin("*")
@RequestMapping("/api")
public class ApiPaymentMethodController {

    @Autowired
    private PaymentMethodRepo paymentMethodRepo;

    @GetMapping("/getallpayment")
    public ResponseEntity<List<PaymentMethod>> GetAllPayment()
    {
        try
        {
            List<PaymentMethod> paymentMethod = this.paymentMethodRepo.findAllNotDelete();
            return new ResponseEntity<>(paymentMethod, HttpStatus.OK);
        }
        catch (Exception exception)
        {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @GetMapping("/getPaymentById/{id}")
    public ResponseEntity<List<PaymentMethod>> GetPaymentById(@PathVariable("id") Long id)
    {
        try
        {
            Optional<PaymentMethod> paymentMethod = this.paymentMethodRepo.findById(id);
            if (paymentMethod.isPresent())
            {
                ResponseEntity rest = new ResponseEntity<>(paymentMethod, HttpStatus.OK);
                return rest;
            } else
            {
                return ResponseEntity.notFound().build();
            }
        }
        catch (Exception exception)
        {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }

    }

    @PostMapping("/addpayment")
    public ResponseEntity<Object> SavePayment(@RequestBody PaymentMethod paymentMethod)
    {
        try {
            paymentMethod.setCreatedBy(Long.valueOf(1));
            paymentMethod.setCreatedOn(new Date());
            this.paymentMethodRepo.save(paymentMethod);
            return new ResponseEntity<>(paymentMethod, HttpStatus.OK);
        }
        catch (Exception exception)
        {
            return new ResponseEntity<>("failed", HttpStatus.BAD_REQUEST);
        }
    }

    @PutMapping("/editpayment/{id}")
    public ResponseEntity<Object> UpdatePayment(@RequestBody PaymentMethod paymentMethod, @PathVariable("id") Long id) {
        Optional<PaymentMethod> PaymentMethodOptional = this.paymentMethodRepo.findById(id);

        if (PaymentMethodOptional.isPresent()) {
            PaymentMethod existingPaymentMethod = PaymentMethodOptional.get();

            Date createOn = existingPaymentMethod.getCreatedOn();
            Long createdBy = existingPaymentMethod.getCreatedBy();

            if (createOn != null && createdBy != null) {
                // Set only fields that are meant to be updated
                existingPaymentMethod.setName(paymentMethod.getName());

                existingPaymentMethod.setModifiedBy(Long.valueOf(1));
                existingPaymentMethod.setModifiedOn(new Date());

                this.paymentMethodRepo.save(existingPaymentMethod);
                ResponseEntity<Object> response = new ResponseEntity<>("Success", HttpStatus.OK);
                return response;
            } else {
                return ResponseEntity.badRequest().body("Nilai createOn atau createdBy kosong");
            }
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @GetMapping("/searchpayment/{keyword}")
    public ResponseEntity<List<PaymentMethod>> SearchPaymentName(@PathVariable("keyword") String keyword)
    {
        if (keyword != null)
        {
            List<PaymentMethod> paymentMethod = this.paymentMethodRepo.SearchPayment(keyword);
            return new ResponseEntity<>(paymentMethod, HttpStatus.OK);
        } else {
            List<PaymentMethod> paymentMethod = this.paymentMethodRepo.findAll();
            return new ResponseEntity<>(paymentMethod, HttpStatus.OK);
        }
    }

    @GetMapping("/paymentmapped")
    public ResponseEntity<Map<String, Object>> GetPage(@RequestParam(defaultValue = "0") int page, @RequestParam(defaultValue = "5") int size) {
        try {
            List<PaymentMethod> paymentMethod = this.paymentMethodRepo.findAllNotDelete();

            int start = page * size;
            int end = Math.min((start + size), paymentMethod.size());
            List<PaymentMethod> paginatedpayment = paymentMethod.subList(start, end);

            Map<String, Object> response = new HashMap<>();
            response.put("paymentMethod", paginatedpayment);
            response.put("currentPage", page);
            response.put("totalItems", paymentMethod.size());
            response.put("totalPages", (int) Math.ceil((double) paymentMethod.size() / size));

            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/paymentsort")
    public ResponseEntity<Map<String, Object>> getSortMapped(@RequestParam(defaultValue = "0") int page, @RequestParam(defaultValue = "5") int size,
                                                             @RequestParam String name,@RequestParam(defaultValue = "true") Boolean isAsc){
        try {
            List<PaymentMethod> paymentMethods = new ArrayList<>();
            Pageable pagingSort;
            if (isAsc){
                pagingSort = PageRequest.of(page, size, Sort.by(name).ascending());
            }else{
                pagingSort = PageRequest.of(page, size, Sort.by(name).descending());
            }
            Page<PaymentMethod> pageTuts;
            pageTuts = this.paymentMethodRepo.findAllOrder(pagingSort);
            paymentMethods = pageTuts.getContent();

            Map<String, Object> respon = new HashMap<>();
            respon.put("paymentMethod", paymentMethods);
            respon.put("currentPage", pageTuts.getNumber());
            respon.put("totalItems", pageTuts.getTotalElements());
            respon.put("totalPages", pageTuts.getTotalPages());
            return new ResponseEntity<>(respon, HttpStatus.OK);
        }catch (Exception exception){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @DeleteMapping("/deletepayment/{id}")
    public String DeletePayment(@PathVariable("id")Long id)
    {
        try {
            PaymentMethod paymentMethod = this.paymentMethodRepo.findByIdData(id);
            paymentMethod.setDeletedBy(Long.valueOf(1));
            paymentMethod.setDeleteOn(new Date());
            paymentMethod.setDelete(true);
            this.paymentMethodRepo.save(paymentMethod);
            return "ok";
        }
        catch (Exception exception)
        {
            return "Data Not Found";
        }
    }
}
