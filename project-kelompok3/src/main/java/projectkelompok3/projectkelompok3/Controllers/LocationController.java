package projectkelompok3.projectkelompok3.Controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping(value = "location")
public class LocationController {
    @RequestMapping("")
    public String location (){
        return "location/location";
    }
    @RequestMapping("addlocation")
    public String addlocation(){
        return "location/addlocation";
    }
    @RequestMapping("editlocation/{id}")
    public String editlocation(@PathVariable("id") Long id, Model model) {
        model.addAttribute("id", id);
        return "location/editlocation";
    }

    @RequestMapping("deletelocation/{id}")
    public String deletelocation(@PathVariable("id") Long id, Model model) {
        model.addAttribute("id", id);
        return "location/deletelocation";
    }
}
