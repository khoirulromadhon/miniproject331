package projectkelompok3.projectkelompok3.Controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/mbank")
public class BankController {

    //    @GetMapping(value = "index")
//    public ModelAndView index()
//    {
//        ModelAndView view = new ModelAndView("mBank/index");
//        return view;
//    }
    @RequestMapping("")
    public String getAllBank()
    {
        return "m_bank/bank";
    }

    @RequestMapping("addbank")
    public String addMBank()
    {
        return "m_bank/addbank";
    }

    @RequestMapping("editbank/{id}")
    public String editMBank(@PathVariable("id") Long id, Model model)
    {
        return "m_bank/editbank";
    }

    @RequestMapping("deletebank/{id}")
    public String deleteMBank(@PathVariable("id") Long id, Model model)
    {
        model.addAttribute("id", id);
        return "m_bank/deletebank";
    }
}
