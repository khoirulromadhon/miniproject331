package projectkelompok3.projectkelompok3.Controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import projectkelompok3.projectkelompok3.Models.MRole;
import projectkelompok3.projectkelompok3.Repositories.RoleRepo;

import java.util.List;
import java.util.Optional;

@RestController
@CrossOrigin("*")
@RequestMapping("/api")
public class ApiRoleController {

    @Autowired
    private RoleRepo roleRepo;

    @GetMapping("/getallrole")
    public ResponseEntity<List<MRole>> GetAllRole(){
        try{
            List<MRole> role = this.roleRepo.findAllNotDeleted();
            return new ResponseEntity<>(role, HttpStatus.OK);
        }
        catch(Exception exception){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @GetMapping("/getrole/{id}")
    public ResponseEntity<List<MRole>> GetRoleById(@PathVariable("id") Long id)
    {
        try{
            Optional<MRole> role = this.roleRepo.findById(id);

            if(role.isPresent()){
                ResponseEntity rest = new ResponseEntity<>(role, HttpStatus.OK);
                return rest;
            } else {
                return ResponseEntity.notFound().build();
            }
        }

        catch (Exception exception){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }



}
