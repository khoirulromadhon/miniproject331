package projectkelompok3.projectkelompok3.Controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import projectkelompok3.projectkelompok3.Models.MLocation;
import projectkelompok3.projectkelompok3.Models.MLocationLevel;
import projectkelompok3.projectkelompok3.Repositories.LocationLevelRepo;
import projectkelompok3.projectkelompok3.Repositories.LocationRepo;

import java.util.*;

@RestController
@CrossOrigin("*")
@RequestMapping("/api")
public class ApiLocationLevelController {
    @Autowired
    private LocationLevelRepo locationLevelRepo;
    @Autowired
    private LocationRepo locationRepo;

    @GetMapping("/getlocationlevel")
    public ResponseEntity<List<MLocationLevel>> GetAllLocationLevel() {
        try {
            List<MLocationLevel> locationlevel = this.locationLevelRepo.findAllNotDeleted();
            return new ResponseEntity<>(locationlevel, HttpStatus.OK);
        } catch (Exception exception) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @PostMapping("/addlocationlevel")
    public ResponseEntity<Object> SaveLocationLevel(@RequestBody MLocationLevel locationLevel) {
        try {
            String namaLocation = locationLevel.getName();
            int existingLocationCount = locationLevelRepo.countByNameIgnoreCaseAndNotDeleted(namaLocation);
            if (existingLocationCount > 0) {
                return new ResponseEntity<>("Nama Level Lokasi sudah ada!", HttpStatus.BAD_REQUEST);
            } else {
                locationLevel.setCreatedBy(1L);
                locationLevel.setCreatedOn(new Date());
                this.locationLevelRepo.save(locationLevel);
                return new ResponseEntity<>(locationLevel, HttpStatus.OK);
            }
        } catch (Exception exception) {
            return new ResponseEntity<>("Failed", HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/locationlevel/{id}")
    public ResponseEntity<List<MLocationLevel>> GetlocationlevelById(@PathVariable("id") Long id) {
        try {
            Optional<MLocationLevel> locationlevel = this.locationLevelRepo.findById(id);

            if (locationlevel.isPresent()) {
                ResponseEntity rest = new ResponseEntity<>(locationlevel, HttpStatus.OK);
                return rest;
            } else {
                return ResponseEntity.notFound().build();
            }
        } catch (Exception exception) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @PutMapping("/locationlevel/{id}")
    public ResponseEntity<Object> updateLocationLevel(@RequestBody MLocationLevel location, @PathVariable("id") Long id) {
        Optional<MLocationLevel> locationLevelOptional = this.locationLevelRepo.findById(id);

        if (locationLevelOptional.isPresent()) {
            MLocationLevel existingLocationLevel = locationLevelOptional.get();

            String newName = location.getName();
            int existingLocationCount = locationLevelRepo.countByNameIgnoreCaseAndNotDeletedAndIdNot(newName, id);
            if (existingLocationCount > 0) {
                return new ResponseEntity<>("Nama Level Lokasi sudah ada!", HttpStatus.BAD_REQUEST);
            }

            Date createOn = existingLocationLevel.getCreatedOn();
            Long createdBy = existingLocationLevel.getCreatedBy();

            if (createOn != null && createdBy != null) {
                // Set only fields that are meant to be updated
                existingLocationLevel.setName(location.getName());
                existingLocationLevel.setAbbreviation(location.getAbbreviation());

                existingLocationLevel.setModifiedBy(1L);
                existingLocationLevel.setModifiedOn(new Date());

                this.locationLevelRepo.save(existingLocationLevel);
                return new ResponseEntity<>("Success", HttpStatus.OK);
            } else {
                return ResponseEntity.badRequest().body("Nilai createOn atau createdBy kosong");
            }
        } else {
            return ResponseEntity.notFound().build();
        }
    }



    @GetMapping("/searchlevellocation/{keyword}")
    public ResponseEntity<List<MLocationLevel>> SearchLocationLevel(@PathVariable("keyword") String keyword) {
        if (keyword != null) {
            List<MLocationLevel> locationLevels = this.locationLevelRepo.searchLocation(keyword);
            return new ResponseEntity<>(locationLevels, HttpStatus.OK);
        } else {
            List<MLocationLevel> locationLevels = this.locationLevelRepo.findAll();
            return new ResponseEntity<>(locationLevels, HttpStatus.OK);
        }
    }

    @GetMapping("/locationlevelmapped")
    public ResponseEntity<Map<String, Object>> GetPage(@RequestParam(defaultValue = "0") int page, @RequestParam(defaultValue = "5") int size) {
        try {
            List<MLocationLevel> locationLevels = this.locationLevelRepo.findAllNotDeleted();
            int totalItems = locationLevels.size();
            int totalPages = (int) Math.ceil((double) totalItems / size);

            int start = page * size;
            int end = Math.min((start + size), locationLevels.size());
            List<MLocationLevel> paginatedlocationlevel = locationLevels.subList(start, end);

            Map<String, Object> response = new HashMap<>();
            response.put("locationlevel", paginatedlocationlevel);
            response.put("currentPage", page);
            response.put("totalItems", locationLevels.size());
            response.put("totalPages", (int) Math.ceil((double) locationLevels.size() / size));
            response.put("lastPageCount", totalPages - 1);

            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/locationlevel/{id}")
    public ResponseEntity<String> deleteLocationLevel(@PathVariable("id") Long id) {
        try {
            MLocationLevel locationLevel = this.locationLevelRepo.findByIdData(id);

            // Periksa apakah level lokasi ini masih digunakan di lokasi
            List<MLocation> locationsUsingLevel = locationRepo.findByLocationLevelId(id);

            if (!locationsUsingLevel.isEmpty()) {
                return ResponseEntity.badRequest().body("Tidak dapat menghapus level lokasi ini karena masih digunakan di lokasi lain.");
            }

            // Lakukan penghapusan jika tidak ada lokasi yang menggunakan level lokasi ini
            locationLevel.setDeletedBy(1L);
            locationLevel.setDelete(true);
            locationLevel.setDeleteOn(new Date());
            this.locationLevelRepo.save(locationLevel);

            return ResponseEntity.ok("Data level lokasi berhasil dihapus");
        } catch (Exception exception) {
            return ResponseEntity.notFound().build();
        }
    }

}
