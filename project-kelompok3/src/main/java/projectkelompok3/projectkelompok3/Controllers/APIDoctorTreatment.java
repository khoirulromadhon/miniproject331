package projectkelompok3.projectkelompok3.Controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import projectkelompok3.projectkelompok3.Models.Doctor;
import projectkelompok3.projectkelompok3.Models.DoctorTreatment;
import projectkelompok3.projectkelompok3.Service.DoctorTreatmentService;

import java.util.List;

@RestController
@RequestMapping("/api")
public class APIDoctorTreatment {
    @Autowired
    private DoctorTreatmentService doctorTreatmentService;

    @GetMapping("/doctortreatment")
    public List<DoctorTreatment> GetAllDoctorTreatment(){
        List<DoctorTreatment> doctorTreatments = doctorTreatmentService.GetAllDoctorTreatments();
        return doctorTreatments;
    }
}
