package projectkelompok3.projectkelompok3.Controllers;


import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("mcourier")
public class MCourierController {

    @RequestMapping("")
    public String mCourier()
    {
        return "mcourier/mcourier";
    }

    @RequestMapping("/addmcourier")
    public String addMCourier()
    {
        return "mcourier/addmcourier";
    }

    @RequestMapping("editmcourier/{id}")
    public String editMCourier(@PathVariable("id") Long id, Model model)
    {
        model.addAttribute("id", id);
        return "mcourier/editmcourier";
    }

    @RequestMapping("deletemcourier/{id}")
    public String deleteMCourier(@PathVariable("id") Long id, Model model)
    {
        model.addAttribute("id", id);
        return "mcourier/deletemcourier";
    }

}
