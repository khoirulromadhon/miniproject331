package projectkelompok3.projectkelompok3;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProjectKelompok3Application {

	public static void main(String[] args) {
		SpringApplication.run(ProjectKelompok3Application.class, args);
	}

}
