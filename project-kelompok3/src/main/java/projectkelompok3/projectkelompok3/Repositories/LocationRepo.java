package projectkelompok3.projectkelompok3.Repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import projectkelompok3.projectkelompok3.Models.MLocation;

import java.util.List;

public interface LocationRepo extends JpaRepository<MLocation, Long> {
    @Query(value = "SELECT * FROM m_location WHERE is_delete = false ORDER BY id ASC", nativeQuery = true)
    List<MLocation> findAllNotDeleted();
    @Query(value = "SELECT * FROM m_location WHERE id = :id", nativeQuery = true)
    MLocation findByIdData(long id);
    @Query(value = "SELECT * FROM m_location WHERE name = :name AND parent_id = :parentId AND location_level_id = :locationLevelId", nativeQuery = true)
    MLocation findByNameAndParentIdAndLocationLevelId(String name, Long parentId, Long locationLevelId);
    @Query("FROM MLocation WHERE lower(name) LIKE lower(concat('%',?1,'%')) AND is_delete = false")
    List<MLocation> searchLocation(String keyword);
    @Query(value = "SELECT * FROM m_location WHERE name = :name AND location_level_id = :locationLevelId", nativeQuery = true)
    MLocation findByNameAndLocationLevelId(String name, Long locationLevelId);
    @Query(value = "SELECT * FROM m_location WHERE name = :name AND parent_id = :parentId AND location_level_id = :locationLevelId AND id != :id", nativeQuery = true)
    MLocation findByNameAndParentIdAndLocationLevelIdAndIdNot(String name, Long parentId, Long locationLevelId, Long id);
    @Query(value = "SELECT * FROM m_location WHERE name = :name AND location_level_id = :locationLevelId AND id != :id", nativeQuery = true)
    MLocation findByNameAndLocationLevelIdAndIdNot(String name, Long locationLevelId, Long id);
    @Query("SELECT l FROM MLocation l WHERE l.parentLocation = :parentLocation AND l.isDelete = false")
    List<MLocation> findByParentLocation(@Param("parentLocation") MLocation parentLocation);
    @Query("SELECT l FROM MLocation l WHERE l.locationLevel.id = :locationLevelId AND l.isDelete = false")
    List<MLocation> findByLocationLevelId(@Param("locationLevelId") Long locationLevelId);
}
