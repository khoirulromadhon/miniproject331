package projectkelompok3.projectkelompok3.Repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import projectkelompok3.projectkelompok3.Models.DoctorTreatment;

import java.util.List;

public interface DoctorTreatmentRepository extends JpaRepository<DoctorTreatment, Long> {
    @Query(value = "SELECT * FROM t_doctor_treatment WHERE is_delete = false", nativeQuery = true)
    public List<DoctorTreatment> getAllNotDeletedDoctorTreatment();
}
