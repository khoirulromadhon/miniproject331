package projectkelompok3.projectkelompok3.Repositories;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import projectkelompok3.projectkelompok3.Models.MCourier;

import java.util.List;

public interface MCourierRepo extends JpaRepository<MCourier, Long> {

    //query search
    @Query("FROM MCourier WHERE lower(Name) LIKE lower (concat('%',?1,'%')) AND isDelete = false")
    List<MCourier> SearchMCourier(String keyword);

    @Query(value = "SELECT * FROM m_courier WHERE id = :id", nativeQuery = true)
    MCourier findByIdData(long id);

    @Query(value = "SELECT * FROM m_courier WHERE is_delete = false", nativeQuery = true)
    List<MCourier> findAllNotDeleted();

    @Query(value = "SELECT * FROM m_courier WHERE is_delete = false ORDER BY m_courier.id ASC",nativeQuery = true)
    Page<MCourier> findAllOrderByAsc(Pageable pageable);

}
