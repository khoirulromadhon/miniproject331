package projectkelompok3.projectkelompok3.Repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import projectkelompok3.projectkelompok3.Models.EducationLevel;

import java.util.List;

public interface EducationLevelRepositories extends JpaRepository<EducationLevel, Long> {
    @Query(value = "SELECT * FROM m_education_level WHERE id = :id", nativeQuery = true)
    EducationLevel findByIdData(long id);

    @Query(value = "SELECT * FROM m_education_level WHERE lower(name) LIKE lower (concat('%', :keyword, '%')) AND is_Delete = false", nativeQuery = true)
    List<EducationLevel> searchEducationLevel(String keyword);

    @Query(value = "SELECT * FROM m_education_level WHERE is_delete = false", nativeQuery = true)
    List<EducationLevel> findAllNotDeleted();
}
