package projectkelompok3.projectkelompok3.Repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import projectkelompok3.projectkelompok3.Models.MAdmin;

public interface AdminRepo extends JpaRepository<MAdmin, Long> {
}
