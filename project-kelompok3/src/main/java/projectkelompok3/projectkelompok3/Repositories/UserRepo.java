package projectkelompok3.projectkelompok3.Repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import projectkelompok3.projectkelompok3.Models.MUser;

import java.util.List;
import java.util.Optional;

public interface UserRepo extends JpaRepository<MUser, Long> {
    @Query(value = "SELECT * FROM public.m_user WHERE m_user.is_delete = false AND lower(m_user.email) = lower(:email)", nativeQuery = true)
    Optional<MUser> isEmailTerdaftar(String email);

    @Query(value = "SELECT * FROM public.m_user WHERE m_user.is_delete = false AND lower(m_user.email) = lower(:email) AND m_user.password = :password LIMIT 1", nativeQuery = true)
    MUser login(String email, String password);

    @Query(value = "SELECT * FROM public.m_user WHERE m_user.is_delete = false AND lower(m_user.email) = lower(:email)", nativeQuery = true)
    MUser lupapassword(String email);

    Optional<MUser> findByBiodataId(Long biodataId);
    List<MUser> findAllByBiodataId(Long biodataId);


//=======
//import java.util.List;
//import java.util.Optional;
//
//public interface UserRepo extends JpaRepository<MUser, Long> {
//
//
//>>>>>>> 69fa3d2cba62225ca032c8408dd10314face3296
}
