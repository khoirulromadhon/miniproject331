package projectkelompok3.projectkelompok3.Repositories;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import projectkelompok3.projectkelompok3.Models.MRole;

import java.util.List;

public interface RoleRepo extends JpaRepository<MRole ,Long> {

    @Query(value = "SELECT * FROM m_role ORDER BY m_role ASC", nativeQuery = true)
    List<MRole> findAllRoleOrderbyASC();
    @Query(value = "SELECT * FROM m_role WHERE is_delete = false ORDER BY m_role.id ASC",nativeQuery = true)
    Page<MRole> findAllOrderByAsc(Pageable pageable);

    @Query(value = "SELECT * FROM m_role WHERE m_role.is_delete = false",nativeQuery = true)
    Page<MRole> findAllOrder(Pageable pageable);

    @Query(value = "SELECT * FROM m_role WHERE lower(m_role.name) LIKE lower(concat('%',:kata,'%'))", nativeQuery = true)
    List<MRole> search(String kata);

    @Query(value = "SELECT * FROM m_role WHERE id = :id", nativeQuery = true)
    MRole findByIdData(long id);

    @Query(value = "SELECT * FROM m_role WHERE is_delete = false ORDER BY m_role.id ASC", nativeQuery = true)
    List<MRole> findAllNotDeleted();

    @Query(value = "SELECT * FROM m_role WHERE m_role.is_delete = false AND lower(m_role.name) LIKE lower (concat('%',:kata,'%'))", nativeQuery = true)
    Page<MRole> searchMapped(Pageable pageable, String kata);


}
