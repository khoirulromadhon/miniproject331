package projectkelompok3.projectkelompok3.Repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import projectkelompok3.projectkelompok3.Models.MLocationLevel;

import java.util.List;

public interface LocationLevelRepo extends JpaRepository<MLocationLevel, Long> {
    @Query(value = "SELECT * FROM m_location_level WHERE is_delete = false ORDER BY id ASC", nativeQuery = true)
    List<MLocationLevel> findAllNotDeleted();
    @Query(value = "SELECT * FROM m_location_level WHERE id = :id", nativeQuery = true)
    MLocationLevel findByIdData(long id);
    @Query("FROM MLocationLevel WHERE lower(name) LIKE lower(concat('%',?1,'%')) AND is_delete = false")
    List<MLocationLevel> searchLocation(String keyword);
    @Query("SELECT COUNT(*) FROM MLocationLevel WHERE lower(name) = lower(?1) AND is_delete = false")
    int countByNameIgnoreCaseAndNotDeleted(String name);
    @Query("SELECT COUNT(*) FROM MLocationLevel WHERE lower(name) = lower(?1) AND is_delete = false AND id != ?2")
    int countByNameIgnoreCaseAndNotDeletedAndIdNot(String name, Long id);
}
