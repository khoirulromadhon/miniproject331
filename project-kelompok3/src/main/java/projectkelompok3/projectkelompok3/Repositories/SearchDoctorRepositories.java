package projectkelompok3.projectkelompok3.Repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import projectkelompok3.projectkelompok3.Models.CurrentDoctorSpecialization;
import projectkelompok3.projectkelompok3.Models.Doctor;
import projectkelompok3.projectkelompok3.Models.DoctorOffice;

import java.util.List;

public interface SearchDoctorRepositories extends JpaRepository<Doctor, Long> {
}
