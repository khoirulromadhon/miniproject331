package projectkelompok3.projectkelompok3.Repositories;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import projectkelompok3.projectkelompok3.Models.MCourier;
import projectkelompok3.projectkelompok3.Models.MCourierType;

import java.util.List;

public interface MCourierTypeRepo extends JpaRepository<MCourierType, Long> {

    @Query("FROM MCourierType WHERE lower(Name) LIKE lower (concat('%',?1,'%')) AND isDelete = false")
    List<MCourierType> SearchMCourierType(String keyword);

    @Query(value = "SELECT * FROM m_courier_type WHERE id = :id", nativeQuery = true)
    MCourierType findByIdData(long id);

    @Query(value = "SELECT * FROM m_courier_type WHERE is_delete = false", nativeQuery = true)
    List<MCourierType> findAllNotDeleted();

    @Query(value = "SELECT * FROM m_courier_type WHERE is_delete = false ORDER BY m_courier_type.id ASC",nativeQuery = true)
    Page<MCourierType> findAllOrderByAsc(Pageable pageable);

    @Query(value = "SELECT * FROM m_courier_type WHERE is_delete = false ORDER BY m_courier_type.id DESC",nativeQuery = true)
    Page<MCourierType> findAllOrderByDesc(Pageable pageable);

    @Query(value = "SELECT * FROM m_courier_type WHERE is_delete = false ORDER BY m_courier_type.couried_id ASC",nativeQuery = true)
    Page<MCourierType> findAllOrderCourierIdByAsc(Pageable pageable);

    @Query(value = "SELECT * FROM m_courier_type WHERE is_delete = false ORDER BY m_courier_type.couried_id DESC",nativeQuery = true)
    Page<MCourierType> findAllOrderCourierIdByDesc(Pageable pageable);

    @Query(value = "SELECT * FROM m_courier_type WHERE is_delete = false ORDER BY m_courier_type.name ASC",nativeQuery = true)
    Page<MCourierType> findAllOrderNameByAsc(Pageable pageable);

    @Query(value = "SELECT * FROM m_courier_type WHERE is_delete = false ORDER BY m_courier_type.name DESC",nativeQuery = true)
    Page<MCourierType> findAllOrderNameByDesc(Pageable pageable);

    @Query(value = "SELECT * FROM m_courier_type WHERE is_delete = false",nativeQuery = true)
    Page<MCourierType> findAllOrder(Pageable pageable);


}
