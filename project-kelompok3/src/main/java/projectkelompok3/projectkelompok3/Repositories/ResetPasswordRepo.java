package projectkelompok3.projectkelompok3.Repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import projectkelompok3.projectkelompok3.Models.T_Reset_Password;

public interface ResetPasswordRepo extends JpaRepository<T_Reset_Password, Long> {

}
