package projectkelompok3.projectkelompok3.Repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import projectkelompok3.projectkelompok3.Models.CustomerRelation;

import java.util.List;

public interface CustomerRelationRepositories extends JpaRepository<CustomerRelation, Long> {
    @Query(value = "SELECT * FROM m_customer_relation WHERE id = :id", nativeQuery = true)
    CustomerRelation findByIdData(long id);

    @Query(value = "SELECT * FROM m_customer_relation WHERE lower(name) LIKE lower (concat('%', :keyword, '%')) AND is_Delete = false", nativeQuery = true)
    List<CustomerRelation> searchCustomerRelation(String keyword);

    @Query(value = "SELECT * FROM m_customer_relation WHERE is_delete = false", nativeQuery = true)
    List<CustomerRelation> findAllNotDeleted();
}
