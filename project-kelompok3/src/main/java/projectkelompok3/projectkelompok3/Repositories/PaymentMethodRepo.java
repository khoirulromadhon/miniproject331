package projectkelompok3.projectkelompok3.Repositories;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import projectkelompok3.projectkelompok3.Models.PaymentMethod;

import java.util.List;

public interface PaymentMethodRepo extends JpaRepository<PaymentMethod, Long> {


    @Query("FROM PaymentMethod WHERE lower(name) LIKE lower(concat('%',?1,'%'))")
    List<PaymentMethod> SearchPayment(String keyword);

    @Query(value = "SELECT * FROM m_payment_method WHERE id = :id", nativeQuery = true)
    PaymentMethod findByIdData(Long id);

    @Query(value = "SELECT * FROM m_payment_method WHERE is_delete = false", nativeQuery = true)
    List<PaymentMethod> findAllNotDelete();

    @Query(value = "SELECT * FROM m_payment_method WHERE is_delete = false", nativeQuery = true)
    Page<PaymentMethod> findAllOrder(Pageable pageable);

    @Query(value = "SELECT * FROM m_payment_method ORDER BY m_payment_mehod.name ASC", nativeQuery = true)
    Page<PaymentMethod> findAllOrderByAsc(Pageable pageable);
}
