package projectkelompok3.projectkelompok3.Repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import projectkelompok3.projectkelompok3.Models.MCustomer;

import java.util.List;
import java.util.Optional;

public interface CustomerRepo extends JpaRepository<MCustomer, Long> {
    Optional<MCustomer> findByBiodataId(Long biodataId);
    List<MCustomer> findAllByBiodataId(Long biodataId);

}
