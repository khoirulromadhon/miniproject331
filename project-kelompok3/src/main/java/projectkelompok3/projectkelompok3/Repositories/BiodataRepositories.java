package projectkelompok3.projectkelompok3.Repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import projectkelompok3.projectkelompok3.Models.Biodata;

public interface BiodataRepositories extends JpaRepository<Biodata, Long> {
    Biodata findByImagePath(String imgPath);

    @Query(value = "SELECT * FROM m_biodata WHERE image_path = :imgPath", nativeQuery = true)
    Biodata getImage(String imgPath);

    @Modifying
    @Query(value = "INSERT INTO m_biodata(image_path, image)VALUES(:imgPath, :image)", nativeQuery = true)
    int storeImage(String imgPath,byte[]image);
//    Biodata getOne(Long id);
}
