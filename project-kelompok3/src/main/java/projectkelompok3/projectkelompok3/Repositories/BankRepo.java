package projectkelompok3.projectkelompok3.Repositories;

import groovy.util.Eval;
import net.bytebuddy.TypeCache;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import projectkelompok3.projectkelompok3.Models.Bank;

import java.util.List;

public interface BankRepo extends JpaRepository<Bank, Long> {

    @Query("FROM Bank WHERE lower(name) LIKE lower(concat('%',?1,'%'))")
    List<Bank> SearchBank(String keyword);

    @Query(value = "SELECT * FROM m_bank WHERE id = :id", nativeQuery = true)
    Bank findByIdData(Long id);

    @Query(value = "SELECT * FROM m_bank WHERE is_delete = false", nativeQuery = true)
    List<Bank> findAllNotDelete();

    @Query(value = "SELECT * FROM m_bank ORDER BY m_bank.name ASC", nativeQuery = true)
    List<Bank> findAllOrderByAsc();

    @Query(value = "SELECT * FROM m_bank WHERE is_delete = false", nativeQuery = true)
    Page<Bank> findAllOrder(Pageable pageable);

    @Query(value = "SELECT * FROM m_bank ORDER BY m_bank.name ASC", nativeQuery = true)
    Page<Bank> findAllOrderByAsc(Pageable pageable);
}