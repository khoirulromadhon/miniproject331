package projectkelompok3.projectkelompok3.Repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import projectkelompok3.projectkelompok3.Models.Specialization;

import java.util.List;

public interface SpecializationRepositories extends JpaRepository<Specialization, Long> {
    @Query(value = "SELECT * FROM m_specialization WHERE is_delete = false", nativeQuery = true)
    public List<Specialization> getAllNotDeletedSpecializations();
}
