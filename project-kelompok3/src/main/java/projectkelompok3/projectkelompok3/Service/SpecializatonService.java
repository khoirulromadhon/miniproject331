package projectkelompok3.projectkelompok3.Service;

import projectkelompok3.projectkelompok3.Models.Specialization;

import java.util.List;

public interface SpecializatonService {
    public List<Specialization> getAllSpecializations();

    public Specialization getSpecializationById(Long id);

    public Specialization inserSpecialization(Specialization specialization);

    public Specialization updateSpecialization(Specialization specialization);

    public void deleteSpecialization(Specialization specialization);

    public List<Specialization> searchByName(String keyword);
}
