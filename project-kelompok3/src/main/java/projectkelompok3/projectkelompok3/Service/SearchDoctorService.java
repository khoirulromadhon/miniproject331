package projectkelompok3.projectkelompok3.Service;

import org.springframework.stereotype.Service;
import projectkelompok3.projectkelompok3.Models.Doctor;

import java.util.List;


public interface SearchDoctorService {
    public List<Doctor> getAllDoctors();
    public Doctor getDoctorById(Long id);
    public Doctor updateDoctor(Doctor doctor);
    public void deleteDoctor(Doctor doctor);
    public List<Doctor> filterDoctor(Doctor doctorFilter);
}
