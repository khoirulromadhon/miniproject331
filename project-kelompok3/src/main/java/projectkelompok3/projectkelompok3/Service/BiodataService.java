package projectkelompok3.projectkelompok3.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import projectkelompok3.projectkelompok3.Models.Biodata;
import projectkelompok3.projectkelompok3.Models.MCustomer;
import projectkelompok3.projectkelompok3.Repositories.BiodataRepositories;
import projectkelompok3.projectkelompok3.Repositories.CustomerRepo;

import java.util.List;
import java.util.Optional;
@Service
public class BiodataService {
    private final BiodataRepositories biodataRepositories;
    private final CustomerRepo customerRepo;
    @Autowired
    public BiodataService(BiodataRepositories biodataRepository, CustomerRepo customerRepo) {
        this.biodataRepositories = biodataRepository;
        this.customerRepo = customerRepo;
    }
    public Biodata getBiodataById(Long id) {
        Optional<Biodata> optionalBiodata = biodataRepositories.findById(id);
        return optionalBiodata.orElse(null);
    }
    // Menambahkan metode baru untuk mendapatkan daftar MCustomer berdasarkan biodataId
    public List<MCustomer> getCustomersByBiodataId(Long biodataId) {
        // Menggunakan repositori CustomerRepo untuk mendapatkan daftar MCustomer berdasarkan biodataId
        return customerRepo.findAllByBiodataId(biodataId);
    }
}
