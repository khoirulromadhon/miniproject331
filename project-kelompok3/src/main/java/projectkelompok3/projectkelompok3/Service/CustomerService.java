package projectkelompok3.projectkelompok3.Service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import projectkelompok3.projectkelompok3.Models.MCustomer;
import projectkelompok3.projectkelompok3.Repositories.CustomerRepo;

import java.util.List;
import java.util.Optional;

@Service
public class CustomerService {
    private final CustomerRepo customerRepo;

    @Autowired
    public CustomerService(CustomerRepo customerRepo) {
        this.customerRepo = customerRepo;
    }

    public List<MCustomer> getCustomersByBiodataId(Long biodataId) {
        return customerRepo.findAllByBiodataId(biodataId);
    }
    public MCustomer getCustomerById(Long id) {
        return customerRepo.findById(id).orElse(null);
    }
}

