package projectkelompok3.projectkelompok3.Service;

import org.springframework.transaction.annotation.Transactional;
import projectkelompok3.projectkelompok3.Models.DoctorTreatment;
import projectkelompok3.projectkelompok3.Repositories.DoctorTreatmentRepository;

import java.util.List;

@Transactional
public class DoctorTreatmentImpl implements DoctorTreatmentService{
    private DoctorTreatmentRepository doctorTreatmentRepository;

    public DoctorTreatmentImpl(DoctorTreatmentRepository doctorTreatmentRepository) {
        this.doctorTreatmentRepository = doctorTreatmentRepository;
    }

    @Override
    public List<DoctorTreatment> GetAllDoctorTreatments() {
        return doctorTreatmentRepository.findAll();
    }

    @Override
    public DoctorTreatment getDoctorTratmentById(Long id) {
        return null;
    }

    @Override
    public DoctorTreatment updateDoctorTreatment(DoctorTreatment doctorTreatment) {
        return null;
    }

    @Override
    public void deleteDoctorTreatment(DoctorTreatment doctorTreatment) {

    }

    @Override
    public List<DoctorTreatment> searchByName(String keyword) {
        return null;
    }
}
