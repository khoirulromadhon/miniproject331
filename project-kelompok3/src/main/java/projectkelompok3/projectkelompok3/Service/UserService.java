package projectkelompok3.projectkelompok3.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import projectkelompok3.projectkelompok3.Models.MCustomer;
import projectkelompok3.projectkelompok3.Models.MUser;
import projectkelompok3.projectkelompok3.Repositories.UserRepo;

import java.util.List;
import java.util.Optional;

@Service
public class UserService {

    private final UserRepo userRepo;

    @Autowired
    public UserService(UserRepo userRepo) {
        this.userRepo = userRepo;
    }

    public List<MUser> getUserByBiodataId(Long biodataId) {
        return userRepo.findAllByBiodataId(biodataId); // Memanggil metode dari instance userRepo yang sudah diinisialisasi
    }
}
