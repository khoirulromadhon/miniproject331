package projectkelompok3.projectkelompok3.Service;

import projectkelompok3.projectkelompok3.Models.DoctorTreatment;

import java.util.List;

public interface DoctorTreatmentService {
    public List<DoctorTreatment> GetAllDoctorTreatments();
    public DoctorTreatment getDoctorTratmentById(Long id);
    public DoctorTreatment updateDoctorTreatment(DoctorTreatment doctorTreatment);
    public void deleteDoctorTreatment(DoctorTreatment doctorTreatment);
    public List<DoctorTreatment> searchByName(String keyword);
}
