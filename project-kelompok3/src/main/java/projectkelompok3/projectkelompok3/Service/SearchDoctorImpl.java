package projectkelompok3.projectkelompok3.Service;

import org.springframework.transaction.annotation.Transactional;
import projectkelompok3.projectkelompok3.Models.Doctor;
import projectkelompok3.projectkelompok3.Repositories.SearchDoctorRepositories;

import java.util.List;
import java.util.stream.Collectors;

@Transactional
public class SearchDoctorImpl implements SearchDoctorService{

    private SearchDoctorRepositories searchDoctorRepositories;

    public SearchDoctorImpl(SearchDoctorRepositories searchDoctorRepositories){
        this.searchDoctorRepositories = searchDoctorRepositories;
    }
    @Override
    public List<Doctor> getAllDoctors() {
        return searchDoctorRepositories.findAll().stream()
                .filter(d -> d.getDelete() == false)
                .collect(Collectors.toList());
    }

    @Override
    public Doctor getDoctorById(Long id) {
        return searchDoctorRepositories.findById(id).orElseThrow();
    }

    @Override
    public Doctor updateDoctor(Doctor doctor) {
        return searchDoctorRepositories.save(doctor);
    }

    @Override
    public void deleteDoctor(Doctor doctor) {
        searchDoctorRepositories.save(doctor);
    }

    @Override
    public List<Doctor> filterDoctor(Doctor doctorFilter) {
        List<Doctor> doctors = getAllDoctors();

        if(doctorFilter.getBiodata() != null) {
            String fullname = doctorFilter.getBiodata().getFullname();
            doctors = doctors.stream()
                    .filter(d -> d.getBiodata().getFullname().toLowerCase().contains(fullname.toLowerCase()))
                    .collect(Collectors.toList());

        }
        if(doctorFilter.getCurrentSpecialization() != null) {
            Long specializationId = doctorFilter.getCurrentSpecialization().get(0).getSpecializationId();
            doctors = doctors.stream()
                    .filter(d -> d.getCurrentSpecialization().get(0).getSpecializationId() == specializationId)
                    .collect(Collectors.toList());
        }

        if(doctorFilter.getDoctorOffice() != null) {
            Long locationId = doctorFilter.getDoctorOffice().get(0).getMedicalFacility().getLocationId();
            doctors = doctors.stream()
                    .filter(d -> d.getDoctorOffice().stream()
                            .anyMatch(o -> (o.getMedicalFacility().getLocationId() == locationId) ||
                                    o.getMedicalFacility().getLocationId() == locationId))
                    .collect(Collectors.toList());
        }

        if(doctorFilter.getDoctorTreatment() != null) {
            Long doctorTreatmentId = doctorFilter.getDoctorTreatment().get(0).getId();
            doctors = doctors.stream()
                    .filter(d -> d.getDoctorTreatment().stream().anyMatch(t -> t.getId() == doctorTreatmentId))
                    .collect(Collectors.toList());
        }
        return null;
    }
}
