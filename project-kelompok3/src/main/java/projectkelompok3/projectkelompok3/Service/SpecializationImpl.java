package projectkelompok3.projectkelompok3.Service;

import org.springframework.transaction.annotation.Transactional;
import projectkelompok3.projectkelompok3.Models.Specialization;
import projectkelompok3.projectkelompok3.Repositories.SpecializationRepositories;

import java.util.List;

@Transactional
public class SpecializationImpl implements SpecializatonService{
    private SpecializationRepositories specializationRepositories;

    public SpecializationImpl(SpecializationRepositories specializationRepositories) {
        this.specializationRepositories = specializationRepositories;
    }

    @Override
    public List<Specialization> getAllSpecializations() {
        return specializationRepositories.getAllNotDeletedSpecializations();
    }

    @Override
    public Specialization getSpecializationById(Long id) {
        return null;
    }

    @Override
    public Specialization inserSpecialization(Specialization specialization) {
        return null;
    }

    @Override
    public Specialization updateSpecialization(Specialization specialization) {
        return null;
    }

    @Override
    public void deleteSpecialization(Specialization specialization) {

    }

    @Override
    public List<Specialization> searchByName(String keyword) {
        return null;
    }
}
